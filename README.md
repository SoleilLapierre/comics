# Comics

A WinForms + MySQL program I created to help track my comic book collection. 

![Screenshot](screenshot.png "Screenshot")

Recently updated to work with Sqlite3 databases, because I needed to be able
to update my collection offline.

This was one of my first database programs and I made a bunch of mistakes - I don't
intend to update this version anymore and don't recommend using it. 
