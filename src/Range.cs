using System;
using System.Collections.Generic;
using System.Text;

namespace Comics
{
    class Range : IEnumerable<int>
    {
        public Range()
        {
        }


        public static Range FromString(string aRangeString)
        {
            if (aRangeString is null)
            {
                return null;
            }

            Range range = new Range();

            string[] parts = aRangeString.Split(',');
            foreach (string ss in parts)
            {
                string s = ss.Trim();
                if (s.Length > 0)
                {
                    bool parsed = false;

                    if (s.Contains(".."))
                    {
                        string[] ends = s.Split(new string[] { ".." }, StringSplitOptions.RemoveEmptyEntries);
                        if (ends.Length == 2)
                        {
                            try
                            {
                                int val1 = int.Parse(ends[0]);
                                int val2 = int.Parse(ends[1]);
                                RangeSegment r = new RangeSegment(val1, val2);
                                range.mSegments.Add(r);
                                parsed = true;
                            }
                            catch (FormatException) { }
                        }
                    }
                    else
                    {
                        try
                        {
                            int val = int.Parse(s);
                            RangeSegment r = new RangeSegment(val, val);
                            range.mSegments.Add(r);
                            parsed = true;
                        }
                        catch (FormatException) { }
                    }

                    if (!parsed)
                    {
                        return null;
                    }
                }
            }

            range.mSegments.Sort();

            int count = range.mSegments.Count;

            if (count == 0)
            {
                return null;
            }

            for (int i = 1; i < count;)
            {
                RangeSegment left = range.mSegments[i - 1];
                RangeSegment right = range.mSegments[i];
                if (left.Intersects(right))
                {
                    left.End = right.End;
                    range.mSegments.RemoveAt(i);
                    count--;
                }
                else
                {
                    i++;
                }
            }

            return range;
        }


        public void Add(int aValue)
        {
            RangeSegment foo = new RangeSegment(aValue, aValue);
            int index = mSegments.BinarySearch(foo);

            if (index >= 0)
            {
                return;
            }

            index = (~index - 1);
            RangeSegment r = mSegments[index];

            if (r.Contains(aValue))
            {
                return;
            }

            if (aValue == (r.Start - 1))
            {
                r.Start--;
                if (index > 0)
                {
                    RangeSegment r2 = mSegments[index - 1];
                    if (r2.End == (r.Start - 1))
                    {
                        r2.End = r.End;
                        mSegments.RemoveAt(index);
                    }
                }
            }
            else if (aValue == (r.End + 1))
            {
                r.End++;
                if (index < (mSegments.Count - 1))
                {
                    RangeSegment r2 = mSegments[index + 1];
                    if (r2.Start == (r.End + 1))
                    {
                        r.End = r2.End;
                        mSegments.RemoveAt(index + 1);
                    }
                }
            }
            else
            {
                mSegments.Insert(index + 1, foo);
            }
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            bool first = true;
            foreach (RangeSegment r in mSegments)
            {
                if (!first)
                {
                    sb.Append(",");
                }
                first = false;

                sb.Append(r);
            }

            return sb.ToString();
        }

        #region IEnumerable<int> implementation

        public IEnumerator<int> GetEnumerator()
        {
            foreach (RangeSegment r in mSegments)
            {
                for (int i = r.Start; i <= r.End; i++)
                {
                    yield return i;
                }
            }
        }

        #endregion
        #region IEnumerable implementation

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<int>)this).GetEnumerator();
        }

        #endregion
        #region ----- Private members and types -----

        private class RangeSegment : IComparable<RangeSegment>
        {
            private int mStart, mEnd;

            public RangeSegment(int aStart, int aEnd)
            {
                mStart = aStart;
                mEnd = aEnd;
                Fixup();
            }

            public int Start
            {
                get { return mStart; }
                set
                {
                    mStart = value;
                    Fixup();
                }
            }

            public int End
            {
                get { return mEnd; }
                set
                {
                    mEnd = value;
                    Fixup();
                }
            }

            private void Fixup()
            {
                if (mStart > mEnd)
                {
                    int temp = mStart;
                    mStart = mEnd;
                    mEnd = temp;
                }
            }

            public bool Contains(int aValue)
            {
                return ((mStart <= aValue) && (aValue <= mEnd));
            }

            public bool Intersects(RangeSegment aRange)
            {
                return (Contains(aRange.mStart) || Contains(aRange.mEnd) || aRange.Contains(mStart));
            }

            public override string ToString()
            {
                return string.Format("{0}..{1}", mStart, mEnd);
            }

            #region IComparable<RangeSegment> Members

            public int CompareTo(RangeSegment other)
            {
                return (mStart - other.mStart);
            }

            #endregion
        }

        private readonly List<RangeSegment> mSegments = new List<RangeSegment>();

        #endregion
    }
}
