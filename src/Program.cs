using System;
using System.Windows.Forms;

namespace Comics
{
    static class Program
    {
        public static Properties.Settings Settings
        {
            get 
            {
                return sSettings; 
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            sSettings.Save();
        }

        private static Properties.Settings sSettings = new Properties.Settings();
    }
}