using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Comics
{
    public partial class ConnectionDialog : Form
    {
        public ConnectionDialog(string aTitle)
        {
            InitializeComponent();
            this.Text = aTitle;

            mFocusedControl = mHostBox;
            if (Comics.Program.Settings.LastSqlHost != null)
            {
                mHostList = Comics.Program.Settings.LastSqlHost;
                foreach (string s in mHostList)
                {
                    mHostBox.Items.Add(s);
                }

                mHostBox.SelectedIndex = 0;
                mFocusedControl = mDatabaseBox;
            }

            if (Comics.Program.Settings.LastSqlDatabase != null)
            {
                mDbList = Comics.Program.Settings.LastSqlDatabase;
                foreach (string s in mDbList)
                {
                    mDatabaseBox.Items.Add(s);
                }

                mDatabaseBox.SelectedIndex = 0;
                mFocusedControl = mUserNameBox;
            }

            mUserNameBox.Text = Comics.Program.Settings.LastSqlUserName;
            if ((mUserNameBox.Text != null) && (mUserNameBox.Text.Length > 0))
            {
                mFocusedControl = mPasswordBox;
            }

            mPasswordBox.Text = Comics.Program.Settings.LastSqlPassword;
            if ((mPasswordBox.Text != null) && (mPasswordBox.Text.Length > 0))
            {
                mFocusedControl = mOKButton;
            }

            mSavePasswordButton.Checked = Comics.Program.Settings.SaveSqlPassword;
        }


        protected override void OnLoad(EventArgs e)
        {
            BeginInvoke(new MethodInvoker(SetFocusDelegate));
            base.OnLoad(e);
        }


        private void SetFocusDelegate()
        {
            if (mFocusedControl != null)
            {
                mFocusedControl.Focus();
            }
        }


        private void ConnectionDialog_OnKeyPress(object sender, KeyPressEventArgs e)
        {
            EnableDisableOKButton();
        }


        private void ConnectionDialog_Validating(object sender, CancelEventArgs e)
        {
            EnableDisableOKButton();
        }


        private void EnableDisableOKButton()
        {
            bool ok = (HostName.Length > 0);
            ok &= (DatabaseName.Length > 0);
            ok &= (UserName.Length > 0);
            ok &= (Password.Length > 0);

            mOKButton.Enabled = ok;
        }


        private void mOKButton_Click(object sender, EventArgs e)
        {
            Comics.Program.Settings.SaveSqlPassword = mSavePasswordButton.Checked;
            Comics.Program.Settings.LastSqlUserName = mUserNameBox.Text;

            if (mSavePasswordButton.Checked)
            {
                Comics.Program.Settings.LastSqlPassword = mPasswordBox.Text;
            }
            else
            {
                Comics.Program.Settings.LastSqlPassword = "";
            }

            UpdateMRUList(mHostList, mHostBox.Text);
            Comics.Program.Settings.LastSqlHost = mHostList;

            UpdateMRUList(mDbList, mDatabaseBox.Text);
            Comics.Program.Settings.LastSqlDatabase = mDbList;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }


        private void UpdateMRUList(System.Collections.Specialized.StringCollection aList, string aNewEntry)
        {
            if (aList.Contains(aNewEntry))
            {
                aList.Remove(aNewEntry);
            }

            aList.Insert(0, aNewEntry);
        }


        public string HostName => mHostBox.Text;

        public string DatabaseName => mDatabaseBox.Text;

        public string UserName => mUserNameBox.Text;

        public string Password => mPasswordBox.Text;


        private System.Collections.Specialized.StringCollection mHostList = new System.Collections.Specialized.StringCollection();
        private System.Collections.Specialized.StringCollection mDbList = new System.Collections.Specialized.StringCollection();
        private Control mFocusedControl;
    }
}