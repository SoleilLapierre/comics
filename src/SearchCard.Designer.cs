﻿namespace Comics
{
    partial class SearchCard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mSearchEnabledCheck = new System.Windows.Forms.CheckBox();
            this.mSearchRecordTypeSelection = new System.Windows.Forms.ComboBox();
            this.mFieldSelection = new System.Windows.Forms.ComboBox();
            this.mMatchOpSelection = new System.Windows.Forms.ComboBox();
            this.mMatchValueBox = new System.Windows.Forms.TextBox();
            this.mCloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mSearchEnabledCheck
            // 
            this.mSearchEnabledCheck.AutoSize = true;
            this.mSearchEnabledCheck.Location = new System.Drawing.Point(10, 34);
            this.mSearchEnabledCheck.Name = "mSearchEnabledCheck";
            this.mSearchEnabledCheck.Size = new System.Drawing.Size(15, 14);
            this.mSearchEnabledCheck.TabIndex = 0;
            this.mSearchEnabledCheck.UseVisualStyleBackColor = true;
            this.mSearchEnabledCheck.CheckedChanged += new System.EventHandler(this.mSearchEnabledCheck_CheckedChanged);
            // 
            // mSearchRecordTypeSelection
            // 
            this.mSearchRecordTypeSelection.FormattingEnabled = true;
            this.mSearchRecordTypeSelection.Items.AddRange(new object[] {
            "Series",
            "Issue"});
            this.mSearchRecordTypeSelection.Location = new System.Drawing.Point(35, 3);
            this.mSearchRecordTypeSelection.Name = "mSearchRecordTypeSelection";
            this.mSearchRecordTypeSelection.Size = new System.Drawing.Size(58, 21);
            this.mSearchRecordTypeSelection.TabIndex = 1;
            this.mSearchRecordTypeSelection.SelectedIndexChanged += new System.EventHandler(this.mSearchRecordTypeSelection_SelectedIndexChanged);
            // 
            // mFieldSelection
            // 
            this.mFieldSelection.FormattingEnabled = true;
            this.mFieldSelection.Location = new System.Drawing.Point(99, 3);
            this.mFieldSelection.Name = "mFieldSelection";
            this.mFieldSelection.Size = new System.Drawing.Size(132, 21);
            this.mFieldSelection.TabIndex = 2;
            this.mFieldSelection.SelectedIndexChanged += new System.EventHandler(this.mFieldSelection_SelectedIndexChanged);
            // 
            // mMatchOpSelection
            // 
            this.mMatchOpSelection.FormattingEnabled = true;
            this.mMatchOpSelection.Items.AddRange(new object[] {
            "Contains",
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "!=",
            "Regex"});
            this.mMatchOpSelection.Location = new System.Drawing.Point(35, 30);
            this.mMatchOpSelection.Name = "mMatchOpSelection";
            this.mMatchOpSelection.Size = new System.Drawing.Size(68, 21);
            this.mMatchOpSelection.TabIndex = 3;
            this.mMatchOpSelection.SelectedIndexChanged += new System.EventHandler(this.mMatchOpSelection_SelectedIndexChanged);
            // 
            // mMatchValueBox
            // 
            this.mMatchValueBox.Location = new System.Drawing.Point(109, 30);
            this.mMatchValueBox.Name = "mMatchValueBox";
            this.mMatchValueBox.Size = new System.Drawing.Size(122, 20);
            this.mMatchValueBox.TabIndex = 4;
            this.mMatchValueBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mMatchValueBox_KeyUp);
            this.mMatchValueBox.Leave += new System.EventHandler(this.mMatchValueBox_Leave);
            // 
            // mCloseButton
            // 
            this.mCloseButton.Location = new System.Drawing.Point(3, 4);
            this.mCloseButton.Name = "mCloseButton";
            this.mCloseButton.Size = new System.Drawing.Size(26, 23);
            this.mCloseButton.TabIndex = 5;
            this.mCloseButton.Text = "X";
            this.mCloseButton.UseVisualStyleBackColor = true;
            this.mCloseButton.Click += new System.EventHandler(this.mCloseButton_Click);
            // 
            // SearchCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mCloseButton);
            this.Controls.Add(this.mMatchValueBox);
            this.Controls.Add(this.mMatchOpSelection);
            this.Controls.Add(this.mFieldSelection);
            this.Controls.Add(this.mSearchRecordTypeSelection);
            this.Controls.Add(this.mSearchEnabledCheck);
            this.Name = "SearchCard";
            this.Size = new System.Drawing.Size(237, 55);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox mSearchEnabledCheck;
        private System.Windows.Forms.ComboBox mSearchRecordTypeSelection;
        private System.Windows.Forms.ComboBox mFieldSelection;
        private System.Windows.Forms.ComboBox mMatchOpSelection;
        private System.Windows.Forms.TextBox mMatchValueBox;
        private System.Windows.Forms.Button mCloseButton;
    }
}
