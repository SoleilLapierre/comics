using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using MySql.Data.MySqlClient;

namespace Comics
{
    class MySqlStorage : IStorage
    {
        public static MySqlStorage Create(string aHostName, string aDatabaseName, string aUserName, string aPassword, Schema aSchema)
        {
            Schema schema = new Schema(aSchema);
            schema.AddTable(typeof(InfoRecord));

            string connectionString = String.Format("server={0};user Id={2};password={3};port=3306;pooling=false",
                aHostName, aDatabaseName, aUserName, aPassword);

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            StringBuilder query = new StringBuilder();
            query.Append("CREATE DATABASE `");
            query.Append(aDatabaseName);
            query.Append("` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;");

            MySqlTransaction transaction = connection.BeginTransaction();
            MySqlCommand command = new MySqlCommand(query.ToString());
            command.Connection = connection;
            command.Transaction = transaction;
            command.ExecuteNonQuery();

            connection.ChangeDatabase(aDatabaseName);

            foreach (Schema.Table t in schema.Tables)
            {
                query = new StringBuilder();
                query.Append("CREATE TABLE `");
                query.Append(t.Name);
                query.AppendLine("` (");

                string primaryKey = null;
                foreach (Schema.Field f in t.Fields)
                {
                    query.Append("  `");
                    query.Append(f.DatabaseName);
                    query.Append("` ");
                    query.Append(FieldTypeToSqlType(f.Type));

                    if (f.Required)
                    {
                        query.Append(" NOT NULL");
                    }
                    else if (f.Type == Schema.Field.FieldType.DateTime)
                    {
                        query.Append(" default '20000101000000'");
                    }
                    else
                    {
                        query.Append(" default NULL");
                    }

                    if (t.Fields.IndexOf(f) == t.KeyField)
                    {
                        primaryKey = f.DatabaseName;
                    }

                    if (t.Fields.IndexOf(f) == t.TimestampField)
                    {
                        query.Append(" default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
                    }

                    query.AppendLine(",");
                }

                query.Append("  PRIMARY KEY (`");
                query.Append(primaryKey);
                query.AppendLine("`)");
                query.AppendLine(") ENGINE=MyISAM DEFAULT CHARSET=latin1;");

                command = new MySqlCommand(query.ToString());
                command.Connection = connection;
                command.ExecuteNonQuery();
            }

            transaction.Commit();

            MySqlStorage storage = new MySqlStorage();
            storage.mConnection = connection;
            storage.mCurrentSchema = schema;

            InfoRecord info = new InfoRecord();
            info.Schema = schema.Serialize();
            info.CollectionName = aDatabaseName;

            storage.BeginTransaction();
            storage.AddRecord<InfoRecord>(info);
            storage.CommitTransaction();

            return storage;
        }


        public static MySqlStorage Open(string aHostName, string aDatabaseName, string aUserName, string aPassword, Schema aSchema)
        {
            Schema schema = new Schema(aSchema);
            schema.AddTable(typeof(InfoRecord));

            string connectionString = String.Format("server={0};database={1};user Id={2};password={3};port=3306;pooling=false;",
                aHostName, aDatabaseName, aUserName, aPassword);

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlStorage storage = new MySqlStorage();
            storage.mConnection = connection;
            storage.mCurrentSchema = schema;

            Dictionary<int, InfoRecord> headers = storage.GetRecords<InfoRecord>();
            int max = 0;
            foreach (int i in headers.Keys)
            {
                if (i > max)
                {
                    max = i;
                }
            }

            InfoRecord info = headers[max];

            if (info.Version != schema.Version)
            {
                throw new NotImplementedException("Upgrading database version not yet supported.");
            }

            //storage.mCurrentSchema = info.Schema;

            return storage;
        }

        #region ----- IStorage implementation  -----

        public Dictionary<int, T> GetRecords<T>() where T : Record
        {
            if (mCurrentTransaction != null)
            {
                throw new InvalidOperationException("Cannot run a query when a transaction is open; complete the transaction first.");
            }

            Schema.Table table = mCurrentSchema[typeof(T)];
            Dictionary<string, int> fieldMap = new Dictionary<string, int>();

            StringBuilder query = new StringBuilder();
            query.Append("SELECT * FROM ");
            query.Append(table.Name);
            query.Append(";");

            MySqlCommand command = new MySqlCommand(query.ToString())
            {
                Connection = mConnection
            };

            Dictionary<int, T> result = new Dictionary<int, T>();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Type typeHandle = typeof(T);
                ConstructorInfo constructor = typeHandle.GetConstructor(System.Type.EmptyTypes);
                T obj = constructor.Invoke(null) as T;

                foreach (Schema.Field field in table.Fields)
                {
                    PropertyInfo prop = typeHandle.GetProperty(field.PropertyName);
                    int ordinal = reader.GetOrdinal(field.DatabaseName);
                    object val = null;

                    if (!reader.IsDBNull(ordinal))
                    {
                        switch (field.Type)
                        {
                            case Schema.Field.FieldType.Int8:
                                val = reader.GetChar(ordinal);
                                break;
                            case Schema.Field.FieldType.Int16:
                                val = reader.GetInt16(ordinal);
                                break;
                            case Schema.Field.FieldType.Int32:
                                val = reader.GetInt32(ordinal);
                                break;
                            case Schema.Field.FieldType.Int64:
                                val = reader.GetInt64(ordinal);
                                break;
                            case Schema.Field.FieldType.Uint8:
                                val = reader.GetByte(ordinal);
                                break;
                            case Schema.Field.FieldType.Uint16:
                                val = reader.GetUInt16(ordinal);
                                break;
                            case Schema.Field.FieldType.Uint32:
                                val = reader.GetUInt32(ordinal);
                                break;
                            case Schema.Field.FieldType.Uint64:
                                val = reader.GetUInt64(ordinal);
                                break;
                            case Schema.Field.FieldType.Decimal:
                                val = reader.GetDecimal(ordinal);
                                break;
                            case Schema.Field.FieldType.Float:
                                val = reader.GetFloat(ordinal);
                                break;
                            case Schema.Field.FieldType.Double:
                                val = reader.GetDouble(ordinal);
                                break;
                            case Schema.Field.FieldType.DateTime:
                                val = reader.GetDateTime(ordinal);
                                break;
                            case Schema.Field.FieldType.String:
                                val = UnescapeString(reader.GetString(ordinal));
                                break;
                            case Schema.Field.FieldType.ByteArray:
                                val = reader.GetValue(ordinal);
                                break;
                            case Schema.Field.FieldType.Int32List:
                                string text = reader.GetString(ordinal);
                                string[] vals = text.Split(',');
                                List<Int32> ints = new List<int>(vals.Length);
                                for (int i = 0; i < vals.Length; i++)
                                {
                                    int temp = -1;
                                    if (Int32.TryParse(vals[i], out temp))
                                    {
                                        ints.Add(temp);
                                    }
                                }
                                val = ints;
                                break;
                            default:
                                throw new Exception("Invalid type for field " + field.DatabaseName);
                            // break;
                        }
                    }
                    else if (field.Required)
                    {
                        throw new Exception("Required field " + field.DatabaseName + " is NULL.");
                    }

                    prop.SetValue(obj, val, null);
                }

                obj.Dirty = false;
                result[obj.ID] = obj;
            }
            reader.Close();

            return result;
        }


        public void BeginTransaction()
        {
            if (mCurrentTransaction != null)
            {
                throw new InvalidOperationException("Cannot start a transaction when a transaction is already open; complete the transaction first.");
            }
            
            mCurrentTransaction = mConnection.BeginTransaction();
        }


        public bool CommitTransaction()
        {
            if (mCurrentTransaction == null)
            {
                throw new InvalidOperationException("Cannot commit when no transaction is open. Call BeginTransaction() first.");
            }

            bool result = false;
            try
            {
                mCurrentTransaction.Commit();
                result = true;
            }
            catch (Exception)
            {
                try
                {
                    mCurrentTransaction.Rollback();
                }
                catch (Exception)
                {
                }
            }

            mCurrentTransaction = null;
            return result;
        }


        public void RollbackTransaction()
        {
            if (mCurrentTransaction == null)
            {
                throw new InvalidOperationException("Cannot commit when no transaction is open. Call BeginTransaction() first.");
            }

            try
            {
                mCurrentTransaction.Rollback();
            }
            catch (Exception)
            {
            }

            mCurrentTransaction = null;
        }


        public bool Update<T>(T aRecord) where T : Record
        {
            if (!aRecord.Dirty)
            {
                throw new InvalidOperationException("Only call Update() on objects that are dirty.");
            }

            if (aRecord.ID < 0)
            {
                throw new InvalidOperationException("Only call Update() on objects that are already in the database.");
            }

            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];

            StringBuilder query = new StringBuilder();
            query.Append("UPDATE `");
            query.Append(table.Name);
            query.Append("` SET ");

            bool first = true;
            foreach (Schema.Field field in table.Fields)
            {
                if (table.Fields.IndexOf(field) != table.KeyField)
                {
                    if (!first)
                    {
                        query.Append(", ");
                    }
                    first = false;

                    query.Append("`");
                    query.Append(field.DatabaseName);
                    query.Append("`");

                    PropertyInfo prop = typeHandle.GetProperty(field.PropertyName);
                    object val = prop.GetValue(aRecord, null);

                    if (table.Fields.IndexOf(field) == table.TimestampField)
                    {
                        query.Append("=NOW()");
                    }
                    else if (val != null)
                    {
                        query.Append("='");
                        if (val is DateTime)
                        {
                            DateTime date = (DateTime)val;
                            query.Append(date.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                        else if (val is List<Int32>)
                        {
                            bool firstInt = true;
                            foreach (int intVal in (val as List<Int32>))
                            {
                                if (!firstInt)
                                {
                                    query.Append(",");
                                }
                                firstInt = false;

                                query.Append(intVal.ToString());
                            }
                        }
                        else if (val is String)
                        {
                            query.Append(EscapeString(val as String));
                        }
                        else
                        {
                            query.Append(val.ToString());
                        }
                        query.Append("'");
                    }
                    else
                    {
                        query.Append("=NULL");
                    }
                }
            }

            query.Append(" WHERE `");
            Schema.Field keyField = table.Fields[table.KeyField];
            query.Append(keyField.DatabaseName);
            query.Append("`='");
            query.Append(aRecord.ID.ToString());
            query.Append("';");

            MySqlCommand command = mConnection.CreateCommand();
            command.Connection = mConnection;
            command.Transaction = mCurrentTransaction;
            command.CommandText = query.ToString();
            int count = command.ExecuteNonQuery();

            return (count == 1);
        }


        public bool AddRecord<T>(T aRecord) where T : Record
        {
            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];

            StringBuilder fieldNames = new StringBuilder();
            fieldNames.Append("(");

            StringBuilder fieldValues = new StringBuilder();
            fieldValues.Append("(");

            bool first = true;
            foreach (Schema.Field field in table.Fields)
            {
                if (!first)
                {
                    fieldNames.Append(", ");
                    fieldValues.Append(", ");
                }
                first = false;

                fieldNames.Append("`");
                fieldNames.Append(field.DatabaseName);
                fieldNames.Append("`");

                PropertyInfo prop = typeHandle.GetProperty(field.PropertyName);
                object val = prop.GetValue(aRecord, null);
                if (table.Fields.IndexOf(field) == table.TimestampField)
                {
                    fieldValues.Append("NOW()");
                }
                else if (val != null)
                {
                    fieldValues.Append("'");
                    if (val is DateTime date)
                    {
                        fieldValues.Append(date.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else if (val is List<Int32>)
                    {
                        bool firstInt = true;
                        foreach (int intval in (val as List<Int32>))
                        {
                            if (!firstInt)
                            {
                                fieldValues.Append(",");
                            }
                            firstInt = false;

                            fieldValues.Append(intval.ToString());
                        }
                    }
                    else if (val is String)
                    {
                        fieldValues.Append(EscapeString(val as String));
                    }
                    else
                    {
                        fieldValues.Append(val.ToString());
                    }
                    fieldValues.Append("'");
                }
                else
                {
                    fieldValues.Append("NULL");
                }
            }

            fieldNames.Append(")");
            fieldValues.Append(")");

            StringBuilder query = new StringBuilder();
            query.Append("INSERT INTO `");
            query.Append(table.Name);
            query.Append("` ");
            query.Append(fieldNames.ToString());
            query.Append(" VALUES ");
            query.Append(fieldValues.ToString());
            query.Append(";");

            MySqlCommand command = mConnection.CreateCommand();
            command.Connection = mConnection;
            command.Transaction = mCurrentTransaction;
            command.CommandText = query.ToString();
            int count = command.ExecuteNonQuery();

            return (count == 1);
        }


        public bool DeleteRecord<T>(T aRecord) where T : Record
        {
            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];
            Schema.Field keyField = table.Fields[table.KeyField];

            StringBuilder query = new StringBuilder();
            query.Append("DELETE FROM `");
            query.Append(table.Name);
            query.Append("` WHERE `");
            query.Append(keyField.DatabaseName);
            query.Append("`='");
            query.Append(aRecord.ID);
            query.Append("';");

            MySqlCommand command = mConnection.CreateCommand();
            command.Connection = mConnection;
            command.Transaction = mCurrentTransaction;
            command.CommandText = query.ToString();
            int count = command.ExecuteNonQuery();

            return (count == 1);
        }


        public string GetMessages()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        private static string FieldTypeToSqlType(Schema.Field.FieldType aType)
        {
            string result = null;

            switch (aType)
            {
                case Schema.Field.FieldType.Int8: result = "tinyint"; break;
                case Schema.Field.FieldType.Int16: result = "smallint"; break;
                case Schema.Field.FieldType.Int32: result = "int"; break;
                case Schema.Field.FieldType.Int64: result = "bigint"; break;
                case Schema.Field.FieldType.Uint8: result = "tinyint unsigned"; break;
                case Schema.Field.FieldType.Uint16: result = "smallint unsigned"; break;
                case Schema.Field.FieldType.Uint32: result = "int unsigned"; break;
                case Schema.Field.FieldType.Uint64: result = "bigint unsigned"; break;
                case Schema.Field.FieldType.Decimal: result = "decimal(8,2)"; break;
                case Schema.Field.FieldType.Float: result = "float"; break;
                case Schema.Field.FieldType.Double: result = "double"; break;
                case Schema.Field.FieldType.DateTime: result = "timestamp"; break;
                case Schema.Field.FieldType.String: result = "text"; break;
                case Schema.Field.FieldType.ByteArray: result = "mediumblob"; break;
                case Schema.Field.FieldType.Int32List: result = "text"; break;
                default: break;
            }

            return result;
        }


        private static string EscapeString(string aString)
        {
            string result = aString.Replace("\\", "\\\\");
            result = result.Replace("'", "\\'");
            result = result.Replace("`", "\\`");
            return result;
        }


        private static string UnescapeString(string aString)
        {
            string result = aString.Replace("\\\\", "\\");
            result = result.Replace("\\'", "'");
            result = result.Replace("\\`", "`");
            return result;
        }

        private MySqlConnection mConnection;
        private Schema mCurrentSchema;
        private MySqlTransaction mCurrentTransaction;
    }
}
