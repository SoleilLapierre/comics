using System.Windows.Forms;

namespace Comics
{
    public partial class SearchTreeViewControl : UserControl, ISeriesFilter, IIssueFilter
    {
        public delegate void SearchUpdatedEvent();
        public event SearchUpdatedEvent OnSearchUpdated;

        public SearchTreeViewControl()
        {
            InitializeComponent();
        }


        public bool IncludeIssue(IssueRecord aIssue)
        {
            foreach (SearchCard card in mSearchList.Controls)
            {
                if (!card.IncludeIssue(aIssue))
                {
                    return false;
                }
            }

            return true;
        }


        public bool IncludeSeries(SeriesRecord aSeries)
        {
            foreach (SearchCard card in mSearchList.Controls)
            {
                if (!card.IncludeSeries(aSeries))
                {
                    return false;
                }
            }

            return true;
        }


        private void mAddSearchButton_Click(object sender, System.EventArgs e)
        {
            var card = new SearchCard();
            card.OnSearchDeleted += (item) => mSearchList.Controls.Remove(item);
            card.OnSearchUpdated += (_) => OnSearchUpdated?.Invoke();
            mSearchList.Controls.Add(card);
        }
    }
}
