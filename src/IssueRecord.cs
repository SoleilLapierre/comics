using System;
using System.Collections.Generic;

namespace Comics
{
    public class IssueRecord : Record
    {
        #region ----- Constructors -----

        public IssueRecord()
        {
            mDateAdded = DateTime.Now;
        }


        public void CopyFrom(IssueRecord aOther)
        {
            mSeriesID = aOther.mSeriesID;
            mTitle = aOther.mTitle;
            mIssue = aOther.mIssue;
            mCost = aOther.mCost;
            mCondition = aOther.mCondition;
            mStorage = aOther.mStorage;
            mReadPrev = aOther.mReadPrev;
            mReadPrevRefs = new List<int>(aOther.mReadPrevRefs);
            mIssueType = aOther.mIssueType;
            mNumCopies = aOther.mNumCopies;
            mRefLinks = aOther.mRefLinks;
            mKeywords = aOther.mKeywords;
            mComments = aOther.mComments;
            mDateAdded = aOther.mDateAdded;
        }

        #endregion
        #region ----- Properties -----

        [StorageRequiredField]
        public int SeriesID
        {
            get { return mSeriesID; }
            set 
            { 
                mSeriesID = value;
                Dirty = true;
            }
        }

        public string Title
        {
            get { return mTitle; }
            set
            {
                mTitle = value;
                Dirty = true;
            }
        }

        public string Issue
        {
            get { return mIssue; }
            set
            {
                mIssue = value;
                Dirty = true;
            }
        }

        public decimal Cost
        {
            get { return mCost; }
            set
            {
                mCost = value;
                Dirty = true;
            }
        }

        public string Condition
        {
            get { return mCondition; }
            set
            {
                mCondition = value;
                Dirty = true;
            }
        }

        public string Storage
        {
            get { return mStorage; }
            set
            {
                mStorage = value;
                Dirty = true;
            }
        }

        public string ReadPrev
        {
            get { return mReadPrev; }
            set
            {
                mReadPrev = value;
                Dirty = true;
            }
        }

        public List<Int32> IssueRefsReadPrev
        {
            get 
            {
                return mReadPrevRefs; 
            }
            set
            {
                mReadPrevRefs = value;
                if (mReadPrevRefs == null)
                {
                    mReadPrevRefs = new List<int>();
                }

                Dirty = true;
            }
        }

        public string IssueType
        {
            get { return mIssueType; }
            set
            {
                mIssueType = value;
                Dirty = true;
            }
        }

        public UInt32 NumCopies
        {
            get { return mNumCopies; }
            set
            {
                mNumCopies = value;
                Dirty = true;
            }
        }

        public string ReferenceURL
        {
            get { return mRefLinks; }
            set
            {
                mRefLinks = value;
                Dirty = true;
            }
        }

        public string Keywords
        {
            get { return mKeywords; }
            set
            {
                mKeywords = value;
                Dirty = true;
            }
        }

        public string Comments
        {
            get { return mComments; }
            set
            {
                mComments = value;
                Dirty = true;
            }
        }

        public DateTime DateAdded
        {
            get { return mDateAdded; }
            set
            {
                mDateAdded = value;
                Dirty = true;
            }
        }

        #endregion
        #region ----- Data members -----

        private int mSeriesID = -1;
        private string mTitle;
        private string mIssue;
        private decimal mCost;
        private string mCondition;
        private string mStorage;
        private string mReadPrev;
        private string mKeywords;
        private string mComments;
        private string mIssueType;
        private UInt32 mNumCopies = 1;
        private string mRefLinks;
        private DateTime mDateAdded;
        private List<Int32> mReadPrevRefs = new List<int>();

        #endregion
    }
}
