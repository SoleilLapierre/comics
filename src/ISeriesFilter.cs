﻿namespace Comics
{
    public interface ISeriesFilter
    {
        bool IncludeSeries(SeriesRecord aSeries);
    }
}
