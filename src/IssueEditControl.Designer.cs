namespace Comics
{
    partial class IssueEditControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mIssuePropSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mTableLayoutContainer = new System.Windows.Forms.TableLayoutPanel();
            this.mIssuePropLabelSeries = new System.Windows.Forms.Label();
            this.mIssuePropLabelIssueNumber = new System.Windows.Forms.Label();
            this.mIssuePropTextKeywords = new System.Windows.Forms.TextBox();
            this.mIssuePropLabelIssueTitle = new System.Windows.Forms.Label();
            this.mIssuePropLabelCost = new System.Windows.Forms.Label();
            this.mIssuePropLabelCondition = new System.Windows.Forms.Label();
            this.mIssuePropLabelStorage = new System.Windows.Forms.Label();
            this.mIssuePropLabelDate = new System.Windows.Forms.Label();
            this.mIssuePropLabelComments = new System.Windows.Forms.Label();
            this.mIssuePropTextSeries = new System.Windows.Forms.TextBox();
            this.mIssuePropTextIssueNumber = new System.Windows.Forms.TextBox();
            this.mIssuePropTextTitle = new System.Windows.Forms.TextBox();
            this.mIssuePropTextCost = new System.Windows.Forms.NumericUpDown();
            this.mIssuePropTextCondition = new System.Windows.Forms.ComboBox();
            this.mIssuePropTextStorage = new System.Windows.Forms.TextBox();
            this.mIssuePropTextComments = new System.Windows.Forms.TextBox();
            this.mIssuePropTextDate = new System.Windows.Forms.DateTimePicker();
            this.mLabelDifferingCost = new System.Windows.Forms.Label();
            this.mLabelDifferingDate = new System.Windows.Forms.Label();
            this.mIssuePropLabelNumCopies = new System.Windows.Forms.Label();
            this.mIssuePropTextNumCopies = new System.Windows.Forms.NumericUpDown();
            this.mLabelDifferingNumCopies = new System.Windows.Forms.Label();
            this.mIssuePropLabelIssueType = new System.Windows.Forms.Label();
            this.mIssuePropTextIssueType = new System.Windows.Forms.ComboBox();
            this.mIssuePropLabelReferenceLinks = new System.Windows.Forms.Label();
            this.mIssuePropTextRefLinks = new System.Windows.Forms.TextBox();
            this.mIssuePropLabelKeywords = new System.Windows.Forms.Label();
            this.mIssuePropLabelPrereqs = new System.Windows.Forms.Label();
            this.mIssuePropTextPrevLinks = new System.Windows.Forms.TextBox();
            this.mIssuePropTextPrereqs = new System.Windows.Forms.TextBox();
            this.mApplyButton = new System.Windows.Forms.Button();
            this.mResetFormButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropSplitContainer)).BeginInit();
            this.mIssuePropSplitContainer.Panel1.SuspendLayout();
            this.mIssuePropSplitContainer.Panel2.SuspendLayout();
            this.mIssuePropSplitContainer.SuspendLayout();
            this.mTableLayoutContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropTextCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropTextNumCopies)).BeginInit();
            this.SuspendLayout();
            // 
            // mIssuePropSplitContainer
            // 
            this.mIssuePropSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.mIssuePropSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mIssuePropSplitContainer.Name = "mIssuePropSplitContainer";
            this.mIssuePropSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mIssuePropSplitContainer.Panel1
            // 
            this.mIssuePropSplitContainer.Panel1.Controls.Add(this.mTableLayoutContainer);
            this.mIssuePropSplitContainer.Panel1MinSize = 200;
            // 
            // mIssuePropSplitContainer.Panel2
            // 
            this.mIssuePropSplitContainer.Panel2.Controls.Add(this.mApplyButton);
            this.mIssuePropSplitContainer.Panel2.Controls.Add(this.mResetFormButton);
            this.mIssuePropSplitContainer.Panel2MinSize = 24;
            this.mIssuePropSplitContainer.Size = new System.Drawing.Size(456, 672);
            this.mIssuePropSplitContainer.SplitterDistance = 645;
            this.mIssuePropSplitContainer.SplitterWidth = 1;
            this.mIssuePropSplitContainer.TabIndex = 0;
            // 
            // mTableLayoutContainer
            // 
            this.mTableLayoutContainer.ColumnCount = 3;
            this.mTableLayoutContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.mTableLayoutContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mTableLayoutContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelSeries, 0, 0);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelIssueNumber, 0, 1);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextKeywords, 1, 10);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelIssueTitle, 0, 2);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelCost, 0, 4);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelCondition, 0, 6);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelStorage, 0, 8);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelDate, 0, 9);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelComments, 0, 13);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextSeries, 1, 0);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextIssueNumber, 1, 1);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextTitle, 1, 2);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextCost, 1, 4);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextCondition, 1, 6);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextStorage, 1, 8);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextComments, 1, 13);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextDate, 1, 9);
            this.mTableLayoutContainer.Controls.Add(this.mLabelDifferingCost, 2, 4);
            this.mTableLayoutContainer.Controls.Add(this.mLabelDifferingDate, 2, 9);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelNumCopies, 0, 5);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextNumCopies, 1, 5);
            this.mTableLayoutContainer.Controls.Add(this.mLabelDifferingNumCopies, 2, 5);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelIssueType, 0, 3);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextIssueType, 1, 3);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelReferenceLinks, 0, 7);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextRefLinks, 1, 7);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelKeywords, 0, 10);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropLabelPrereqs, 0, 11);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextPrevLinks, 1, 11);
            this.mTableLayoutContainer.Controls.Add(this.mIssuePropTextPrereqs, 1, 12);
            this.mTableLayoutContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTableLayoutContainer.Location = new System.Drawing.Point(0, 0);
            this.mTableLayoutContainer.Name = "mTableLayoutContainer";
            this.mTableLayoutContainer.RowCount = 14;
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.mTableLayoutContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mTableLayoutContainer.Size = new System.Drawing.Size(456, 645);
            this.mTableLayoutContainer.TabIndex = 0;
            // 
            // mIssuePropLabelSeries
            // 
            this.mIssuePropLabelSeries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelSeries.AutoSize = true;
            this.mIssuePropLabelSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelSeries.Location = new System.Drawing.Point(101, 5);
            this.mIssuePropLabelSeries.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelSeries.Name = "mIssuePropLabelSeries";
            this.mIssuePropLabelSeries.Size = new System.Drawing.Size(42, 13);
            this.mIssuePropLabelSeries.TabIndex = 0;
            this.mIssuePropLabelSeries.Text = "Series";
            // 
            // mIssuePropLabelIssueNumber
            // 
            this.mIssuePropLabelIssueNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelIssueNumber.AutoSize = true;
            this.mIssuePropLabelIssueNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelIssueNumber.Location = new System.Drawing.Point(94, 29);
            this.mIssuePropLabelIssueNumber.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelIssueNumber.Name = "mIssuePropLabelIssueNumber";
            this.mIssuePropLabelIssueNumber.Size = new System.Drawing.Size(49, 13);
            this.mIssuePropLabelIssueNumber.TabIndex = 1;
            this.mIssuePropLabelIssueNumber.Text = "Issue #";
            // 
            // mIssuePropTextKeywords
            // 
            this.mIssuePropTextKeywords.AcceptsReturn = true;
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextKeywords, 2);
            this.mIssuePropTextKeywords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextKeywords.Location = new System.Drawing.Point(149, 285);
            this.mIssuePropTextKeywords.Multiline = true;
            this.mIssuePropTextKeywords.Name = "mIssuePropTextKeywords";
            this.mIssuePropTextKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mIssuePropTextKeywords.Size = new System.Drawing.Size(304, 50);
            this.mIssuePropTextKeywords.TabIndex = 16;
            // 
            // mIssuePropLabelIssueTitle
            // 
            this.mIssuePropLabelIssueTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelIssueTitle.AutoSize = true;
            this.mIssuePropLabelIssueTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelIssueTitle.Location = new System.Drawing.Point(5, 53);
            this.mIssuePropLabelIssueTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelIssueTitle.Name = "mIssuePropLabelIssueTitle";
            this.mIssuePropLabelIssueTitle.Size = new System.Drawing.Size(138, 26);
            this.mIssuePropLabelIssueTitle.TabIndex = 2;
            this.mIssuePropLabelIssueTitle.Text = "Title\n(if different from series)";
            this.mIssuePropLabelIssueTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // mIssuePropLabelCost
            // 
            this.mIssuePropLabelCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelCost.AutoSize = true;
            this.mIssuePropLabelCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelCost.Location = new System.Drawing.Point(111, 111);
            this.mIssuePropLabelCost.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelCost.Name = "mIssuePropLabelCost";
            this.mIssuePropLabelCost.Size = new System.Drawing.Size(32, 13);
            this.mIssuePropLabelCost.TabIndex = 3;
            this.mIssuePropLabelCost.Text = "Cost";
            // 
            // mIssuePropLabelCondition
            // 
            this.mIssuePropLabelCondition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelCondition.AutoSize = true;
            this.mIssuePropLabelCondition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelCondition.Location = new System.Drawing.Point(83, 159);
            this.mIssuePropLabelCondition.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelCondition.Name = "mIssuePropLabelCondition";
            this.mIssuePropLabelCondition.Size = new System.Drawing.Size(60, 13);
            this.mIssuePropLabelCondition.TabIndex = 4;
            this.mIssuePropLabelCondition.Text = "Condition";
            // 
            // mIssuePropLabelStorage
            // 
            this.mIssuePropLabelStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelStorage.AutoSize = true;
            this.mIssuePropLabelStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelStorage.Location = new System.Drawing.Point(39, 239);
            this.mIssuePropLabelStorage.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelStorage.Name = "mIssuePropLabelStorage";
            this.mIssuePropLabelStorage.Size = new System.Drawing.Size(104, 13);
            this.mIssuePropLabelStorage.TabIndex = 5;
            this.mIssuePropLabelStorage.Text = "Storage Location";
            // 
            // mIssuePropLabelDate
            // 
            this.mIssuePropLabelDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelDate.AutoSize = true;
            this.mIssuePropLabelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelDate.Location = new System.Drawing.Point(69, 263);
            this.mIssuePropLabelDate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelDate.Name = "mIssuePropLabelDate";
            this.mIssuePropLabelDate.Size = new System.Drawing.Size(74, 13);
            this.mIssuePropLabelDate.TabIndex = 6;
            this.mIssuePropLabelDate.Text = "Date Added";
            // 
            // mIssuePropLabelComments
            // 
            this.mIssuePropLabelComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelComments.AutoSize = true;
            this.mIssuePropLabelComments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelComments.Location = new System.Drawing.Point(79, 481);
            this.mIssuePropLabelComments.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelComments.Name = "mIssuePropLabelComments";
            this.mIssuePropLabelComments.Size = new System.Drawing.Size(64, 13);
            this.mIssuePropLabelComments.TabIndex = 9;
            this.mIssuePropLabelComments.Text = "Comments";
            // 
            // mIssuePropTextSeries
            // 
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextSeries, 2);
            this.mIssuePropTextSeries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextSeries.Location = new System.Drawing.Point(149, 3);
            this.mIssuePropTextSeries.Name = "mIssuePropTextSeries";
            this.mIssuePropTextSeries.ReadOnly = true;
            this.mIssuePropTextSeries.Size = new System.Drawing.Size(304, 20);
            this.mIssuePropTextSeries.TabIndex = 10;
            // 
            // mIssuePropTextIssueNumber
            // 
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextIssueNumber, 2);
            this.mIssuePropTextIssueNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextIssueNumber.Location = new System.Drawing.Point(149, 27);
            this.mIssuePropTextIssueNumber.MaxLength = 256;
            this.mIssuePropTextIssueNumber.Name = "mIssuePropTextIssueNumber";
            this.mIssuePropTextIssueNumber.Size = new System.Drawing.Size(304, 20);
            this.mIssuePropTextIssueNumber.TabIndex = 11;
            this.mIssuePropTextIssueNumber.WordWrap = false;
            // 
            // mIssuePropTextTitle
            // 
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextTitle, 2);
            this.mIssuePropTextTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextTitle.Location = new System.Drawing.Point(149, 55);
            this.mIssuePropTextTitle.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.mIssuePropTextTitle.MaxLength = 2048;
            this.mIssuePropTextTitle.Name = "mIssuePropTextTitle";
            this.mIssuePropTextTitle.Size = new System.Drawing.Size(304, 20);
            this.mIssuePropTextTitle.TabIndex = 12;
            this.mIssuePropTextTitle.WordWrap = false;
            // 
            // mIssuePropTextCost
            // 
            this.mIssuePropTextCost.DecimalPlaces = 2;
            this.mIssuePropTextCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextCost.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mIssuePropTextCost.Location = new System.Drawing.Point(149, 109);
            this.mIssuePropTextCost.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.mIssuePropTextCost.Name = "mIssuePropTextCost";
            this.mIssuePropTextCost.Size = new System.Drawing.Size(204, 20);
            this.mIssuePropTextCost.TabIndex = 13;
            this.mIssuePropTextCost.ThousandsSeparator = true;
            this.mIssuePropTextCost.ValueChanged += new System.EventHandler(this.mIssuePropTextCost_ValueChanged);
            // 
            // mIssuePropTextCondition
            // 
            this.mIssuePropTextCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextCondition.FormattingEnabled = true;
            this.mIssuePropTextCondition.Items.AddRange(new object[] {
            "Mint",
            "Near Mint",
            "Very Fine",
            "Fine",
            "Very Good",
            "Good",
            "Fair",
            "Poor"});
            this.mIssuePropTextCondition.Location = new System.Drawing.Point(149, 157);
            this.mIssuePropTextCondition.Name = "mIssuePropTextCondition";
            this.mIssuePropTextCondition.Size = new System.Drawing.Size(204, 21);
            this.mIssuePropTextCondition.TabIndex = 14;
            // 
            // mIssuePropTextStorage
            // 
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextStorage, 2);
            this.mIssuePropTextStorage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextStorage.Location = new System.Drawing.Point(149, 237);
            this.mIssuePropTextStorage.MaxLength = 2048;
            this.mIssuePropTextStorage.Name = "mIssuePropTextStorage";
            this.mIssuePropTextStorage.Size = new System.Drawing.Size(304, 20);
            this.mIssuePropTextStorage.TabIndex = 15;
            this.mIssuePropTextStorage.WordWrap = false;
            // 
            // mIssuePropTextComments
            // 
            this.mIssuePropTextComments.AcceptsReturn = true;
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextComments, 2);
            this.mIssuePropTextComments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextComments.Location = new System.Drawing.Point(149, 479);
            this.mIssuePropTextComments.MaxLength = 65535;
            this.mIssuePropTextComments.Multiline = true;
            this.mIssuePropTextComments.Name = "mIssuePropTextComments";
            this.mIssuePropTextComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mIssuePropTextComments.Size = new System.Drawing.Size(304, 163);
            this.mIssuePropTextComments.TabIndex = 18;
            // 
            // mIssuePropTextDate
            // 
            this.mIssuePropTextDate.CustomFormat = "yyyy MMM dd";
            this.mIssuePropTextDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.mIssuePropTextDate.Location = new System.Drawing.Point(149, 261);
            this.mIssuePropTextDate.Name = "mIssuePropTextDate";
            this.mIssuePropTextDate.ShowCheckBox = true;
            this.mIssuePropTextDate.Size = new System.Drawing.Size(204, 20);
            this.mIssuePropTextDate.TabIndex = 19;
            this.mIssuePropTextDate.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // mLabelDifferingCost
            // 
            this.mLabelDifferingCost.AutoSize = true;
            this.mLabelDifferingCost.Location = new System.Drawing.Point(359, 111);
            this.mLabelDifferingCost.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mLabelDifferingCost.Name = "mLabelDifferingCost";
            this.mLabelDifferingCost.Size = new System.Drawing.Size(76, 13);
            this.mLabelDifferingCost.TabIndex = 20;
            this.mLabelDifferingCost.Text = "<DIFFERING>";
            this.mLabelDifferingCost.Visible = false;
            // 
            // mLabelDifferingDate
            // 
            this.mLabelDifferingDate.AutoSize = true;
            this.mLabelDifferingDate.Location = new System.Drawing.Point(359, 263);
            this.mLabelDifferingDate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mLabelDifferingDate.Name = "mLabelDifferingDate";
            this.mLabelDifferingDate.Size = new System.Drawing.Size(76, 13);
            this.mLabelDifferingDate.TabIndex = 21;
            this.mLabelDifferingDate.Text = "<DIFFERING>";
            this.mLabelDifferingDate.Visible = false;
            // 
            // mIssuePropLabelNumCopies
            // 
            this.mIssuePropLabelNumCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelNumCopies.AutoSize = true;
            this.mIssuePropLabelNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelNumCopies.Location = new System.Drawing.Point(86, 135);
            this.mIssuePropLabelNumCopies.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelNumCopies.Name = "mIssuePropLabelNumCopies";
            this.mIssuePropLabelNumCopies.Size = new System.Drawing.Size(57, 13);
            this.mIssuePropLabelNumCopies.TabIndex = 22;
            this.mIssuePropLabelNumCopies.Text = "# Copies";
            // 
            // mIssuePropTextNumCopies
            // 
            this.mIssuePropTextNumCopies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextNumCopies.Location = new System.Drawing.Point(149, 133);
            this.mIssuePropTextNumCopies.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.mIssuePropTextNumCopies.Name = "mIssuePropTextNumCopies";
            this.mIssuePropTextNumCopies.Size = new System.Drawing.Size(204, 20);
            this.mIssuePropTextNumCopies.TabIndex = 23;
            this.mIssuePropTextNumCopies.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mIssuePropTextNumCopies.ValueChanged += new System.EventHandler(this.mIssuePropTextNumCopies_ValueChanged);
            // 
            // mLabelDifferingNumCopies
            // 
            this.mLabelDifferingNumCopies.AutoSize = true;
            this.mLabelDifferingNumCopies.Location = new System.Drawing.Point(359, 135);
            this.mLabelDifferingNumCopies.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mLabelDifferingNumCopies.Name = "mLabelDifferingNumCopies";
            this.mLabelDifferingNumCopies.Size = new System.Drawing.Size(76, 13);
            this.mLabelDifferingNumCopies.TabIndex = 24;
            this.mLabelDifferingNumCopies.Text = "<DIFFERING>";
            this.mLabelDifferingNumCopies.Visible = false;
            // 
            // mIssuePropLabelIssueType
            // 
            this.mIssuePropLabelIssueType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelIssueType.AutoSize = true;
            this.mIssuePropLabelIssueType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelIssueType.Location = new System.Drawing.Point(74, 87);
            this.mIssuePropLabelIssueType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelIssueType.Name = "mIssuePropLabelIssueType";
            this.mIssuePropLabelIssueType.Size = new System.Drawing.Size(69, 13);
            this.mIssuePropLabelIssueType.TabIndex = 25;
            this.mIssuePropLabelIssueType.Text = "Issue Type";
            // 
            // mIssuePropTextIssueType
            // 
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextIssueType, 2);
            this.mIssuePropTextIssueType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextIssueType.FormattingEnabled = true;
            this.mIssuePropTextIssueType.Items.AddRange(new object[] {
            "",
            "Annual",
            "Special"});
            this.mIssuePropTextIssueType.Location = new System.Drawing.Point(149, 85);
            this.mIssuePropTextIssueType.Name = "mIssuePropTextIssueType";
            this.mIssuePropTextIssueType.Size = new System.Drawing.Size(304, 21);
            this.mIssuePropTextIssueType.TabIndex = 26;
            // 
            // mIssuePropLabelReferenceLinks
            // 
            this.mIssuePropLabelReferenceLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelReferenceLinks.AutoSize = true;
            this.mIssuePropLabelReferenceLinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelReferenceLinks.Location = new System.Drawing.Point(43, 183);
            this.mIssuePropLabelReferenceLinks.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelReferenceLinks.Name = "mIssuePropLabelReferenceLinks";
            this.mIssuePropLabelReferenceLinks.Size = new System.Drawing.Size(100, 13);
            this.mIssuePropLabelReferenceLinks.TabIndex = 27;
            this.mIssuePropLabelReferenceLinks.Text = "Reference Links";
            // 
            // mIssuePropTextRefLinks
            // 
            this.mIssuePropTextRefLinks.AcceptsReturn = true;
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextRefLinks, 2);
            this.mIssuePropTextRefLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextRefLinks.Location = new System.Drawing.Point(149, 181);
            this.mIssuePropTextRefLinks.Multiline = true;
            this.mIssuePropTextRefLinks.Name = "mIssuePropTextRefLinks";
            this.mIssuePropTextRefLinks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mIssuePropTextRefLinks.Size = new System.Drawing.Size(304, 50);
            this.mIssuePropTextRefLinks.TabIndex = 28;
            this.mIssuePropTextRefLinks.WordWrap = false;
            // 
            // mIssuePropLabelKeywords
            // 
            this.mIssuePropLabelKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelKeywords.AutoSize = true;
            this.mIssuePropLabelKeywords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelKeywords.Location = new System.Drawing.Point(82, 287);
            this.mIssuePropLabelKeywords.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelKeywords.Name = "mIssuePropLabelKeywords";
            this.mIssuePropLabelKeywords.Size = new System.Drawing.Size(61, 13);
            this.mIssuePropLabelKeywords.TabIndex = 7;
            this.mIssuePropLabelKeywords.Text = "Keywords";
            // 
            // mIssuePropLabelPrereqs
            // 
            this.mIssuePropLabelPrereqs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mIssuePropLabelPrereqs.AutoSize = true;
            this.mIssuePropLabelPrereqs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mIssuePropLabelPrereqs.Location = new System.Drawing.Point(12, 343);
            this.mIssuePropLabelPrereqs.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.mIssuePropLabelPrereqs.Name = "mIssuePropLabelPrereqs";
            this.mIssuePropLabelPrereqs.Size = new System.Drawing.Size(131, 13);
            this.mIssuePropLabelPrereqs.TabIndex = 8;
            this.mIssuePropLabelPrereqs.Text = "Reading Prerequisites";
            // 
            // mIssuePropTextPrevLinks
            // 
            this.mIssuePropTextPrevLinks.AllowDrop = true;
            this.mIssuePropTextPrevLinks.BackColor = System.Drawing.SystemColors.Info;
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextPrevLinks, 2);
            this.mIssuePropTextPrevLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextPrevLinks.Location = new System.Drawing.Point(149, 341);
            this.mIssuePropTextPrevLinks.Multiline = true;
            this.mIssuePropTextPrevLinks.Name = "mIssuePropTextPrevLinks";
            this.mIssuePropTextPrevLinks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mIssuePropTextPrevLinks.Size = new System.Drawing.Size(304, 50);
            this.mIssuePropTextPrevLinks.TabIndex = 29;
            this.mIssuePropTextPrevLinks.WordWrap = false;
            this.mIssuePropTextPrevLinks.DragDrop += new System.Windows.Forms.DragEventHandler(this.mIssuePropTextPrevLinks_DragDrop);
            this.mIssuePropTextPrevLinks.DragEnter += new System.Windows.Forms.DragEventHandler(this.mIssuePropTextPrevLinks_DragEnter);
            this.mIssuePropTextPrevLinks.DragOver += new System.Windows.Forms.DragEventHandler(this.mIssuePropTextPrevLinks_DragEnter);
            this.mIssuePropTextPrevLinks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mIssuePropTextPrevLinks_KeyDown);
            // 
            // mIssuePropTextPrereqs
            // 
            this.mIssuePropTextPrereqs.AcceptsReturn = true;
            this.mTableLayoutContainer.SetColumnSpan(this.mIssuePropTextPrereqs, 2);
            this.mIssuePropTextPrereqs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssuePropTextPrereqs.Location = new System.Drawing.Point(149, 397);
            this.mIssuePropTextPrereqs.Multiline = true;
            this.mIssuePropTextPrereqs.Name = "mIssuePropTextPrereqs";
            this.mIssuePropTextPrereqs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mIssuePropTextPrereqs.Size = new System.Drawing.Size(304, 76);
            this.mIssuePropTextPrereqs.TabIndex = 17;
            // 
            // mApplyButton
            // 
            this.mApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mApplyButton.Location = new System.Drawing.Point(297, 5);
            this.mApplyButton.Name = "mApplyButton";
            this.mApplyButton.Size = new System.Drawing.Size(75, 23);
            this.mApplyButton.TabIndex = 1;
            this.mApplyButton.Text = "Apply";
            this.mApplyButton.UseVisualStyleBackColor = true;
            this.mApplyButton.Click += new System.EventHandler(this.mApplyButton_Click);
            // 
            // mResetFormButton
            // 
            this.mResetFormButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mResetFormButton.Location = new System.Drawing.Point(378, 5);
            this.mResetFormButton.Name = "mResetFormButton";
            this.mResetFormButton.Size = new System.Drawing.Size(75, 23);
            this.mResetFormButton.TabIndex = 0;
            this.mResetFormButton.Text = "Reset";
            this.mResetFormButton.UseVisualStyleBackColor = true;
            this.mResetFormButton.Click += new System.EventHandler(this.mCancelButton_Click);
            // 
            // IssueEditControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mIssuePropSplitContainer);
            this.Name = "IssueEditControl";
            this.Size = new System.Drawing.Size(456, 672);
            this.mIssuePropSplitContainer.Panel1.ResumeLayout(false);
            this.mIssuePropSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropSplitContainer)).EndInit();
            this.mIssuePropSplitContainer.ResumeLayout(false);
            this.mTableLayoutContainer.ResumeLayout(false);
            this.mTableLayoutContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropTextCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mIssuePropTextNumCopies)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mIssuePropSplitContainer;
        private System.Windows.Forms.TableLayoutPanel mTableLayoutContainer;
        private System.Windows.Forms.Button mApplyButton;
        private System.Windows.Forms.Button mResetFormButton;
        private System.Windows.Forms.Label mIssuePropLabelSeries;
        private System.Windows.Forms.Label mIssuePropLabelIssueNumber;
        private System.Windows.Forms.Label mIssuePropLabelIssueTitle;
        private System.Windows.Forms.Label mIssuePropLabelCost;
        private System.Windows.Forms.Label mIssuePropLabelCondition;
        private System.Windows.Forms.Label mIssuePropLabelStorage;
        private System.Windows.Forms.Label mIssuePropLabelDate;
        private System.Windows.Forms.Label mIssuePropLabelKeywords;
        private System.Windows.Forms.Label mIssuePropLabelPrereqs;
        private System.Windows.Forms.Label mIssuePropLabelComments;
        private System.Windows.Forms.TextBox mIssuePropTextSeries;
        private System.Windows.Forms.TextBox mIssuePropTextIssueNumber;
        private System.Windows.Forms.TextBox mIssuePropTextTitle;
        private System.Windows.Forms.NumericUpDown mIssuePropTextCost;
        private System.Windows.Forms.ComboBox mIssuePropTextCondition;
        private System.Windows.Forms.TextBox mIssuePropTextStorage;
        private System.Windows.Forms.TextBox mIssuePropTextKeywords;
        private System.Windows.Forms.TextBox mIssuePropTextPrereqs;
        private System.Windows.Forms.TextBox mIssuePropTextComments;
        private System.Windows.Forms.DateTimePicker mIssuePropTextDate;
        private System.Windows.Forms.Label mLabelDifferingCost;
        private System.Windows.Forms.Label mLabelDifferingDate;
        private System.Windows.Forms.Label mIssuePropLabelNumCopies;
        private System.Windows.Forms.NumericUpDown mIssuePropTextNumCopies;
        private System.Windows.Forms.Label mLabelDifferingNumCopies;
        private System.Windows.Forms.Label mIssuePropLabelIssueType;
        private System.Windows.Forms.ComboBox mIssuePropTextIssueType;
        private System.Windows.Forms.Label mIssuePropLabelReferenceLinks;
        private System.Windows.Forms.TextBox mIssuePropTextRefLinks;
        private System.Windows.Forms.TextBox mIssuePropTextPrevLinks;



    }
}
