namespace Comics
{
    partial class SeriesPropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mSortKeyBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mTitleLabel = new System.Windows.Forms.Label();
            this.mAltTitleLabel = new System.Windows.Forms.Label();
            this.mPublisherLabel = new System.Windows.Forms.Label();
            this.mVolumeLabel = new System.Windows.Forms.Label();
            this.mFormatLabel = new System.Windows.Forms.Label();
            this.mCommentLabel = new System.Windows.Forms.Label();
            this.mKeywordsLabel = new System.Windows.Forms.Label();
            this.mTitleText = new System.Windows.Forms.TextBox();
            this.mAltTitleText = new System.Windows.Forms.TextBox();
            this.mPublisherText = new System.Windows.Forms.TextBox();
            this.mVolumeText = new System.Windows.Forms.TextBox();
            this.mFormatText = new System.Windows.Forms.TextBox();
            this.mKeywordsText = new System.Windows.Forms.TextBox();
            this.mCommentsText = new System.Windows.Forms.TextBox();
            this.mLsLengthLabel = new System.Windows.Forms.Label();
            this.mLsLengthBox = new System.Windows.Forms.NumericUpDown();
            this.mLabelRefLinks = new System.Windows.Forms.Label();
            this.mTextRefLinks = new System.Windows.Forms.TextBox();
            this.mCancelButton = new System.Windows.Forms.Button();
            this.mOKButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLsLengthBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.mSortKeyBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.mTitleLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mAltTitleLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.mPublisherLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.mVolumeLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.mFormatLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.mCommentLabel, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.mKeywordsLabel, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.mTitleText, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.mAltTitleText, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.mPublisherText, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.mVolumeText, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.mFormatText, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.mKeywordsText, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.mCommentsText, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.mLsLengthLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.mLsLengthBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.mLabelRefLinks, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.mTextRefLinks, 1, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(367, 414);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mSortKeyBox
            // 
            this.mSortKeyBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSortKeyBox.Location = new System.Drawing.Point(104, 389);
            this.mSortKeyBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mSortKeyBox.Name = "mSortKeyBox";
            this.mSortKeyBox.Size = new System.Drawing.Size(272, 20);
            this.mSortKeyBox.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 392);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Sort Key";
            // 
            // mTitleLabel
            // 
            this.mTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mTitleLabel.AutoSize = true;
            this.mTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mTitleLabel.Location = new System.Drawing.Point(66, 8);
            this.mTitleLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mTitleLabel.Name = "mTitleLabel";
            this.mTitleLabel.Size = new System.Drawing.Size(32, 13);
            this.mTitleLabel.TabIndex = 0;
            this.mTitleLabel.Text = "Title";
            // 
            // mAltTitleLabel
            // 
            this.mAltTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mAltTitleLabel.AutoSize = true;
            this.mAltTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mAltTitleLabel.Location = new System.Drawing.Point(15, 40);
            this.mAltTitleLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mAltTitleLabel.Name = "mAltTitleLabel";
            this.mAltTitleLabel.Size = new System.Drawing.Size(83, 13);
            this.mAltTitleLabel.TabIndex = 1;
            this.mAltTitleLabel.Text = "Sub/Alt. Title";
            // 
            // mPublisherLabel
            // 
            this.mPublisherLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mPublisherLabel.AutoSize = true;
            this.mPublisherLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mPublisherLabel.Location = new System.Drawing.Point(39, 72);
            this.mPublisherLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mPublisherLabel.Name = "mPublisherLabel";
            this.mPublisherLabel.Size = new System.Drawing.Size(59, 13);
            this.mPublisherLabel.TabIndex = 2;
            this.mPublisherLabel.Text = "Publisher";
            // 
            // mVolumeLabel
            // 
            this.mVolumeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mVolumeLabel.AutoSize = true;
            this.mVolumeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mVolumeLabel.Location = new System.Drawing.Point(50, 104);
            this.mVolumeLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mVolumeLabel.Name = "mVolumeLabel";
            this.mVolumeLabel.Size = new System.Drawing.Size(48, 13);
            this.mVolumeLabel.TabIndex = 3;
            this.mVolumeLabel.Text = "Volume";
            // 
            // mFormatLabel
            // 
            this.mFormatLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mFormatLabel.AutoSize = true;
            this.mFormatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mFormatLabel.Location = new System.Drawing.Point(53, 136);
            this.mFormatLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mFormatLabel.Name = "mFormatLabel";
            this.mFormatLabel.Size = new System.Drawing.Size(45, 13);
            this.mFormatLabel.TabIndex = 4;
            this.mFormatLabel.Text = "Format";
            // 
            // mCommentLabel
            // 
            this.mCommentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mCommentLabel.AutoSize = true;
            this.mCommentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mCommentLabel.Location = new System.Drawing.Point(34, 296);
            this.mCommentLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mCommentLabel.Name = "mCommentLabel";
            this.mCommentLabel.Size = new System.Drawing.Size(64, 13);
            this.mCommentLabel.TabIndex = 5;
            this.mCommentLabel.Text = "Comments";
            // 
            // mKeywordsLabel
            // 
            this.mKeywordsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mKeywordsLabel.AutoSize = true;
            this.mKeywordsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mKeywordsLabel.Location = new System.Drawing.Point(37, 232);
            this.mKeywordsLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mKeywordsLabel.Name = "mKeywordsLabel";
            this.mKeywordsLabel.Size = new System.Drawing.Size(61, 13);
            this.mKeywordsLabel.TabIndex = 6;
            this.mKeywordsLabel.Text = "Keywords";
            // 
            // mTitleText
            // 
            this.mTitleText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mTitleText.Location = new System.Drawing.Point(104, 5);
            this.mTitleText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mTitleText.Name = "mTitleText";
            this.mTitleText.Size = new System.Drawing.Size(272, 20);
            this.mTitleText.TabIndex = 0;
            this.mTitleText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mAltTitleText
            // 
            this.mAltTitleText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mAltTitleText.Location = new System.Drawing.Point(104, 37);
            this.mAltTitleText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mAltTitleText.Name = "mAltTitleText";
            this.mAltTitleText.Size = new System.Drawing.Size(272, 20);
            this.mAltTitleText.TabIndex = 1;
            this.mAltTitleText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mPublisherText
            // 
            this.mPublisherText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mPublisherText.Location = new System.Drawing.Point(104, 69);
            this.mPublisherText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mPublisherText.Name = "mPublisherText";
            this.mPublisherText.Size = new System.Drawing.Size(272, 20);
            this.mPublisherText.TabIndex = 2;
            this.mPublisherText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mVolumeText
            // 
            this.mVolumeText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mVolumeText.Location = new System.Drawing.Point(104, 101);
            this.mVolumeText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mVolumeText.Name = "mVolumeText";
            this.mVolumeText.Size = new System.Drawing.Size(272, 20);
            this.mVolumeText.TabIndex = 3;
            this.mVolumeText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mFormatText
            // 
            this.mFormatText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mFormatText.Location = new System.Drawing.Point(104, 133);
            this.mFormatText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mFormatText.Name = "mFormatText";
            this.mFormatText.Size = new System.Drawing.Size(272, 20);
            this.mFormatText.TabIndex = 5;
            this.mFormatText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mKeywordsText
            // 
            this.mKeywordsText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mKeywordsText.Location = new System.Drawing.Point(104, 229);
            this.mKeywordsText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.mKeywordsText.Multiline = true;
            this.mKeywordsText.Name = "mKeywordsText";
            this.mKeywordsText.Size = new System.Drawing.Size(272, 54);
            this.mKeywordsText.TabIndex = 7;
            this.mKeywordsText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mCommentsText
            // 
            this.mCommentsText.AcceptsReturn = true;
            this.mCommentsText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mCommentsText.Location = new System.Drawing.Point(104, 293);
            this.mCommentsText.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.mCommentsText.Multiline = true;
            this.mCommentsText.Name = "mCommentsText";
            this.mCommentsText.Size = new System.Drawing.Size(272, 86);
            this.mCommentsText.TabIndex = 8;
            this.mCommentsText.Validating += new System.ComponentModel.CancelEventHandler(this.Validate);
            // 
            // mLsLengthLabel
            // 
            this.mLsLengthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mLsLengthLabel.AutoSize = true;
            this.mLsLengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mLsLengthLabel.Location = new System.Drawing.Point(33, 168);
            this.mLsLengthLabel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mLsLengthLabel.Name = "mLsLengthLabel";
            this.mLsLengthLabel.Size = new System.Drawing.Size(65, 13);
            this.mLsLengthLabel.TabIndex = 14;
            this.mLsLengthLabel.Text = "LS Length";
            // 
            // mLsLengthBox
            // 
            this.mLsLengthBox.Location = new System.Drawing.Point(104, 165);
            this.mLsLengthBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mLsLengthBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mLsLengthBox.Name = "mLsLengthBox";
            this.mLsLengthBox.Size = new System.Drawing.Size(85, 20);
            this.mLsLengthBox.TabIndex = 4;
            // 
            // mLabelRefLinks
            // 
            this.mLabelRefLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mLabelRefLinks.AutoSize = true;
            this.mLabelRefLinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mLabelRefLinks.Location = new System.Drawing.Point(3, 200);
            this.mLabelRefLinks.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.mLabelRefLinks.Name = "mLabelRefLinks";
            this.mLabelRefLinks.Size = new System.Drawing.Size(95, 13);
            this.mLabelRefLinks.TabIndex = 16;
            this.mLabelRefLinks.Text = "Reference URL";
            // 
            // mTextRefLinks
            // 
            this.mTextRefLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTextRefLinks.Location = new System.Drawing.Point(104, 197);
            this.mTextRefLinks.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.mTextRefLinks.Name = "mTextRefLinks";
            this.mTextRefLinks.Size = new System.Drawing.Size(272, 20);
            this.mTextRefLinks.TabIndex = 6;
            // 
            // mCancelButton
            // 
            this.mCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mCancelButton.CausesValidation = false;
            this.mCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.mCancelButton.Location = new System.Drawing.Point(304, 450);
            this.mCancelButton.Name = "mCancelButton";
            this.mCancelButton.Size = new System.Drawing.Size(75, 23);
            this.mCancelButton.TabIndex = 2;
            this.mCancelButton.Text = "Cancel";
            this.mCancelButton.UseVisualStyleBackColor = true;
            // 
            // mOKButton
            // 
            this.mOKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mOKButton.CausesValidation = false;
            this.mOKButton.Enabled = false;
            this.mOKButton.Location = new System.Drawing.Point(223, 450);
            this.mOKButton.Name = "mOKButton";
            this.mOKButton.Size = new System.Drawing.Size(75, 23);
            this.mOKButton.TabIndex = 1;
            this.mOKButton.Text = "OK";
            this.mOKButton.UseVisualStyleBackColor = true;
            this.mOKButton.Click += new System.EventHandler(this.mOKButton_Click);
            // 
            // SeriesPropertiesForm
            // 
            this.AcceptButton = this.mOKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.mCancelButton;
            this.ClientSize = new System.Drawing.Size(391, 485);
            this.ControlBox = false;
            this.Controls.Add(this.mOKButton);
            this.Controls.Add(this.mCancelButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SeriesPropertiesForm";
            this.ShowInTaskbar = false;
            this.Text = "Series Properties";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLsLengthBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button mCancelButton;
        private System.Windows.Forms.Button mOKButton;
        private System.Windows.Forms.Label mTitleLabel;
        private System.Windows.Forms.Label mAltTitleLabel;
        private System.Windows.Forms.Label mPublisherLabel;
        private System.Windows.Forms.Label mVolumeLabel;
        private System.Windows.Forms.Label mFormatLabel;
        private System.Windows.Forms.Label mCommentLabel;
        private System.Windows.Forms.Label mKeywordsLabel;
        private System.Windows.Forms.TextBox mTitleText;
        private System.Windows.Forms.TextBox mAltTitleText;
        private System.Windows.Forms.TextBox mPublisherText;
        private System.Windows.Forms.TextBox mVolumeText;
        private System.Windows.Forms.TextBox mFormatText;
        private System.Windows.Forms.TextBox mKeywordsText;
        private System.Windows.Forms.TextBox mCommentsText;
        private System.Windows.Forms.Label mLsLengthLabel;
        private System.Windows.Forms.NumericUpDown mLsLengthBox;
        private System.Windows.Forms.Label mLabelRefLinks;
        private System.Windows.Forms.TextBox mTextRefLinks;
        private System.Windows.Forms.TextBox mSortKeyBox;
        private System.Windows.Forms.Label label1;
    }
}