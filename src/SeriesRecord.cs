using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Comics
{
    public class SeriesRecord : Record
    {
        [StorageRequiredField(true)]
        public string Title
        {
            get { return mTitle; }
            set
            {
                mTitle = value;
                Dirty = true;
            }
        }

        public string Publisher
        {
            get { return mPublisher; }
            set
            {
                mPublisher = value;
                Dirty = true;
            }
        }

        public string Format
        {
            get { return mFormat; }
            set
            {
                mFormat = value;
                Dirty = true;
            }
        }

        public string Volume
        {
            get { return mVolume; }
            set
            {
                mVolume = value;
                Dirty = true;
            }
        }

        public int LimitedSeriesLength
        {
            get { return mLsLength; }
            set
            {
                mLsLength = value;
                Dirty = true;
            }
        }

        public string AltTitle
        {
            get { return mAltTitle; }
            set
            {
                mAltTitle = value;
                Dirty = true;
            }
        }

        public int ParentKey
        {
            get { return mParentKey; }
            set
            {
                mParentKey = value;
                Dirty = true;
            }
        }

        public string ReferenceURL
        {
            get { return mRefLinks; }
            set
            {
                mRefLinks = value;
                Dirty = true;
            }
        }

        public string Keywords
        {
            get { return mKeywords; }
            set
            {
                mKeywords = value;
                Dirty = true;
            }
        }

        public string Comments
        {
            get { return mComments; }
            set
            {
                mComments = value;
                Dirty = true;
            }
        }

        public string SortKey
        {
            get { return mSortKey; }
            set
            {
                mSortKey = value;
                Dirty = true;
            }
        }

        public void CopyFrom(SeriesRecord aOther)
        {
            mTitle = aOther.mTitle;
            mPublisher = aOther.mPublisher;
            mFormat = aOther.mFormat;
            mVolume = aOther.mVolume;
            mLsLength = aOther.mLsLength;
            mAltTitle = aOther.mAltTitle;
            mParentKey = aOther.mParentKey;
            mRefLinks = aOther.mRefLinks;
            mKeywords = aOther.mKeywords;
            mComments = aOther.mComments;
            mSortKey = aOther.mSortKey;
            mIssues = new List<int>(aOther.mIssues);
            Dirty = true;
        }

        [XmlIgnore]
        public List<int> Issues => mIssues;

        [XmlIgnore]
        public string FullTitle
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(mTitle);

                if (mAltTitle != null)
                {
                    sb.Append(": ");
                    sb.Append(mAltTitle);
                }

                if (mVolume != null)
                {
                    sb.Append(" v.");
                    sb.Append(mVolume);
                }

                if (mPublisher != null)
                {
                    sb.Append(" (");
                    sb.Append(mPublisher);
                    sb.Append(")");
                }

                return sb.ToString();
            }
        }


        public bool TryGuessNextIssueNumber(Collection aCollection, out int aNumber)
        {
            int max = -1;

            if (!aCollection.Series.ContainsValue(this))
            {
                throw new InvalidOperationException("TryGuessIssueNumber called on a series not in the provided collection.");
            }

            if (mIssues.Count == 0)
            {
                max = 0;
            }
            else
            {
                foreach (int index in mIssues)
                {
                    IssueRecord issue = aCollection.Issues[index];
                    string issueNum = issue.Issue;
                    if (issueNum != null)
                    {
                        int num = max;
                        if (int.TryParse(issueNum, out num))
                        {
                            max = Math.Max(num, max);
                        }
                    }
                }
            }

            aNumber = max + 1;
            return (max >= 0);
        }


        public void GenerateSortKey()
        {
            SortKey = RemoveCommonWords(FullTitle);
        }

        
        private string RemoveCommonWords(string aStr)
        {
            if (sWordsToIgnore == null)
            {
                sWordsToIgnore = new HashSet<string>();
                foreach (string s in sWordsToIgnoreTable)
                {
                    sWordsToIgnore.Add(s);
                }
            }

            StringBuilder sb = new StringBuilder();

            int i = 0;
            foreach (string str in aStr.Split(' ', '-', '_', '+', '_', '=', ':', '.', '\''))
            {
                string s = str.Trim(' ', '(', ')', '.', ',', '|', '[', ']', '<', '>', '!', '?', '@', '#', '$', '%', '^', '&', '*', '-', '_', '+', '_', '=', '/', '\\', '~', '`', '\'', ':', ';');
                s = s.ToLower();
                if (!sWordsToIgnore.Contains(s))
                {
                    sb.Append(s);
                }
                else if ((i == 0) && (s.Equals("a")))
                {
                    sb.Append(s);
                }

                i++;
            }

            return sb.ToString();
        }


        private static string[] sWordsToIgnoreTable = new string[]
        {
            "the", "and", "or", "of", "to", "is", "a"
        };

        private static HashSet<string> sWordsToIgnore = null;

        private string mTitle = "";
        private string mPublisher;
        private string mFormat;
        private string mVolume;
        private int mLsLength;
        private string mAltTitle;
        private int mParentKey = -1;
        private string mRefLinks;
        private string mKeywords;
        private string mComments;
        private string mSortKey;
        private List<int> mIssues = new List<int>();
    }
}
