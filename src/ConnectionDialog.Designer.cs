namespace Comics
{
    partial class ConnectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mHostBox = new System.Windows.Forms.ComboBox();
            this.mDatabaseBox = new System.Windows.Forms.ComboBox();
            this.mPasswordBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mCancelButton = new System.Windows.Forms.Button();
            this.mOKButton = new System.Windows.Forms.Button();
            this.mSavePasswordButton = new System.Windows.Forms.CheckBox();
            this.mUserNameBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.mHostBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.mDatabaseBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.mPasswordBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.mSavePasswordButton, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.mUserNameBox, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(289, 160);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(10, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "DB Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "User Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Right;
            this.label4.Location = new System.Drawing.Point(10, 80);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Password";
            // 
            // mHostBox
            // 
            this.mHostBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mHostBox.FormattingEnabled = true;
            this.mHostBox.Location = new System.Drawing.Point(69, 3);
            this.mHostBox.Name = "mHostBox";
            this.mHostBox.Size = new System.Drawing.Size(217, 21);
            this.mHostBox.TabIndex = 0;
            this.mHostBox.Validating += new System.ComponentModel.CancelEventHandler(this.ConnectionDialog_Validating);
            this.mHostBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ConnectionDialog_OnKeyPress);
            // 
            // mDatabaseBox
            // 
            this.mDatabaseBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mDatabaseBox.FormattingEnabled = true;
            this.mDatabaseBox.Location = new System.Drawing.Point(69, 28);
            this.mDatabaseBox.Name = "mDatabaseBox";
            this.mDatabaseBox.Size = new System.Drawing.Size(217, 21);
            this.mDatabaseBox.TabIndex = 1;
            this.mDatabaseBox.Validating += new System.ComponentModel.CancelEventHandler(this.ConnectionDialog_Validating);
            this.mDatabaseBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ConnectionDialog_OnKeyPress);
            // 
            // mPasswordBox
            // 
            this.mPasswordBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mPasswordBox.Location = new System.Drawing.Point(69, 78);
            this.mPasswordBox.Name = "mPasswordBox";
            this.mPasswordBox.PasswordChar = '*';
            this.mPasswordBox.Size = new System.Drawing.Size(217, 20);
            this.mPasswordBox.TabIndex = 3;
            this.mPasswordBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ConnectionDialog_OnKeyPress);
            this.mPasswordBox.Validating += new System.ComponentModel.CancelEventHandler(this.ConnectionDialog_Validating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Location = new System.Drawing.Point(16, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "DB Host";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel2.Controls.Add(this.mCancelButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.mOKButton, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(69, 103);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(217, 54);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // mCancelButton
            // 
            this.mCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.mCancelButton.Location = new System.Drawing.Point(139, 28);
            this.mCancelButton.Name = "mCancelButton";
            this.mCancelButton.Size = new System.Drawing.Size(75, 23);
            this.mCancelButton.TabIndex = 5;
            this.mCancelButton.Text = "Cancel";
            this.mCancelButton.UseVisualStyleBackColor = true;
            // 
            // mOKButton
            // 
            this.mOKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mOKButton.Enabled = false;
            this.mOKButton.Location = new System.Drawing.Point(58, 28);
            this.mOKButton.Name = "mOKButton";
            this.mOKButton.Size = new System.Drawing.Size(75, 23);
            this.mOKButton.TabIndex = 4;
            this.mOKButton.Text = "OK";
            this.mOKButton.UseVisualStyleBackColor = true;
            this.mOKButton.Click += new System.EventHandler(this.mOKButton_Click);
            // 
            // mSavePasswordButton
            // 
            this.mSavePasswordButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSavePasswordButton.AutoSize = true;
            this.mSavePasswordButton.Location = new System.Drawing.Point(12, 103);
            this.mSavePasswordButton.Name = "mSavePasswordButton";
            this.mSavePasswordButton.Size = new System.Drawing.Size(51, 17);
            this.mSavePasswordButton.TabIndex = 6;
            this.mSavePasswordButton.Text = "Save";
            this.mSavePasswordButton.UseVisualStyleBackColor = true;
            // 
            // mUserNameBox
            // 
            this.mUserNameBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mUserNameBox.Location = new System.Drawing.Point(69, 53);
            this.mUserNameBox.Name = "mUserNameBox";
            this.mUserNameBox.Size = new System.Drawing.Size(217, 20);
            this.mUserNameBox.TabIndex = 2;
            this.mUserNameBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ConnectionDialog_OnKeyPress);
            this.mUserNameBox.Validating += new System.ComponentModel.CancelEventHandler(this.ConnectionDialog_Validating);
            // 
            // ConnectionDialog
            // 
            this.AcceptButton = this.mOKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.mCancelButton;
            this.ClientSize = new System.Drawing.Size(289, 160);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ConnectionDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Open Database";
            this.Validating += new System.ComponentModel.CancelEventHandler(this.ConnectionDialog_Validating);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button mCancelButton;
        private System.Windows.Forms.Button mOKButton;
        private System.Windows.Forms.CheckBox mSavePasswordButton;
        private System.Windows.Forms.TextBox mUserNameBox;
        private System.Windows.Forms.ComboBox mHostBox;
        private System.Windows.Forms.ComboBox mDatabaseBox;
        private System.Windows.Forms.TextBox mPasswordBox;

    }
}