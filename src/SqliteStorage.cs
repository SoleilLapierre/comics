﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Reflection;
using System.Text;

namespace Comics
{
    class SqliteStorage : IStorage
    {
        public static SqliteStorage Create(string aFilePath, string aDatabaseName, Schema aSchema)
        {
            Schema schema = new Schema(aSchema);
            schema.AddTable(typeof(InfoRecord));

            SQLiteConnection.CreateFile(aFilePath);
            var connection = new SQLiteConnection(GetConnectionStringForFile(aFilePath));
            connection.Open();

            using (var transaction = connection.BeginTransaction())
            {
                foreach (Schema.Table t in schema.Tables)
                {
                    var query = new StringBuilder();
                    query.Append("CREATE TABLE `");
                    query.Append(t.Name);
                    query.AppendLine("` (");

                    for (int i = 0; i < t.Fields.Count; i++)
                    {
                        var f = t.Fields[i];
                        query.Append("  `");
                        query.Append(f.DatabaseName);
                        query.Append("` ");
                        query.Append(FieldTypeToSqlType(f.Type));

                        if (f.Required)
                        {
                            query.Append(" NOT NULL");
                        }
                        else if (f.Type == Schema.Field.FieldType.DateTime)
                        {
                            query.Append(" default '2000-01-01 00:00:00'");
                        }
                        else
                        {
                            query.Append(" default NULL");
                        }

                        if (t.Fields.IndexOf(f) == t.KeyField)
                        {
                            query.Append(" PRIMARY KEY");
                        }

                        if (i < t.Fields.Count - 1)
                        {
                            query.AppendLine(",");
                        }
                    }

                    query.AppendLine(");");

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = query.ToString();
                        command.ExecuteNonQuery();
                    }
                }

                transaction.Commit();
            }

            SqliteStorage storage = new SqliteStorage();
            storage.mConnection = connection;
            storage.mCurrentSchema = schema;

            InfoRecord info = new InfoRecord();
            info.Schema = schema.Serialize();
            info.CollectionName = aDatabaseName;

            storage.BeginTransaction();
            storage.AddRecord<InfoRecord>(info);
            storage.CommitTransaction();

            return storage;
        }


        public static SqliteStorage Open(string aFilePath, Schema aSchema)
        {
            Schema schema = new Schema(aSchema);
            schema.AddTable(typeof(InfoRecord));

            var connection = new SQLiteConnection(GetConnectionStringForFile(aFilePath));
            connection.Open();

            SqliteStorage storage = new SqliteStorage();
            storage.mConnection = connection;
            storage.mCurrentSchema = schema;

            Dictionary<int, InfoRecord> headers = storage.GetRecords<InfoRecord>();
            int max = 0;
            foreach (int i in headers.Keys)
            {
                if (i > max)
                {
                    max = i;
                }
            }

            InfoRecord info = headers[max];

            if (info.Version != schema.Version)
            {
                throw new NotImplementedException("Upgrading database version not yet supported.");
            }

            return storage;
        }

        #region ----- IStorage implementation -----

        public Dictionary<int, T> GetRecords<T>() where T : Record
        {
            if (null != mCurrentTransaction)
            {
                throw new InvalidOperationException("Cannot run a query when a transaction is open; complete the transaction first.");
            }

            Schema.Table table = mCurrentSchema[typeof(T)];
            Dictionary<string, int> fieldMap = new Dictionary<string, int>();

            StringBuilder query = new StringBuilder();
            query.Append("SELECT * FROM ");
            query.Append(table.Name);
            query.Append(";");

            Dictionary<int, T> result = new Dictionary<int, T>();

            using (var command = new SQLiteCommand(mConnection))
            {
                command.CommandText = query.ToString();

                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Type typeHandle = typeof(T);
                    var constructor = typeHandle.GetConstructor(Type.EmptyTypes);
                    T obj = constructor.Invoke(null) as T;

                    foreach (Schema.Field field in table.Fields)
                    {
                        var prop = typeHandle.GetProperty(field.PropertyName);
                        int ordinal = reader.GetOrdinal(field.DatabaseName);
                        object val = null;

                        if (!reader.IsDBNull(ordinal))
                        {
                            switch (field.Type)
                            {
                                case Schema.Field.FieldType.Int8:
                                    val = reader.GetChar(ordinal);
                                    break;
                                case Schema.Field.FieldType.Int16:
                                    val = reader.GetInt16(ordinal);
                                    break;
                                case Schema.Field.FieldType.Int32:
                                    val = reader.GetInt32(ordinal);
                                    break;
                                case Schema.Field.FieldType.Int64:
                                    val = reader.GetInt64(ordinal);
                                    break;
                                case Schema.Field.FieldType.Uint8:
                                    val = reader.GetByte(ordinal);
                                    break;
                                case Schema.Field.FieldType.Uint16:
                                    val = (UInt16)reader.GetInt16(ordinal);
                                    break;
                                case Schema.Field.FieldType.Uint32:
                                    val = (UInt32)reader.GetInt32(ordinal);
                                    break;
                                case Schema.Field.FieldType.Uint64:
                                    val = (UInt64)reader.GetInt64(ordinal);
                                    break;
                                case Schema.Field.FieldType.Decimal:
                                    val = reader.GetDecimal(ordinal);
                                    break;
                                case Schema.Field.FieldType.Float:
                                    val = reader.GetFloat(ordinal);
                                    break;
                                case Schema.Field.FieldType.Double:
                                    val = reader.GetDouble(ordinal);
                                    break;
                                case Schema.Field.FieldType.DateTime:
                                    val = reader.GetDateTime(ordinal);
                                    break;
                                case Schema.Field.FieldType.String:
                                    val = UnescapeString(reader.GetString(ordinal));
                                    break;
                                case Schema.Field.FieldType.ByteArray:
                                    val = reader.GetValue(ordinal);
                                    break;
                                case Schema.Field.FieldType.Int32List:
                                    string text = reader.GetString(ordinal);
                                    string[] vals = text.Split(',');
                                    List<Int32> ints = new List<int>(vals.Length);
                                    for (int i = 0; i < vals.Length; i++)
                                    {
                                        int temp = -1;
                                        if (Int32.TryParse(vals[i], out temp))
                                        {
                                            ints.Add(temp);
                                        }
                                    }
                                    val = ints;
                                    break;
                                default:
                                    throw new Exception("Invalid type for field " + field.DatabaseName);
                                    // break;
                            }
                        }
                        else if (field.Required)
                        {
                            throw new Exception("Required field " + field.DatabaseName + " is NULL.");
                        }

                        prop.SetValue(obj, val, null);
                    }

                    obj.Dirty = false;
                    result[obj.ID] = obj;
                }

                reader.Close();
            }

            return result;
        }


        public void BeginTransaction()
        {
            if (mCurrentTransaction != null)
            {
                throw new InvalidOperationException("Cannot start a transaction when a transaction is already open; complete the transaction first.");
            }

            mCurrentTransaction = mConnection.BeginTransaction();
        }


        public bool CommitTransaction()
        {
            if (mCurrentTransaction is null)
            {
                throw new InvalidOperationException("Cannot commit when no transaction is open. Call BeginTransaction() first.");
            }

            bool result = false;
            try
            {
                mCurrentTransaction.Commit();
                result = true;
            }
            catch (Exception)
            {
                try
                {
                    mCurrentTransaction.Rollback();
                }
                catch (Exception)
                {
                }
            }

            mCurrentTransaction = null;
            return result;
        }


        public void RollbackTransaction()
        {
            if (mCurrentTransaction is null)
            {
                throw new InvalidOperationException("Cannot commit when no transaction is open. Call BeginTransaction() first.");
            }

            try
            {
                mCurrentTransaction.Rollback();
            }
            catch (Exception)
            {
            }

            mCurrentTransaction = null;
        }


        public bool Update<T>(T aRecord) where T : Record
        {
            if (!aRecord.Dirty)
            {
                throw new InvalidOperationException("Only call Update() on objects that are dirty.");
            }

            if (aRecord.ID < 0)
            {
                throw new InvalidOperationException("Only call Update() on objects that are already in the database.");
            }

            if (null == mCurrentTransaction)
            {
                throw new InvalidOperationException("Can't update records without an open transaction.");
            }

            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];

            StringBuilder query = new StringBuilder();
            query.Append("UPDATE `");
            query.Append(table.Name);
            query.Append("` SET ");

            bool first = true;
            foreach (Schema.Field field in table.Fields)
            {
                if (table.Fields.IndexOf(field) != table.KeyField)
                {
                    if (!first)
                    {
                        query.Append(", ");
                    }
                    first = false;

                    query.Append("`");
                    query.Append(field.DatabaseName);
                    query.Append("`");

                    PropertyInfo prop = typeHandle.GetProperty(field.PropertyName);
                    object val = prop.GetValue(aRecord, null);

                    if (table.Fields.IndexOf(field) == table.TimestampField)
                    {
                        query.Append("=DateTime('now')");
                    }
                    else if (val != null)
                    {
                        query.Append("='");
                        if (val is DateTime)
                        {
                            DateTime date = (DateTime)val;
                            query.Append(date.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                        else if (val is List<Int32>)
                        {
                            bool firstInt = true;
                            foreach (int intVal in (val as List<Int32>))
                            {
                                if (!firstInt)
                                {
                                    query.Append(",");
                                }
                                firstInt = false;

                                query.Append(intVal.ToString());
                            }
                        }
                        else if (val is String)
                        {
                            query.Append(EscapeString(val as String));
                        }
                        else
                        {
                            query.Append(val.ToString());
                        }
                        query.Append("'");
                    }
                    else
                    {
                        query.Append("=NULL");
                    }
                }
            }

            query.Append(" WHERE `");
            Schema.Field keyField = table.Fields[table.KeyField];
            query.Append(keyField.DatabaseName);
            query.Append("`='");
            query.Append(aRecord.ID.ToString());
            query.Append("';");

            int count = 0;
            using (var command = new SQLiteCommand(mConnection))
            {
                command.Transaction = mCurrentTransaction;
                command.CommandText = query.ToString();
                count = command.ExecuteNonQuery();
            }

            return (count == 1);
        }


        public bool AddRecord<T>(T aRecord) where T : Record
        {
            if (null == mCurrentTransaction)
            {
                throw new InvalidOperationException("Can't update records without an open transaction.");
            }

            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];

            StringBuilder fieldNames = new StringBuilder();
            fieldNames.Append("(");

            StringBuilder fieldValues = new StringBuilder();
            fieldValues.Append("(");

            bool first = true;
            foreach (Schema.Field field in table.Fields)
            {
                if (!first)
                {
                    fieldNames.Append(", ");
                    fieldValues.Append(", ");
                }
                first = false;

                fieldNames.Append("`");
                fieldNames.Append(field.DatabaseName);
                fieldNames.Append("`");

                PropertyInfo prop = typeHandle.GetProperty(field.PropertyName);
                object val = prop.GetValue(aRecord, null);
                if (table.Fields.IndexOf(field) == table.TimestampField)
                {
                    fieldValues.Append("DateTime('now')");
                }
                else if (val != null)
                {
                    fieldValues.Append("'");
                    if (val is DateTime)
                    {
                        DateTime date = (DateTime)val;
                        fieldValues.Append(date.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else if (val is List<Int32>)
                    {
                        bool firstInt = true;
                        foreach (int intval in (val as List<Int32>))
                        {
                            if (!firstInt)
                            {
                                fieldValues.Append(",");
                            }
                            firstInt = false;

                            fieldValues.Append(intval.ToString());
                        }
                    }
                    else if (val is String)
                    {
                        fieldValues.Append(EscapeString(val as String));
                    }
                    else
                    {
                        fieldValues.Append(val.ToString());
                    }
                    fieldValues.Append("'");
                }
                else
                {
                    fieldValues.Append("NULL");
                }
            }

            fieldNames.Append(")");
            fieldValues.Append(")");

            StringBuilder query = new StringBuilder();
            query.Append("INSERT INTO `");
            query.Append(table.Name);
            query.Append("` ");
            query.Append(fieldNames.ToString());
            query.Append(" VALUES ");
            query.Append(fieldValues.ToString());
            query.Append(";");

            int count = 0;
            using (var command = new SQLiteCommand(mConnection))
            {
                command.Transaction = mCurrentTransaction;
                command.CommandText = query.ToString();
                count = command.ExecuteNonQuery();
            }

            return (count == 1);
        }


        public bool DeleteRecord<T>(T aRecord) where T : Record
        {
            if (null == mCurrentTransaction)
            {
                throw new InvalidOperationException("Can't update records without an open transaction.");
            }

            Type typeHandle = typeof(T);
            Schema.Table table = mCurrentSchema[typeHandle];
            Schema.Field keyField = table.Fields[table.KeyField];

            StringBuilder query = new StringBuilder();
            query.Append("DELETE FROM `");
            query.Append(table.Name);
            query.Append("` WHERE `");
            query.Append(keyField.DatabaseName);
            query.Append("`='");
            query.Append(aRecord.ID);
            query.Append("';");

            int count = 0;
            using (var command = new SQLiteCommand(mConnection))
            {
                command.Transaction = mCurrentTransaction;
                command.CommandText = query.ToString();
                count = command.ExecuteNonQuery();
            }

            return (count == 1);
        }


        public string GetMessages()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        private static string FieldTypeToSqlType(Schema.Field.FieldType aType)
        {
            string result = null;

            switch (aType)
            {
                case Schema.Field.FieldType.Int8: result = "tinyint"; break;
                case Schema.Field.FieldType.Int16: result = "smallint"; break;
                case Schema.Field.FieldType.Int32: result = "int"; break;
                case Schema.Field.FieldType.Int64: result = "bigint"; break;
                case Schema.Field.FieldType.Uint8: result = "unsigned tinyint"; break;
                case Schema.Field.FieldType.Uint16: result = "unsigned smallint"; break;
                case Schema.Field.FieldType.Uint32: result = "unsigned int"; break;
                case Schema.Field.FieldType.Uint64: result = "unsigned bigint"; break;
                case Schema.Field.FieldType.Decimal: result = "decimal(8,2)"; break;
                case Schema.Field.FieldType.Float: result = "float"; break;
                case Schema.Field.FieldType.Double: result = "double"; break;
                case Schema.Field.FieldType.DateTime: result = "datetime"; break;
                case Schema.Field.FieldType.String: result = "text"; break;
                case Schema.Field.FieldType.ByteArray: result = "blob"; break;
                case Schema.Field.FieldType.Int32List: result = "text"; break;
                default: break;
            }

            return result;
        }


        private static string EscapeString(string aString)
        {
            return aString.Replace("'", "''");
        }


        private static string UnescapeString(string aString)
        {
            return aString.Replace("''", "'");
        }


        private static string GetConnectionStringForFile(string aFilePath)
        {
            // Workaround a la http://system.data.sqlite.org/index.html/info/bbdda6eae2
            var connectString = aFilePath;
            if (aFilePath.StartsWith(@"\\"))
            {
                connectString = connectString.Replace(@"\\", @"\\\\");
            }

            connectString = $"Data Source={connectString};Version=3;Read Only=False";
            return connectString;
        }


        private SQLiteConnection mConnection;
        private Schema mCurrentSchema;
        private SQLiteTransaction mCurrentTransaction;
    }
}
