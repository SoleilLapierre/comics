namespace Comics
{
    partial class CollectionPropertiesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mLabelCollectionName = new System.Windows.Forms.Label();
            this.mLabelCollectionOwner = new System.Windows.Forms.Label();
            this.mTextCollectionName = new System.Windows.Forms.TextBox();
            this.mTextCollectionOwner = new System.Windows.Forms.TextBox();
            this.mLabelVersion = new System.Windows.Forms.Label();
            this.mLabelLastEdit = new System.Windows.Forms.Label();
            this.mButtonCancel = new System.Windows.Forms.Button();
            this.mButtonApply = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mLabelCollectionName
            // 
            this.mLabelCollectionName.AutoSize = true;
            this.mLabelCollectionName.Location = new System.Drawing.Point(12, 9);
            this.mLabelCollectionName.Name = "mLabelCollectionName";
            this.mLabelCollectionName.Size = new System.Drawing.Size(87, 13);
            this.mLabelCollectionName.TabIndex = 0;
            this.mLabelCollectionName.Text = "Collection Name:";
            // 
            // mLabelCollectionOwner
            // 
            this.mLabelCollectionOwner.AutoSize = true;
            this.mLabelCollectionOwner.Location = new System.Drawing.Point(12, 35);
            this.mLabelCollectionOwner.Name = "mLabelCollectionOwner";
            this.mLabelCollectionOwner.Size = new System.Drawing.Size(90, 13);
            this.mLabelCollectionOwner.TabIndex = 1;
            this.mLabelCollectionOwner.Text = "Collection Owner:";
            // 
            // mTextCollectionName
            // 
            this.mTextCollectionName.Location = new System.Drawing.Point(105, 6);
            this.mTextCollectionName.Name = "mTextCollectionName";
            this.mTextCollectionName.Size = new System.Drawing.Size(260, 20);
            this.mTextCollectionName.TabIndex = 2;
            // 
            // mTextCollectionOwner
            // 
            this.mTextCollectionOwner.Location = new System.Drawing.Point(105, 32);
            this.mTextCollectionOwner.Name = "mTextCollectionOwner";
            this.mTextCollectionOwner.Size = new System.Drawing.Size(260, 20);
            this.mTextCollectionOwner.TabIndex = 3;
            // 
            // mLabelVersion
            // 
            this.mLabelVersion.AutoSize = true;
            this.mLabelVersion.Location = new System.Drawing.Point(15, 72);
            this.mLabelVersion.Name = "mLabelVersion";
            this.mLabelVersion.Size = new System.Drawing.Size(48, 13);
            this.mLabelVersion.TabIndex = 4;
            this.mLabelVersion.Text = "Version: ";
            // 
            // mLabelLastEdit
            // 
            this.mLabelLastEdit.AutoSize = true;
            this.mLabelLastEdit.Location = new System.Drawing.Point(18, 89);
            this.mLabelLastEdit.Name = "mLabelLastEdit";
            this.mLabelLastEdit.Size = new System.Drawing.Size(75, 13);
            this.mLabelLastEdit.TabIndex = 5;
            this.mLabelLastEdit.Text = "Last modified: ";
            // 
            // mButtonCancel
            // 
            this.mButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.mButtonCancel.Location = new System.Drawing.Point(289, 121);
            this.mButtonCancel.Name = "mButtonCancel";
            this.mButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.mButtonCancel.TabIndex = 6;
            this.mButtonCancel.Text = "Cancel";
            this.mButtonCancel.UseVisualStyleBackColor = true;
            this.mButtonCancel.Click += new System.EventHandler(this.mButtonCancel_Click);
            // 
            // mButtonApply
            // 
            this.mButtonApply.Location = new System.Drawing.Point(208, 121);
            this.mButtonApply.Name = "mButtonApply";
            this.mButtonApply.Size = new System.Drawing.Size(75, 23);
            this.mButtonApply.TabIndex = 7;
            this.mButtonApply.Text = "Apply";
            this.mButtonApply.UseVisualStyleBackColor = true;
            this.mButtonApply.Click += new System.EventHandler(this.mButtonApply_Click);
            // 
            // CollectionPropertiesDialog
            // 
            this.AcceptButton = this.mButtonApply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.mButtonCancel;
            this.ClientSize = new System.Drawing.Size(378, 156);
            this.Controls.Add(this.mButtonApply);
            this.Controls.Add(this.mButtonCancel);
            this.Controls.Add(this.mLabelLastEdit);
            this.Controls.Add(this.mLabelVersion);
            this.Controls.Add(this.mTextCollectionOwner);
            this.Controls.Add(this.mTextCollectionName);
            this.Controls.Add(this.mLabelCollectionOwner);
            this.Controls.Add(this.mLabelCollectionName);
            this.Name = "CollectionPropertiesDialog";
            this.Text = "Collection Properties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label mLabelCollectionName;
        private System.Windows.Forms.Label mLabelCollectionOwner;
        private System.Windows.Forms.TextBox mTextCollectionName;
        private System.Windows.Forms.TextBox mTextCollectionOwner;
        private System.Windows.Forms.Label mLabelVersion;
        private System.Windows.Forms.Label mLabelLastEdit;
        private System.Windows.Forms.Button mButtonCancel;
        private System.Windows.Forms.Button mButtonApply;
    }
}