using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Comics
{
    public partial class SeriesTreeViewControl : UserControl, IDisposable
    {
        #region ----- Public methods and properties -----

        public delegate void SeriesSelectionChangedEvent(Collection aCollection, int aLastSelectedSeriesID, List<SeriesRecord> aSelectedSeries);
        public event SeriesSelectionChangedEvent OnSeriesSelectionChanged;


        public SeriesTreeViewControl()
        {
            InitializeComponent();
        }


        public Collection Collection
        {
            set
            {
                mCollection = value;
                RebuildSeriesTree();
                if (mCollection != null)
                {
                    mCollection.OnSeriesModified += new Collection.SeriesModifiedEvent(SeriesModifiedEvent);
                }

                mAddSeriesButton.Enabled = (mCollection != null);
                mDeleteSeriesButton.Enabled = (mCollection != null);
                mTreeView.Enabled = (mCollection != null);
            }
        }


        public ISeriesFilter Filter { get; set; } = null;

        public void RefreshContent()
        {
            RebuildSeriesTree();
        }

        #endregion
        #region ----- IDisposable Members -----

        void IDisposable.Dispose()
        {
            if (mCollection != null)
            {
                mCollection.OnSeriesModified -= new Collection.SeriesModifiedEvent(SeriesModifiedEvent);
            }
        }

        #endregion
        #region ----- Tree (re)population methods -----

        private void RebuildSeriesTree()
        {
            mTreeMap.Clear();
            mTreeView.Nodes.Clear();

            if (mCollection is null)
            {
                return;
            }

            foreach (SeriesRecord s in mCollection.Series.Values.Where(s => Filter is null || Filter.IncludeSeries(s)))
            {
                if (!mTreeMap.ContainsKey(s.ID))
                {
                    InsertTreeNode(s);
                }
            }
            mTreeView.Sort();

            foreach (int id in mTreeMap.Keys)
            {
                if (mExpandedMap.ContainsKey(id))
                {
                    if (mExpandedMap[id])
                    {
                        mTreeMap[id].Expand();
                    }
                    else
                    {
                        mTreeMap[id].Collapse();
                    }
                }
            }
        }


        private TreeNode InsertTreeNode(SeriesRecord s)
        {
            TreeNode n = null;
            if (s.ParentKey != -1)
            {
                TreeNode parent = null;
                mTreeMap.TryGetValue(s.ParentKey, out parent);
                if (parent == null)
                {
                    SeriesRecord sp = mCollection.Series[s.ParentKey];
                    parent = InsertTreeNode(sp);
                }

                n = parent.Nodes.Add(s.FullTitle);
            }
            else
            {
                n = mTreeView.Nodes.Add(s.FullTitle);
            }

            n.Tag = s.ID;
            mTreeMap[s.ID] = n;
            return n;
        }

        #endregion
        #region ----- Private functionality -----

        private void DeleteSeries(int id)
        {
            if (!mCollection.DeleteSeries(id))
            {
                MessageBox.Show("Could not delete this series because it contains issues. Delete or move the issues first.");
            }
        }

        #endregion
        #region ----- Tree expand/collapse events -----

        private void mTreeView_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            SaveExpandCollapseState(e.Node);
        }


        private void mTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            SaveExpandCollapseState(e.Node);
        }


        private void SaveExpandCollapseState(TreeNode n)
        {
            if (n.Tag != null)
            {
                int id = (int)n.Tag;
                if (id != -1)
                {
                    mExpandedMap[id] = n.IsExpanded;
                }
            }
        }

        #endregion
        #region ----- Drag'n'drop events -----

        void mTreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }


        void mTreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;

            if (sender is TreeView view)
            {
                int destID = -1;
                Point pt = view.PointToClient(new Point(e.X, e.Y));
                TreeNode destNode = view.GetNodeAt(pt);
                if (destNode != null)
                {
                    destID = (int)destNode.Tag;
                }

                if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
                {
                    TreeNode sourceNode = e.Data.GetData("System.Windows.Forms.TreeNode") as TreeNode;
                    int sourceID = (int)sourceNode.Tag;

                    bool found = (destID == sourceID);
                    while ((destID != -1) && !found)
                    {
                        SeriesRecord s = mCollection.Series[destID];
                        destID = s.ParentKey;
                        found |= (destID == sourceID);
                    }

                    if (found)
                    {
                        e.Effect = DragDropEffects.None;
                    }
                }
                else if (e.Data.GetDataPresent(typeof(List<int>)))
                {
                    List<int> issues = e.Data.GetData(typeof(List<int>)) as List<int>;
                    if ((issues.Count > 0) && (destID != -1))
                    {
                        e.Effect = DragDropEffects.Link;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                    }
                }
            }
        }


        void mTreeView_DragDrop(object sender, DragEventArgs e)
        {
            if (sender is TreeView view)
            {
                int destID = -1;
                Point pt = view.PointToClient(new Point(e.X, e.Y));
                TreeNode destNode = view.GetNodeAt(pt);
                if (destNode != null)
                {
                    destID = (int)destNode.Tag;
                }

                if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
                {
                    TreeNode sourceNode = e.Data.GetData("System.Windows.Forms.TreeNode") as TreeNode;
                    int sourceID = (int)sourceNode.Tag;

                    SeriesRecord s = mCollection.Series[sourceID];
                    s.ParentKey = destID;
                    RebuildSeriesTree();
                    mTreeView.SelectedNode = mTreeMap[sourceID];
                    Invalidate();
                }
                else if (e.Data.GetDataPresent(typeof(List<int>)))
                {
                    List<int> issues = e.Data.GetData(typeof(List<int>)) as List<int>;

                    if (destID != -1)
                    {
                        mCollection.MoveIssuesToSeries(issues, destID);
                    }
                }
            }
        }

        #endregion
        #region ----- Toolbar button events -----

        private void mNewSeriesButton_Click(object sender, EventArgs e)
        {
            TreeNode n = mTreeView.SelectedNode;

            SeriesPropertiesForm form = new SeriesPropertiesForm();
            DialogResult dr = form.ShowDialog();
            if (dr == DialogResult.OK)
            {
                SeriesRecord s = form.Result;

                if (n != null)
                {
                    if (n.Tag != null)
                    {
                        s.ParentKey = (int)n.Tag;
                    }
                }

                mCollection.AddSeries(s);
            }
        }


        private void mDeleteSeriesButton_Click(object sender, EventArgs e)
        {
            TreeNode n = mTreeView.SelectedNode;
            if (n != null)
            {
                if (n.Tag != null)
                {
                    DeleteSeries((int)n.Tag);
                }
            }

        }

        #endregion
        #region ----- Collection modification events -----

        void SeriesModifiedEvent(Collection aCollection, int aSeriesID, Collection.CollectionItemModificationType aReason)
        {
            if (mCollection is null)
            {
                return;
            }

            if (aCollection != mCollection)
            {
                throw new InvalidOperationException("SeriesModifiedEvent received for wrong collection.");
            }

            switch (aReason)
            {
                case Collection.CollectionItemModificationType.Added:
                    TreeNode n = InsertTreeNode(mCollection.Series[aSeriesID]);
                    mTreeView.Sort();
                    mExpandedMap[aSeriesID] = false;
                    n.Collapse();
                    mTreeView.SelectedNode = n;
                    break;
                case Collection.CollectionItemModificationType.Changed:
                    break;
                case Collection.CollectionItemModificationType.Deleted:
                    RebuildSeriesTree();
                    break;
            }

            Invalidate();
        }

        #endregion
        #region ----- Selection events -----

        private void FireSeriesSelectionChangedEvent(List<SeriesRecord> aSeriesList, int aLastSelectedSeriesID)
        {
            if (OnSeriesSelectionChanged != null)
            {
                OnSeriesSelectionChanged(mCollection, aLastSelectedSeriesID, aSeriesList);
            }
        }


        private void AddSeriesIDsRecursive(TreeNode aNode, List<SeriesRecord> aList)
        {
            if (aNode.Tag != null)
            {
                int seriesID = (int)aNode.Tag;
                aList.Add(mCollection.Series[seriesID]);
            }

            foreach (TreeNode node in aNode.Nodes)
            {
                AddSeriesIDsRecursive(node, aList);
            }
        }


        private void mTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            List<SeriesRecord> list = new List<SeriesRecord>();
            int seriesID = -1;
            if (e.Node != null)
            {
                seriesID = (int)e.Node.Tag;
                AddSeriesIDsRecursive(e.Node, list);
            }

            FireSeriesSelectionChangedEvent(new List<SeriesRecord>(list), seriesID);
        }


        private void mTreeView_MouseUp(object sender, MouseEventArgs e)
        {
            Point p = new Point(e.X, e.Y);
            TreeNode node = mTreeView.GetNodeAt(p);

            if (node != null)
            {
                mTreeView.SelectedNode = node;
                if (e.Button == MouseButtons.Right)
                {
                    mContextMenuNode = node;
                    mSeriesTreeNodeContextMenu.Show(mTreeView, p);
                }
            }
            else
            {
                mTreeView.SelectedNode = null;
                mContextMenuNode = null;
            }
        }

        #endregion
        #region ----- Context menu events -----

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mContextMenuNode != null)
            {
                int seriesID = (int)mContextMenuNode.Tag;
                SeriesRecord series = mCollection.Series[seriesID];
                SeriesRecord s = new SeriesRecord();
                s.CopyFrom(series);

                SeriesPropertiesForm dlg = new SeriesPropertiesForm(s);
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    series.CopyFrom(s);
                }
            }

            mContextMenuNode = null;
        }

        private void generateReadingOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mContextMenuNode != null)
            {
                int seriesID = (int)mContextMenuNode.Tag;
                SeriesRecord series = mCollection.Series[seriesID];
                Dictionary<int, IssueRecord> numMap = new Dictionary<int, IssueRecord>();

                // Build a map of issue numbers currently presnt.
                foreach (int issueID in series.Issues)
                {
                    IssueRecord issue = mCollection.Issues[issueID];
                    
                    int num = -1;
                    bool parsed = false;
                    try
                    {
                        num = int.Parse(issue.Issue);
                        parsed = true;
                    }
                    catch (FormatException) { }

                    if (parsed)
                    {
                        if (!numMap.ContainsKey(num))
                        {
                            numMap.Add(num, issue);
                        }
                        else if ((issue.IssueType == null) || (issue.IssueType.Length < 1)) 
                        {
                            numMap[num] = issue;
                        }
                    }
                }

                // Iterate over all found.
                foreach (int key in numMap.Keys)
                {
                    int prev = key - 1;
                    if (numMap.ContainsKey(prev))
                    {
                        IssueRecord issue = numMap[key];
                        IssueRecord prevIssue = numMap[prev];
                        int id = prevIssue.ID;
                        if (!issue.IssueRefsReadPrev.Contains(id))
                        {
                            issue.IssueRefsReadPrev.Add(id);
                            issue.Dirty = true;
                        }
                    }
                }
            }

            mContextMenuNode = null;
        }

        #endregion
        #region ----- Private members -----

        Collection mCollection;
        Dictionary<int, TreeNode> mTreeMap = new Dictionary<int, TreeNode>();
        Dictionary<int, bool> mExpandedMap = new Dictionary<int, bool>();

        TreeNode mContextMenuNode;

        #endregion
    }
}
