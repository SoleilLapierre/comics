using System;
using System.Collections.Generic;
using System.Linq;

namespace Comics
{
    public class Collection
    {
        #region ----- Public types and events -----

        public enum CollectionItemModificationType
        {
            Added,
            Changed,
            Deleted
        };

        public delegate void CollectionLoadedEvent(Collection aCollection);
        public event CollectionLoadedEvent OnCollectionLoaded;

        public delegate void CollectionPropertiesChangedEvent(Collection aCollection);
        public event CollectionPropertiesChangedEvent OnCollectionPropertiesChanged;

        public delegate void SeriesModifiedEvent(Collection aCollection, int aSeriesID, CollectionItemModificationType aReason);
        public event SeriesModifiedEvent OnSeriesModified;

        public delegate void IssueModifiedEvent(Collection aCollection, int aIssueID, CollectionItemModificationType aReason);
        public event IssueModifiedEvent OnIssueModified;

        #endregion
        #region ----- Public API ----

        #region ----- Construction and database management -----

        static Collection()
        {
            sCurrentSchema = new Schema(1);
            sCurrentSchema.AddTable(typeof(SeriesRecord));
            sCurrentSchema.AddTable(typeof(IssueRecord));
        }


        public Collection(IStorage aDatabase)
        {
            Populate(aDatabase);
        }


        public bool Save()
        {
            if (mSaved)
            {
                FireCollectionPropertiesChangedEvent();
                return true;
            }

            bool result = true;
            mDatabase.BeginTransaction();

            if (result)
            {
                result &= TableUpdateHelper<SeriesRecord>(mSeries, mAddedSeries, mDeletedSeries);
            }

            if (result)
            {
                result &= TableUpdateHelper<IssueRecord>(mIssues, mAddedIssues, mDeletedIssues);
            }

            if (result && mHeader.Dirty)
            {
                result &= mDatabase.Update<InfoRecord>(mHeader);
            }

            if (result)
            {
                result &= mDatabase.CommitTransaction();
            }
            else
            {
                mDatabase.RollbackTransaction();
            }

            if (result)
            {
                Populate(mDatabase); 
                // Note the discards the current contents and reloads, in order to ensure
                // we get dirrect series IDs. Probably not really necessary, but if this is
                // removed, remember to clear the add and delete tables and reset the
                // dirty state ov everything saved.

                mSaved = true;
                FireCollectionLoadedEvent();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Error saving database.");
            }

            return result;
        }

        #endregion
        #region ----- Properties -----

        public static Schema CurrentSchema => sCurrentSchema;

        public bool Saved => mSaved;

        public Dictionary<int, IssueRecord> Issues => mIssues;

        public Dictionary<int, SeriesRecord> Series => mSeries;

        public InfoRecord InfoRecord => mHeader;

        #endregion
        #region ----- Data addition and deletion -----

        public int AddSeries(SeriesRecord series)
        {
            series.ID = mHeader.NextSeriesID;
            mHeader.NextSeriesID++;
            mSeries[series.ID] = series;
            mAddedSeries[series.ID] = series;
            mSaved = false;

            FireSeriesModifiedEvent(series.ID, CollectionItemModificationType.Added);
            series.OnRecordDirtied += series_OnRecordDirtied;

            return series.ID;
        }


        public int ImportSeries(SeriesRecord series)
        {
            series.ID = mHeader.NextSeriesID;
            mHeader.NextSeriesID++;
            mSeries[series.ID] = series;
            mAddedSeries[series.ID] = series;
            mSaved = false;
            return series.ID;
        }


        public bool DeleteSeries(int id)
        {
            bool referenced = false;

            foreach (IssueRecord issue in mIssues.Values)
            {
                if (issue.SeriesID == id)
                {
                    referenced = true;
                    break;
                }
            }

            if (!referenced)
            {
                foreach (SeriesRecord s in mSeries.Values)
                {
                    if (s.ParentKey == id)
                    {
                        referenced = true;
                        break;
                    }
                }
            }

            if (!referenced)
            {
                SeriesRecord series = mSeries[id];
                series.OnRecordDirtied -= series_OnRecordDirtied;

                if (mAddedSeries.ContainsKey(id))
                {
                    mAddedSeries.Remove(id);
                }
                else
                {
                    mDeletedSeries.Add(id, mSeries[id]);
                }

                mSeries.Remove(id);
                mSaved = false;

                FireSeriesModifiedEvent(id, CollectionItemModificationType.Deleted);

                return true;
            }

            return false;
        }


        public int AddIssue(IssueRecord issue)
        {
            if (issue.SeriesID < 0)
            {
                throw new InvalidOperationException("Cannot add an issue without a valid series ID.");
            }

            issue.ID = mHeader.NextIssueID;
            mHeader.NextIssueID++;
            mIssues[issue.ID] = issue;
            mAddedIssues[issue.ID] = issue;
            mSaved = false;

            SeriesRecord series = Series[issue.SeriesID];
            series.Issues.Add(issue.ID);

            FireIssueModifiedEvent(issue.ID, CollectionItemModificationType.Added);
            issue.OnRecordDirtied += issue_OnRecordDirtied;

            return issue.ID;
        }


        public int ImportIssue(IssueRecord issue)
        {
            issue.ID = mHeader.NextIssueID;
            mHeader.NextIssueID++;
            mIssues[issue.ID] = issue;
            mAddedIssues[issue.ID] = issue;
            mSaved = false;
            return issue.ID;
        }


        public bool DeleteIssue(int id)
        {
            IssueRecord issue = mIssues[id];
            issue.OnRecordDirtied -= issue_OnRecordDirtied;

            if (mAddedIssues.ContainsKey(id))
            {
                mAddedIssues.Remove(id);
            }
            else
            {
                mDeletedIssues.Add(id, mIssues[id]);
            }

            mSaved = false;

            FireIssueModifiedEvent(id, CollectionItemModificationType.Deleted);

            mIssues.Remove(id);
            return true;
        }


        public void MoveIssuesToSeries(List<int> aIssueList, int aSeriesID)
        {
            SeriesRecord newSeries = mSeries[aSeriesID];

            foreach (int issueIndex in aIssueList)
            {
                IssueRecord issue = mIssues[issueIndex];

                if (issue.SeriesID != aSeriesID)
                {
                    int oldSeriesID = issue.SeriesID;
                    SeriesRecord oldSeries = mSeries[oldSeriesID];
                    oldSeries.Issues.Remove(issueIndex);

                    issue.SeriesID = aSeriesID;
                    newSeries.Issues.Add(issueIndex);

                    FireIssueModifiedEvent(issueIndex, CollectionItemModificationType.Changed);
                    FireSeriesModifiedEvent(oldSeriesID, CollectionItemModificationType.Changed);
                }
            }

            FireSeriesModifiedEvent(aSeriesID, CollectionItemModificationType.Changed);
        }


        public void Import(Collection aOther)
        {
            var seriesMap = new Dictionary<int, int>();
            var issueMap = new Dictionary<int, int>();
            var importedSeries = new List<SeriesRecord>();
            var importedIssues = new List<IssueRecord>();

            // Copy over series records.
            foreach (var pair in aOther.Series.OrderBy(pair => pair.Key))
            {
                var id = pair.Key;
                var series = pair.Value;
                var newSeries = new SeriesRecord();
                newSeries.CopyFrom(series);
                importedSeries.Add(newSeries);
                var newId = ImportSeries(newSeries);
                seriesMap[id] = newId;
            }

            // Fix up series parent IDs.
            foreach (var series in importedSeries)
            {
                if (seriesMap.ContainsKey(series.ParentKey))
                {
                    series.ParentKey = seriesMap[series.ParentKey];
                }
            }

            // Copy issue records, fixing up series IDs.
            foreach (var pair in aOther.Issues.OrderBy(pair => pair.Key))
            {
                var id = pair.Key;
                var issue = pair.Value;
                var newIssue = new IssueRecord();
                newIssue.CopyFrom(issue);
                newIssue.SeriesID = seriesMap[issue.SeriesID];
                var newId = ImportIssue(newIssue);
                importedIssues.Add(newIssue);
                issueMap[id] = newId;
            }

            // Fix up references between issues.
            foreach (var issue in importedIssues)
            {
                issue.IssueRefsReadPrev = issue.IssueRefsReadPrev
                    .Where(id => issueMap.ContainsKey(id))
                    .Select(id => issueMap[id])
                    .ToList();
            }

            // For good measure, mark all records dirty.
            mHeader.Dirty = true;
            foreach (var series in importedSeries)
            {
                series.Dirty = true;
            }

            foreach (var issue in importedIssues)
            {
                issue.Dirty = true;
            }
        }

        #endregion

        #endregion
        #region ----- Private table population methods -----

        private void Populate(IStorage aDatabase)
        {
            DetachDirtiedEvents();

            mAddedSeries = new Dictionary<int, SeriesRecord>();
            mDeletedSeries = new Dictionary<int, SeriesRecord>();
            if (aDatabase != null)
            {
                mSeries = aDatabase.GetRecords<SeriesRecord>();
            }
            else
            {
                mSeries = new Dictionary<int, SeriesRecord>();
            }

            mAddedIssues = new Dictionary<int, IssueRecord>();
            mDeletedIssues = new Dictionary<int, IssueRecord>();
            if (aDatabase != null)
            {
                mIssues = aDatabase.GetRecords<IssueRecord>();
            }
            else
            {
                mIssues = new Dictionary<int, IssueRecord>();
            }

            if (aDatabase != null)
            {
                Dictionary<int, InfoRecord> headers = aDatabase.GetRecords<InfoRecord>();
                int max = 0;
                foreach (int i in headers.Keys)
                {
                    if (i > max)
                    {
                        max = i;
                    }
                }

                mHeader = headers[max];
            }
            else
            {
                mHeader = new InfoRecord();
            }

            mDatabase = aDatabase;

            PopulateSeriesIssueLists();

            AttachDirtiedEvents();
            FireCollectionLoadedEvent();
        }


        private void AttachDirtiedEvents()
        {
            if (mHeader != null)
            {
                mHeader.OnRecordDirtied += mHeader_OnRecordDirtied;
            }

            if (mSeries != null)
            {
                foreach (SeriesRecord series in mSeries.Values)
                {
                    series.OnRecordDirtied += series_OnRecordDirtied;
                }
            }

            if (mIssues != null)
            {
                foreach (IssueRecord issue in mIssues.Values)
                {
                    issue.OnRecordDirtied += issue_OnRecordDirtied;
                }
            }
        }


        private void DetachDirtiedEvents()
        {
            if (mHeader != null)
            {
                mHeader.OnRecordDirtied -= mHeader_OnRecordDirtied;
            }

            if (mSeries != null)
            {
                foreach (SeriesRecord series in mSeries.Values)
                {
                    series.OnRecordDirtied -= series_OnRecordDirtied;
                }
            }

            if (mIssues != null)
            {
                foreach (IssueRecord issue in mIssues.Values)
                {
                    issue.OnRecordDirtied -= issue_OnRecordDirtied;
                }
            }
        }


        private void PopulateSeriesIssueLists()
        {
            foreach (SeriesRecord series in mSeries.Values)
            {
                series.Issues.Clear();
                if (string.IsNullOrEmpty(series.SortKey))
                {
                    series.GenerateSortKey();
                }
            }

            foreach (IssueRecord issue in mIssues.Values)
            {
                if (!mSeries.ContainsKey(issue.SeriesID))
                {
                    SeriesRecord s = new SeriesRecord();
                    s.ID = issue.SeriesID;
                    s.Title = "??? Reconstructed";
                    s.Dirty = true;
                    mSeries[s.ID] = s;
                    mAddedSeries[s.ID] = s;
                    mSaved = false;
                    FireSeriesModifiedEvent(s.ID, CollectionItemModificationType.Added);
                    s.OnRecordDirtied += series_OnRecordDirtied;
                    mHeader.NextSeriesID = Math.Max(mHeader.NextSeriesID, s.ID + 1);
                }

                SeriesRecord series = mSeries[issue.SeriesID];
                series.Issues.Add(issue.ID);
            }
        }


        private bool TableUpdateHelper<T>(Dictionary<int, T> aAllEntries, Dictionary<int, T> aAddedEntries, Dictionary<int, T> aDeletedEntries) where T : Record
        {
            bool result = true;

            foreach (T item in aAllEntries.Values)
            {
                if (!result)
                {
                    break;
                }

                if (item.Dirty && !aAddedEntries.ContainsKey(item.ID) && !aDeletedEntries.ContainsKey(item.ID))
                {
                    result &= mDatabase.Update<T>(item);
                }
            }

            foreach (T item in aDeletedEntries.Values)
            {
                if (!result)
                {
                    break;
                }

                result &= mDatabase.DeleteRecord<T>(item);
            }

            foreach (T item in aAddedEntries.Values)
            {
                if (!result)
                {
                    break;
                }

                result &= mDatabase.AddRecord<T>(item);
            }

            return result;
        }

        #endregion
        #region ----- Notification helpers -----

        private void FireCollectionLoadedEvent()
        {
            OnCollectionLoaded?.Invoke(this);
            FireCollectionPropertiesChangedEvent();
        }


        private void FireCollectionPropertiesChangedEvent()
        {
            OnCollectionPropertiesChanged?.Invoke(this);
        }


        private void FireSeriesModifiedEvent(int aSeriesID, CollectionItemModificationType aReason)
        {
            OnSeriesModified?.Invoke(this, aSeriesID, aReason);
            FireCollectionPropertiesChangedEvent();
        }


        private void FireIssueModifiedEvent(int aIssueID, CollectionItemModificationType aReason)
        {
            if (OnIssueModified != null)
            {
                OnIssueModified(this, aIssueID, aReason);
            }

            if (Issues.ContainsKey(aIssueID))
            {
                IssueRecord issue = Issues[aIssueID];
                FireSeriesModifiedEvent(issue.SeriesID, CollectionItemModificationType.Changed);
            }
        }

        #endregion
        #region ----- Record dirtied events -----

        private void mHeader_OnRecordDirtied(Record aRecord)
        {
            mSaved = false;
            mHeader.Timestamp = DateTime.Now;
            FireCollectionPropertiesChangedEvent();
        }

        private void series_OnRecordDirtied(Record aRecord)
        {
            SeriesRecord series = aRecord as SeriesRecord;
            if (series != null)
            {
                mSaved = false;
                FireSeriesModifiedEvent(series.ID, CollectionItemModificationType.Changed);
            }
        }

        private void issue_OnRecordDirtied(Record aRecord)
        {
            IssueRecord issue = aRecord as IssueRecord;
            if (issue != null)
            {
                mSaved = false;
                FireIssueModifiedEvent(issue.ID, CollectionItemModificationType.Changed);
            }
        }

        #endregion
        #region ----- Private data -----

        private static Schema sCurrentSchema;

        private IStorage mDatabase;

        private InfoRecord mHeader;

        private Dictionary<int, SeriesRecord> mSeries;
        private Dictionary<int, SeriesRecord> mAddedSeries;
        private Dictionary<int, SeriesRecord> mDeletedSeries;

        private Dictionary<int, IssueRecord> mIssues;
        private Dictionary<int, IssueRecord> mAddedIssues;
        private Dictionary<int, IssueRecord> mDeletedIssues;

        private bool mSaved = true;

        #endregion
    }
}
