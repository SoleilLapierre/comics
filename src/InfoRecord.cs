namespace Comics
{
    // Out in nature this would be generated code.
    public class InfoRecord : Record
    {
        public InfoRecord()
        {
            mVersion = Collection.CurrentSchema.Version;
            mSchema = Collection.CurrentSchema.ToString();
            mNextInfoID = 1;
            mNextIssueID = 1;
            mNextSeriesID = 1;
        }


        [StorageRequiredField(true)]
        public int Version
        {
            get { return mVersion; }
            set
            {
                mVersion = value;
                Dirty = true;
            }
        }


        [StorageName("VersionSchema")]
        public string Schema
        {
            get { return mSchema; }
            set
            {
                mSchema = value;
                Dirty = true;
            }
        }


        public int NextSeriesID
        {
            get { return mNextSeriesID; }
            set
            {
                mNextSeriesID = value;
                Dirty = true;
            }
        }


        public int NextIssueID
        {
            get { return mNextIssueID; }
            set
            {
                mNextIssueID = value;
                Dirty = true;
            }
        }


        public int NextInfoID
        {
            get { return mNextInfoID; }
            set
            {
                mNextInfoID = value;
                Dirty = true;
            }
        }


        public string CollectionName
        {
            get { return mName; }
            set
            {
                mName = value;
                Dirty = true;
            }
        }


        public string OwnerName
        {
            get { return mOwnerName; }
            set
            {
                mOwnerName = value;
                Dirty = true;
            }
        }


        private int mVersion;
        private string mSchema;
        private int mNextSeriesID;
        private int mNextIssueID;
        private int mNextInfoID;
        private string mName;
        private string mOwnerName;
    }
}
