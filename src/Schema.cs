using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Comics
{
    public class Schema
    {
        public class Field
        {
            public enum FieldType
            {
                Invalid = -1,
                Int8 = 0,
                Int16,
                Int32,
                Int64,
                Uint8,
                Uint16,
                Uint32,
                Uint64,
                Decimal,
                Float,
                Double,
                DateTime,
                String,
                ByteArray,
                Int32List
            };

            static Field()
            {
                sFieldTypeMap.Add(typeof(Byte), FieldType.Uint8);
                sFieldTypeMap.Add(typeof(SByte), FieldType.Int8);
                sFieldTypeMap.Add(typeof(Int16), FieldType.Int16);
                sFieldTypeMap.Add(typeof(Int32), FieldType.Int32);
                sFieldTypeMap.Add(typeof(Int64), FieldType.Int64);
                sFieldTypeMap.Add(typeof(UInt16), FieldType.Uint16);
                sFieldTypeMap.Add(typeof(UInt32), FieldType.Uint32);
                sFieldTypeMap.Add(typeof(UInt64), FieldType.Uint64);
                sFieldTypeMap.Add(typeof(Single), FieldType.Float);
                sFieldTypeMap.Add(typeof(Double), FieldType.Double);
                sFieldTypeMap.Add(typeof(Char), FieldType.Uint16);
                sFieldTypeMap.Add(typeof(Decimal), FieldType.Decimal);
                sFieldTypeMap.Add(typeof(String), FieldType.String);
                sFieldTypeMap.Add(typeof(DateTime), FieldType.DateTime);
                sFieldTypeMap.Add(typeof(Byte[]), FieldType.ByteArray);
                sFieldTypeMap.Add(typeof(List<Int32>), FieldType.Int32List);
            }

            public Field()
            {
            }

            public Field(Field aOther)
            {
                mType = aOther.mType;
                mFieldName = aOther.mFieldName;
                mPropertyName = aOther.mPropertyName;
                mRequired = aOther.mRequired;
            }

            public static FieldType GetFieldType(Type aType)
            {
                FieldType ft = FieldType.Invalid;

                sFieldTypeMap.TryGetValue(aType, out ft);

                return ft;
            }

            public FieldType Type
            {
                get { return mType; }
                set { mType = value; }
            }
             
            public string DatabaseName
            {
                get { return mFieldName; }
                set { mFieldName = value; }
            }

            public string PropertyName
            {
                get { return mPropertyName; }
                set { mPropertyName = value; }
            }

            public bool Required
            {
                get { return mRequired; }
                set { mRequired = value; }
            }

            public XmlNode ToXML(XmlDocument aDoc, int aKey)
            {
                XmlElement fieldElem = aDoc.CreateElement("Field");

                fieldElem.SetAttribute("Index", aKey.ToString());
                fieldElem.SetAttribute("Name", mFieldName);
                fieldElem.SetAttribute("PropertyName", mPropertyName);
                fieldElem.SetAttribute("Required", mRequired.ToString());
                fieldElem.SetAttribute("Type", mType.ToString());

                return fieldElem;
            }

            private FieldType mType = FieldType.Invalid;
            private string mFieldName = null;
            private string mPropertyName = null;
            private bool mRequired = false;

            private static readonly Dictionary<Type, FieldType> sFieldTypeMap = new Dictionary<Type, FieldType>();
        };


        public class Table
        {
            public Table()
            {
            }

            public Table(Table aOther)
            {
                mFields = new List<Field>(aOther.mFields.Count);
                foreach (Field f in aOther.mFields)
                {
                    mFields.Add(new Field(f));
                }

                mKeyField = aOther.mKeyField;
                mTimestampField = aOther.mTimestampField;
                mName = aOther.mName;
            }

            public System.Collections.ObjectModel.ReadOnlyCollection<Field> Fields => mFields.AsReadOnly();

            public int KeyField
            {
                get { return mKeyField; }
                set { mKeyField = value; }
            }

            public int TimestampField
            {
                get { return mTimestampField; }
                set { mTimestampField = value; }
            }

            public string Name
            {
                get { return mName; }
                set { mName = value; }
            }

            public int AddField(Field aField)
            {
                int index = mFields.Count;
                mFields.Add(aField);
                return index;
            }

            public XmlNode ToXML(XmlDocument aDoc, Type aKey)
            {
                XmlElement tableElem = aDoc.CreateElement("Table");

                tableElem.SetAttribute("Type", aKey.Name);
                tableElem.SetAttribute("Name", mName);
                tableElem.SetAttribute("KeyField", mKeyField.ToString());
                tableElem.SetAttribute("TimestampField", mTimestampField.ToString());

                XmlElement fields = aDoc.CreateElement("Fields");
                for (int i = 0; i < mFields.Count; i++)
                {
                    Field field = mFields[i];
                    fields.AppendChild(field.ToXML(aDoc, i));
                }
                tableElem.AppendChild(fields);

                return tableElem;
            }

            private readonly List<Field> mFields = new List<Field>();
            private int mKeyField = -1;
            private int mTimestampField = -1;
            private string mName = null;
        };


        public Schema(int aVersion)
        {
            mVersion = aVersion;
        }

        public Schema(Schema aOther)
        {
            mTableMap = new Dictionary<Type, Table>(aOther.mTableMap.Count);
            foreach (Type aType in aOther.mTableMap.Keys)
            {
                mTableMap[aType] = new Table(aOther.mTableMap[aType]);
            }

            mVersion = aOther.mVersion;
        }

        public int Version => mVersion;

        public System.Collections.ObjectModel.ReadOnlyCollection<Table> Tables
        {
            get 
            {
                List<Table> list = new List<Table>(mTableMap.Values);
                return list.AsReadOnly();
            }
        }

        public Table AddTable(Type aType)
        {
            Table table = MakeTableDefinition(aType);
            mTableMap[aType] = table;
            return table;
        }

        public Table this[Type aType] => mTableMap[aType];

        private Table MakeTableDefinition(Type aType)
        {
            Table table = new Table();

            object[] nameAttribs = aType.GetCustomAttributes(typeof(StorageNameAttribute), false);
            if (nameAttribs.Length > 0)
            {
                StorageNameAttribute nameAttrib = nameAttribs[0] as StorageNameAttribute;
                table.Name = nameAttrib.ElementName;
            }
            else
            {
                table.Name = aType.Name;
            }

            foreach (PropertyInfo pi in aType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (pi.GetCustomAttributes(typeof(XmlIgnoreAttribute), false).Length < 1)
                {
                    Field field = new Field();
                    field.PropertyName = pi.Name;

                    object[] propNameAttribs = pi.GetCustomAttributes(typeof(StorageNameAttribute), false);
                    if (propNameAttribs.Length > 0)
                    {
                        StorageNameAttribute propNameAttr = propNameAttribs[0] as StorageNameAttribute;
                        field.DatabaseName = propNameAttr.ElementName;
                    }
                    else
                    {
                        field.DatabaseName = pi.Name;
                    }

                    field.Required = (pi.GetCustomAttributes(typeof(StorageRequiredFieldAttribute), true).Length > 0);

                    int index = table.AddField(field);

                    if (pi.GetCustomAttributes(typeof(StorageKeyAttribute), true).Length > 0)
                    {
                        if (table.KeyField != -1)
                        {
                            throw new ArgumentException(string.Format("Type {0} has more than one property with the StorageKey attribute.", aType.Name));
                        }

                        table.KeyField = index;                        
                    }

                    if (pi.GetCustomAttributes(typeof(StorageTimestampAttribute), true).Length > 0)
                    {
                        if (table.TimestampField != -1)
                        {
                            throw new ArgumentException(string.Format("Type {0} has more than one property with the StorageTimestamp attribute.", aType.Name));
                        }
                        else if (pi.PropertyType != typeof(DateTime))
                        {
                            throw new ArgumentException(string.Format("Type {0} has a non-DateTime property with the StorageTimestamp attribute.", aType.Name));
                        }

                        table.TimestampField = index;
                    }

                    field.Type = Field.GetFieldType(pi.PropertyType);

                    if (field.Type == Field.FieldType.Invalid)
                    {
                        throw new ArgumentException(string.Format("Type {0} has a property with an unrecognized type: {1}.", aType.Name, pi.PropertyType.Name));
                    }
                }
            }

            if (table.KeyField == -1)
            {
                throw new ArgumentException(string.Format("Type {0} lacks a primary key field identified by the StorageKey attribute.", aType.Name));
            }
            else if (table.TimestampField == -1)
            {
                throw new ArgumentException(string.Format("Type {0} lacks a DateTime field marked with the StorageTimestamp attribute.", aType.Name));
            }

            return table;
        }


        public string Serialize()
        {
            XmlDocument doc = new XmlDocument();

            XmlElement main = doc.CreateElement("CollectionSchema");
            main.SetAttribute("Version", mVersion.ToString());
            doc.AppendChild(main);

            XmlElement tables = doc.CreateElement("Tables");
            foreach (Type type in mTableMap.Keys)
            {
                Table table = mTableMap[type];
                tables.AppendChild(table.ToXML(doc, type));
            }
            main.AppendChild(tables);

            return doc.OuterXml;
        }


        private Dictionary<Type, Table> mTableMap = new Dictionary<Type, Table>();
        private int mVersion;
    }
}
