using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Comics
{
    public partial class IssueListControl : UserControl
    {
        #region ----- Public methods and properties ----

        public delegate void IssueListSelectionChangedEvent(Collection aCollection, List<int> aSelectedIssues);
        public event IssueListSelectionChangedEvent OnSelectionChanged;

        public IssueListControl()
        {
            InitializeComponent();
            mIssueListGrid.AutoGenerateColumns = true;
            mIssueListBindingSource.DataSource = mBindingList;
        }


        public Collection Collection
        {
            set 
            { 
                mCollection = value;
                mCurrentSelection = null;
                UpdateWidgets();
                UpdateStatusBar();
            }
        }


        public IIssueFilter Filter { get; set; }


        public void ShowIssues(Collection aCollection, int aLastSelectedSeriesID, List<int> aIssueList)
        {
            if (aCollection != mCollection)
            {
                throw new InvalidOperationException("IssueListControl.ShowIssues passed the wrong collection.");
            }

            mCurrentContent = aIssueList;
            mDefaultSeriesForAdd = aLastSelectedSeriesID;

            RefreshContent();
            mCurrentSelection = null;
        }


        public void RefreshContent()
        {
            mIssueListGrid.ClearSelection();
            mBindingList.Clear();

            List<int> selectedRows = new List<int>();

            int rowIndex = 0;
            foreach (int issueIndex in mCurrentContent)
            {
                if (Filter != null)
                {
                    if (mCollection.Issues.ContainsKey(issueIndex))
                    {
                        if (!Filter.IncludeIssue(mCollection.Issues[issueIndex]))
                        {
                            continue;
                        }
                    }
                }

                mBindingList.Add(new IssueDataGridWrapper(mCollection, issueIndex));
                if (mCurrentSelection.Contains(issueIndex))
                {
                    selectedRows.Add(rowIndex);
                }
                rowIndex++;
            }

            if (mIssueListGrid.CurrentRow != null)
            {
                mIssueListGrid.CurrentRow.Selected = false;
            }

            foreach (int i in selectedRows)
            {
                mIssueListGrid.Rows[i].Selected = true;
                mIssueListGrid.CurrentCell = mIssueListGrid.Rows[i].Cells[0];
            }

            UpdateWidgets();
            UpdateStatusBar();
            Invalidate(true);
        }


        public void TriggerRedraw()
        {
            mIssueListGrid.Refresh();
        }

        #endregion
        #region ----- Private functonality -----

        private void UpdateStatusBar()
        {
            SortedList<int, int> seriesSet = new SortedList<int, int>();
            foreach (IssueDataGridWrapper issue in mBindingList)
            {
                IssueRecord record = issue.GetIssueRecord();
                if ((record != null) && !seriesSet.ContainsKey(record.SeriesID))
                {
                    seriesSet.Add(record.SeriesID, 0);
                }
            }

            int numSelected = 0;
            if (mCurrentSelection != null)
            {
                numSelected = mCurrentSelection.Count;
            }

            mStatusBarText.Text = String.Format("{0} issues from {1} series shown; {2} selected.", mBindingList.Count, seriesSet.Count, numSelected);
        }


        private void UpdateWidgets()
        {
            mIssueListMenuIssueAddButton.Enabled = (mDefaultSeriesForAdd != -1);
            if (mCurrentSelection == null)
            {
                mIssueListMenuIssueDeleteButton.Enabled = false;
            }
            else
            {
                mIssueListMenuIssueDeleteButton.Enabled = (mCurrentSelection.Count > 0);
            }
        }

        #endregion
        #region ----- Add/delete events -----

        private void mIssueListMenuIssueAddButton_Click(object sender, EventArgs e)
        {
            if (mDefaultSeriesForAdd == -1)
            {
                return;
            }

            IssueRecord issue = new IssueRecord();
            issue.SeriesID = mDefaultSeriesForAdd;
            issue.DateAdded = DateTime.Now;

            SeriesRecord series = mCollection.Series[mDefaultSeriesForAdd];
            if (series.TryGuessNextIssueNumber(mCollection, out var num))
            {
                issue.Issue = num.ToString();
            }

            int index = mCollection.AddIssue(issue);
            mBindingList.Add(new IssueDataGridWrapper(mCollection, index));

            List<int> issues = new List<int>
            {
                index
            };
            mCurrentSelection = issues;
            FireSelectionChangedEvent(mCurrentSelection);

            UpdateWidgets();
            UpdateStatusBar();
            Invalidate(true);
        }


        private void mIssueListMenuIssueAddRangeButton_Click(object sender, EventArgs e)
        {
            if (mDefaultSeriesForAdd == -1)
            {
                return;
            }

            Range r = Range.FromString(mIssueListMenuRangeText.Text);
            if (r != null)
            {
                List<int> issues = new List<int>();

                foreach (int i in r)
                {
                    IssueRecord issue = new IssueRecord();
                    issue.SeriesID = mDefaultSeriesForAdd;
                    issue.DateAdded = DateTime.Now;
                    issue.Issue = i.ToString();

                    int index = mCollection.AddIssue(issue);
                    mBindingList.Add(new IssueDataGridWrapper(mCollection, index));

                    issues.Add(index);
                }

                mCurrentSelection = issues;
                FireSelectionChangedEvent(mCurrentSelection);

                UpdateWidgets();
                UpdateStatusBar();
                Invalidate(true);
            }
        }


        private void mIssueListMenuIssueDeleteButton_Click(object sender, EventArgs e)
        {
            mEnableSelectionChangedEvent = false;
            List<int> toDelete = mCurrentSelection ?? new List<int>();
            mCurrentSelection = new List<int>();
            mIssueListGrid.ClearSelection();
            mBindingList.Clear();

            foreach (int id in toDelete)
            {
                mCurrentContent.Remove(id);
                mCollection.DeleteIssue(id);
            }

            RefreshContent();
            mEnableSelectionChangedEvent = true;
        }

        #endregion
        #region ----- Issue selection events -----

        private void FireSelectionChangedEvent(List<int> aSelectedIssues)
        {
            OnSelectionChanged?.Invoke(mCollection, aSelectedIssues);
        }

        private void mIssueListGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (!mEnableSelectionChangedEvent)
            {
                return;
            }

            List<int> selected = new List<int>();

            DataGridViewSelectedRowCollection selectedRows = mIssueListGrid.SelectedRows;
            foreach (DataGridViewRow row in selectedRows)
            {
                IssueDataGridWrapper item = row.DataBoundItem as IssueDataGridWrapper;
                selected.Add(item.GetIssueRecord().ID);
            }

            FireSelectionChangedEvent(selected);
            mCurrentSelection = selected;
            UpdateWidgets();
            UpdateStatusBar();
        }

        private void mIssueListGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo hit = mIssueListGrid.HitTest(e.X, e.Y);
                if (hit.RowIndex >= 0)
                {
                    List<int> selectedIssues = new List<int>();

                    if (mIssueListGrid.Rows[hit.RowIndex].Selected)
                    {
                        foreach (DataGridViewRow row in mIssueListGrid.SelectedRows)
                        {
                            IssueDataGridWrapper item = row.DataBoundItem as IssueDataGridWrapper;
                            selectedIssues.Add(item.GetIssueRecord().ID);
                        }
                    }
                    else
                    {
                        IssueDataGridWrapper item = mIssueListGrid.Rows[hit.RowIndex].DataBoundItem as IssueDataGridWrapper;
                        selectedIssues.Add(item.GetIssueRecord().ID);
                    }


                    if (selectedIssues.Count > 0)
                    {
                        mIssueListGrid.DoDragDrop(selectedIssues, DragDropEffects.Link);
                    }
                }
            }
        }

        #endregion
        #region ----- Private data and types -----

        private BindingList<IssueDataGridWrapper> mBindingList = new BindingList<IssueDataGridWrapper>();
        private Collection mCollection;
        private int mDefaultSeriesForAdd = -1;
        private List<int> mCurrentSelection;
        private List<int> mCurrentContent = new List<int>();
        private bool mEnableSelectionChangedEvent = true;

        private class IssueDataGridWrapper
        {
            private Collection mCollection;
            private int mIssueIndex;

            public IssueDataGridWrapper(Collection aCollection, int aRecordIndex)
            {
                mCollection = aCollection;
                mIssueIndex = aRecordIndex;
            }

            public IssueRecord GetIssueRecord()
            {
                if (mCollection.Issues.ContainsKey(mIssueIndex))
                {
                    return mCollection.Issues[mIssueIndex];
                }

                return null;
            }

            public string Unsaved
            {
                get
                {
                    string result = "";
                    if (GetIssueRecord().Dirty)
                    {
                        result = "*";
                    }
                    return result;
                }
            }

            public string Series => mCollection.Series[GetIssueRecord().SeriesID].FullTitle;

            public string Title => GetIssueRecord().Title;

            public string Issue => GetIssueRecord().Issue;

            public decimal Cost => GetIssueRecord().Cost;

            public string Condition => GetIssueRecord().Condition;

            public string Storage => GetIssueRecord().Storage;

            public DateTime DateAdded => GetIssueRecord().DateAdded;

            public string IssueType => GetIssueRecord().IssueType;

            public UInt32 NumCopies => GetIssueRecord().NumCopies;
        }

        #endregion 
    }
}
