namespace Comics
{
    partial class CollectionViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mMenuStrip = new System.Windows.Forms.MenuStrip();
            this.mViewerMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mViewerMenuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mViewerMenuFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mStatusBar = new System.Windows.Forms.StatusStrip();
            this.mTopLevelSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mLeftSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mTreeViewSplitContainer = new System.Windows.Forms.SplitContainer();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSeriesTreeViewControl = new Comics.SeriesTreeViewControl();
            this.mSearchTreeViewControl = new Comics.SearchTreeViewControl();
            this.mIssueListControl = new Comics.IssueListControl();
            this.mIssueEditControl = new Comics.IssueEditControl();
            this.mViewerMenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mViewerMenuEditProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuStrip.SuspendLayout();
            this.mTopLevelSplitContainer.Panel1.SuspendLayout();
            this.mTopLevelSplitContainer.Panel2.SuspendLayout();
            this.mTopLevelSplitContainer.SuspendLayout();
            this.mLeftSplitContainer.Panel1.SuspendLayout();
            this.mLeftSplitContainer.Panel2.SuspendLayout();
            this.mLeftSplitContainer.SuspendLayout();
            this.mTreeViewSplitContainer.Panel1.SuspendLayout();
            this.mTreeViewSplitContainer.Panel2.SuspendLayout();
            this.mTreeViewSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // mMenuStrip
            // 
            this.mMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mViewerMenuFile,
            this.mViewerMenuEdit});
            this.mMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mMenuStrip.Name = "mMenuStrip";
            this.mMenuStrip.Size = new System.Drawing.Size(1102, 24);
            this.mMenuStrip.TabIndex = 0;
            this.mMenuStrip.Text = "menuStrip1";
            // 
            // mViewerMenuFile
            // 
            this.mViewerMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mViewerMenuFileSave,
            this.mViewerMenuFileClose});
            this.mViewerMenuFile.Name = "mViewerMenuFile";
            this.mViewerMenuFile.Size = new System.Drawing.Size(35, 20);
            this.mViewerMenuFile.Text = "File";
            // 
            // mViewerMenuFileSave
            // 
            this.mViewerMenuFileSave.Enabled = false;
            this.mViewerMenuFileSave.Name = "mViewerMenuFileSave";
            this.mViewerMenuFileSave.Size = new System.Drawing.Size(152, 22);
            this.mViewerMenuFileSave.Text = "Save";
            this.mViewerMenuFileSave.Click += new System.EventHandler(this.mViewerMenuFileSave_Click);
            // 
            // mViewerMenuFileClose
            // 
            this.mViewerMenuFileClose.Name = "mViewerMenuFileClose";
            this.mViewerMenuFileClose.Size = new System.Drawing.Size(152, 22);
            this.mViewerMenuFileClose.Text = "Close Window";
            this.mViewerMenuFileClose.Click += new System.EventHandler(this.mViewerMenuFileClose_Click);
            // 
            // mStatusBar
            // 
            this.mStatusBar.Location = new System.Drawing.Point(0, 645);
            this.mStatusBar.Name = "mStatusBar";
            this.mStatusBar.Size = new System.Drawing.Size(1102, 22);
            this.mStatusBar.TabIndex = 1;
            this.mStatusBar.Text = "statusStrip1";
            // 
            // mTopLevelSplitContainer
            // 
            this.mTopLevelSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTopLevelSplitContainer.Location = new System.Drawing.Point(0, 24);
            this.mTopLevelSplitContainer.Name = "mTopLevelSplitContainer";
            // 
            // mTopLevelSplitContainer.Panel1
            // 
            this.mTopLevelSplitContainer.Panel1.Controls.Add(this.mLeftSplitContainer);
            // 
            // mTopLevelSplitContainer.Panel2
            // 
            this.mTopLevelSplitContainer.Panel2.Controls.Add(this.mIssueEditControl);
            this.mTopLevelSplitContainer.Size = new System.Drawing.Size(1102, 621);
            this.mTopLevelSplitContainer.SplitterDistance = 638;
            this.mTopLevelSplitContainer.TabIndex = 2;
            // 
            // mLeftSplitContainer
            // 
            this.mLeftSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mLeftSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mLeftSplitContainer.Name = "mLeftSplitContainer";
            // 
            // mLeftSplitContainer.Panel1
            // 
            this.mLeftSplitContainer.Panel1.Controls.Add(this.mTreeViewSplitContainer);
            // 
            // mLeftSplitContainer.Panel2
            // 
            this.mLeftSplitContainer.Panel2.Controls.Add(this.mIssueListControl);
            this.mLeftSplitContainer.Size = new System.Drawing.Size(638, 621);
            this.mLeftSplitContainer.SplitterDistance = 243;
            this.mLeftSplitContainer.TabIndex = 0;
            // 
            // mTreeViewSplitContainer
            // 
            this.mTreeViewSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTreeViewSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mTreeViewSplitContainer.Name = "mTreeViewSplitContainer";
            this.mTreeViewSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mTreeViewSplitContainer.Panel1
            // 
            this.mTreeViewSplitContainer.Panel1.Controls.Add(this.mSeriesTreeViewControl);
            // 
            // mTreeViewSplitContainer.Panel2
            // 
            this.mTreeViewSplitContainer.Panel2.Controls.Add(this.mSearchTreeViewControl);
            this.mTreeViewSplitContainer.Size = new System.Drawing.Size(243, 621);
            this.mTreeViewSplitContainer.SplitterDistance = 284;
            this.mTreeViewSplitContainer.TabIndex = 0;
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // mSeriesTreeViewControl
            // 
            this.mSeriesTreeViewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSeriesTreeViewControl.Location = new System.Drawing.Point(0, 0);
            this.mSeriesTreeViewControl.Name = "mSeriesTreeViewControl";
            this.mSeriesTreeViewControl.Size = new System.Drawing.Size(243, 284);
            this.mSeriesTreeViewControl.TabIndex = 1;
            // 
            // mSearchTreeViewControl
            // 
            this.mSearchTreeViewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSearchTreeViewControl.Location = new System.Drawing.Point(0, 0);
            this.mSearchTreeViewControl.Name = "mSearchTreeViewControl";
            this.mSearchTreeViewControl.Size = new System.Drawing.Size(243, 333);
            this.mSearchTreeViewControl.TabIndex = 0;
            // 
            // mIssueListControl
            // 
            this.mIssueListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssueListControl.Location = new System.Drawing.Point(0, 0);
            this.mIssueListControl.Name = "mIssueListControl";
            this.mIssueListControl.Size = new System.Drawing.Size(391, 621);
            this.mIssueListControl.TabIndex = 0;
            // 
            // mIssueEditControl
            // 
            this.mIssueEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssueEditControl.Location = new System.Drawing.Point(0, 0);
            this.mIssueEditControl.Name = "mIssueEditControl";
            this.mIssueEditControl.Size = new System.Drawing.Size(460, 621);
            this.mIssueEditControl.TabIndex = 0;
            // 
            // mViewerMenuEdit
            // 
            this.mViewerMenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mViewerMenuEditProperties});
            this.mViewerMenuEdit.Name = "mViewerMenuEdit";
            this.mViewerMenuEdit.Size = new System.Drawing.Size(37, 20);
            this.mViewerMenuEdit.Text = "Edit";
            // 
            // mViewerMenuEditProperties
            // 
            this.mViewerMenuEditProperties.Name = "mViewerMenuEditProperties";
            this.mViewerMenuEditProperties.Size = new System.Drawing.Size(183, 22);
            this.mViewerMenuEditProperties.Text = "Collection Properties";
            this.mViewerMenuEditProperties.Click += new System.EventHandler(this.mViewerMenuEditProperties_Click);
            // 
            // CollectionViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 667);
            this.Controls.Add(this.mTopLevelSplitContainer);
            this.Controls.Add(this.mStatusBar);
            this.Controls.Add(this.mMenuStrip);
            this.MainMenuStrip = this.mMenuStrip;
            this.Name = "CollectionViewer";
            this.ShowIcon = false;
            this.Text = "Collection View";
            this.mMenuStrip.ResumeLayout(false);
            this.mMenuStrip.PerformLayout();
            this.mTopLevelSplitContainer.Panel1.ResumeLayout(false);
            this.mTopLevelSplitContainer.Panel2.ResumeLayout(false);
            this.mTopLevelSplitContainer.ResumeLayout(false);
            this.mLeftSplitContainer.Panel1.ResumeLayout(false);
            this.mLeftSplitContainer.Panel2.ResumeLayout(false);
            this.mLeftSplitContainer.ResumeLayout(false);
            this.mTreeViewSplitContainer.Panel1.ResumeLayout(false);
            this.mTreeViewSplitContainer.Panel2.ResumeLayout(false);
            this.mTreeViewSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mMenuStrip;
        private System.Windows.Forms.StatusStrip mStatusBar;
        private System.Windows.Forms.SplitContainer mTopLevelSplitContainer;
        private System.Windows.Forms.SplitContainer mLeftSplitContainer;
        private System.Windows.Forms.SplitContainer mTreeViewSplitContainer;
        private SearchTreeViewControl mSearchTreeViewControl;
        private IssueListControl mIssueListControl;
        private IssueEditControl mIssueEditControl;
        private System.Windows.Forms.ToolStripMenuItem mViewerMenuFile;
        private System.Windows.Forms.ToolStripMenuItem mViewerMenuFileSave;
        private System.Windows.Forms.ToolStripMenuItem mViewerMenuFileClose;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private SeriesTreeViewControl mSeriesTreeViewControl;
        private System.Windows.Forms.ToolStripMenuItem mViewerMenuEdit;
        private System.Windows.Forms.ToolStripMenuItem mViewerMenuEditProperties;
    }
}