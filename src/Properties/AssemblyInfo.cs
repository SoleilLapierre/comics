﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Comics")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("BogoWare")]
[assembly: AssemblyProduct("Comics")]
[assembly: AssemblyCopyright("Copyright © Soleil Lapierre 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = true)]
//[assembly: FileIOPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
//[assembly: ReflectionPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
//[assembly: RegistryPermission(SecurityAction.RequestMinimum, All = "*")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d0331f9f-f583-4412-a522-9d6b8ec09d81")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]
