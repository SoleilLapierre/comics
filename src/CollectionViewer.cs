using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Comics
{
    public partial class CollectionViewer : Form
    {
        #region ----- Public construction and properties -----

        static CollectionViewer()
        {
            sWindowSequenceNumber = 1;
        }

        public CollectionViewer()
        : this(null)
        {
        }


        public CollectionViewer(Collection aCollection)
        {
            mWindowSequenceNumber = sWindowSequenceNumber;
            sWindowSequenceNumber++;

            InitializeComponent();
            mCollection = aCollection;
            mIssueEditControl.Collection = mCollection;
            mIssueListControl.Collection = mCollection;
            mSeriesTreeViewControl.Collection = mCollection;
            mSeriesTreeViewControl.OnSeriesSelectionChanged += new SeriesTreeViewControl.SeriesSelectionChangedEvent(mSeriesTreeViewControl_OnSeriesSelectionChanged);
            mIssueListControl.OnSelectionChanged += new IssueListControl.IssueListSelectionChangedEvent(mIssueListControl_OnSelectionChanged);
            mIssueEditControl.OnIssuesEdited += new IssueEditControl.IssueEditPerformedEvent(mIssueEditControl_OnIssuesEdited);
            mCollection.OnCollectionLoaded += new Collection.CollectionLoadedEvent(mCollection_OnCollectionLoaded);
            mCollection.OnCollectionPropertiesChanged += new Collection.CollectionPropertiesChangedEvent(mCollection_OnCollectionPropertiesChanged);
            mCollection.OnSeriesModified += new Collection.SeriesModifiedEvent(mCollection_OnSeriesModified);
            mCollection.OnIssueModified += new Collection.IssueModifiedEvent(mCollection_OnIssueModified);

            mSeriesTreeViewControl.Filter = mSearchTreeViewControl;
            mIssueListControl.Filter = mSearchTreeViewControl;

            mSearchTreeViewControl.OnSearchUpdated += () =>
            {
                mSeriesTreeViewControl.RefreshContent();
                mIssueListControl.RefreshContent();
            };

            UpdateWindowTitle();
        }


        public Collection Collection
        {
            get { return mCollection; }
        }

        #endregion
        #region ----- Private functionality -----

        private void UpdateWindowTitle()
        {
            string title = "(untitled collection)";
            if (mCollection != null)
            {
                string name = mCollection.InfoRecord.CollectionName;
                if (name != null)
                {
                    title = name;
                }

                if (mCollection.InfoRecord.OwnerName != null)
                {
                    title += " (" + name + ")";
                }

                if (!mCollection.Saved)
                {
                    title += " (UNSAVED)";
                }
            }

            title += " (" + mWindowSequenceNumber.ToString() + ")";

            Text = title;
        }

        #endregion
        #region ----- Menu event handlers -----

        private void mViewerMenuFileClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void mViewerMenuFileSave_Click(object sender, EventArgs e)
        {
            mCollection.Save();
        }


        private void mViewerMenuEditProperties_Click(object sender, EventArgs e)
        {
            CollectionPropertiesDialog dlg = new CollectionPropertiesDialog();
            dlg.CollectionName = mCollection.InfoRecord.CollectionName;
            dlg.CollectionOwner = mCollection.InfoRecord.OwnerName;
            dlg.Version = mCollection.InfoRecord.Version;
            dlg.LastModified = mCollection.InfoRecord.Timestamp;
            DialogResult dr = dlg.ShowDialog();

            if (dr == DialogResult.OK)
            {
                mCollection.InfoRecord.CollectionName = dlg.CollectionName;
                mCollection.InfoRecord.OwnerName = dlg.CollectionOwner;
            }
        }

        #endregion
        #region ----- Selection event handlers -----

        void mSeriesTreeViewControl_OnSeriesSelectionChanged(Collection aCollection, int aLastSelectedSeriesID, List<SeriesRecord> aSelectedSeries)
        {
            if (aCollection != mCollection)
            {
                throw new InvalidOperationException("CollectionViewer.OnSeriesSelectonChanged called with wrong collection.");
            }

            List<int> issues = new List<int>();

            foreach (SeriesRecord series in aSelectedSeries)
            {
                issues.AddRange(series.Issues);
            }

            // !!! issues.Sort();

            mIssueListControl.ShowIssues(mCollection, aLastSelectedSeriesID, issues);
        }


        void mIssueListControl_OnSelectionChanged(Collection aCollection, List<int> aSelectedIssues)
        {
            if (aCollection != mCollection)
            {
                throw new InvalidOperationException("CollectionViewer.OnIssueSelectionChanged called with wrong collection.");
            }

            mIssueEditControl.EditIssues(aSelectedIssues);
        }


        void mIssueEditControl_OnIssuesEdited(Collection aCollection, List<int> aEditedIssues)
        {
            if (aCollection != mCollection)
            {
                throw new InvalidOperationException("CollectionViewer.OnIssuesEdited called with wrong collection.");
            }

            mIssueListControl.TriggerRedraw();
        }

        #endregion
        #region ----- Window update events -----

        void mCollection_OnCollectionLoaded(Collection aCollection)
        {
            UpdateWindowTitle();
            mViewerMenuFileSave.Enabled = !mCollection.Saved;
            mIssueListControl.RefreshContent();
            Invalidate(true);
        }

        void mCollection_OnCollectionPropertiesChanged(Collection aCollection)
        {
            UpdateWindowTitle();
            mViewerMenuFileSave.Enabled = !mCollection.Saved;
        }

        void mCollection_OnSeriesModified(Collection aCollection, int aSeriesID, Collection.CollectionItemModificationType aReason)
        {
            UpdateWindowTitle();
            mViewerMenuFileSave.Enabled = !mCollection.Saved;
        }

        void mCollection_OnIssueModified(Collection aCollection, int aIssueID, Collection.CollectionItemModificationType aReason)
        {
            UpdateWindowTitle();
            mViewerMenuFileSave.Enabled = !mCollection.Saved;
            if (aReason != Comics.Collection.CollectionItemModificationType.Deleted) // Handled elsewhere.
            {
                mIssueListControl.Refresh();
            }
        }

        #endregion
        #region ----- Private data -----

        private readonly Collection mCollection;

        private static int sWindowSequenceNumber;
        private int mWindowSequenceNumber;

        #endregion
    }
}