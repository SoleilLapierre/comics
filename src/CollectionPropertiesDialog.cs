using System;
using System.Windows.Forms;

namespace Comics
{
    public partial class CollectionPropertiesDialog : Form
    {
        public CollectionPropertiesDialog()
        {
            InitializeComponent();
        }


        private void mButtonApply_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }


        private void mButtonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }


        public string CollectionName
        {
            get { return mTextCollectionName.Text; }
            set { mTextCollectionName.Text = value; }
        }


        public string CollectionOwner
        {
            get { return mTextCollectionOwner.Text; }
            set { mTextCollectionOwner.Text = value; }
        }


        public int Version
        {
            set { mLabelVersion.Text = "Version: " + value.ToString(); }
        }


        public DateTime LastModified
        {
            set { mLabelLastEdit.Text = "Last modified: " + value.ToString(); }
        }
    }
}