﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Comics
{
    public partial class SearchCard : UserControl, ISeriesFilter, IIssueFilter
    {
        public delegate void SearchDeletedEvent(SearchCard aSearch);
        public event SearchDeletedEvent OnSearchDeleted;

        public delegate void SearchUpdatedEvent(SearchCard aSearch);
        public event SearchUpdatedEvent OnSearchUpdated;

        public SearchCard()
        {
            InitializeComponent();
            mSearchRecordTypeSelection.SelectedIndex = 0;
            mMatchOpSelection.SelectedIndex = 1;
        }


        public bool IncludeIssue(IssueRecord aIssue)
        {
            if (mSearchEnabledCheck.Checked && mSearchRecordTypeSelection.SelectedItem as string == "Issue")
            {
                if (mProperty is null)
                {
                    mProperty = typeof(IssueRecord).GetProperty(mFieldSelection.SelectedItem as string, BindingFlags.Instance | BindingFlags.Public);
                }

                if (mProperty != null)
                {
                    var val = mProperty.GetValue(aIssue) ?? "";
                    var test = mMatchValueBox.Text;
                    var result = Match(val, test);
                    if (result.HasValue)
                    {
                        return result.Value;
                    }
                }
            }

            return true;
        }


        public bool IncludeSeries(SeriesRecord aSeries)
        {
            if (mSearchEnabledCheck.Checked && mSearchRecordTypeSelection.SelectedItem as string == "Series")
            {
                if (mProperty is null)
                {
                    mProperty = typeof(SeriesRecord).GetProperty(mFieldSelection.SelectedItem as string, BindingFlags.Instance | BindingFlags.Public);
                }

                if (mProperty != null)
                {
                    var val = mProperty.GetValue(aSeries) ?? "";
                    var test = mMatchValueBox.Text;
                    var result = Match(val, test);
                    if (result.HasValue)
                    {
                        return result.Value;
                    }
                }
            }

            return true;
        }


        private bool? Match(object val, string test)
        {
            switch (mMatchOpSelection.SelectedItem as string)
            {
                case "Contains":
                    return val.ToString().ToLowerInvariant().Contains(test.ToLowerInvariant());
                case "=":
                    return val.ToString().ToLowerInvariant() == test.ToLowerInvariant();
                case ">":
                    {
                        var result = Compare(val, test);
                        if (result.HasValue)
                        {
                            return result.Value > 0;
                        }
                    }
                    break;
                case "<":
                    {
                        var result = Compare(val, test);
                        if (result.HasValue)
                        {
                            return result.Value < 0;
                        }
                    }
                    break;
                case ">=":
                    {
                        var result = Compare(val, test);
                        if (result.HasValue)
                        {
                            return result.Value >= 0;
                        }
                    }
                    break;
                case "<=":
                    {
                        var result = Compare(val, test);
                        if (result.HasValue)
                        {
                            return result.Value <= 0;
                        }
                    }
                    break;
                case "!=":
                    return val.ToString().ToLowerInvariant() != test.ToLowerInvariant();
                case "Regex":
                    {
                        if (mRegex is null)
                        {
                            mRegex = new Regex(test, RegexOptions.Compiled);
                        }

                        if (mRegex != null)
                        {
                            return mRegex.IsMatch(val.ToString());
                        }
                    }
                    break;
                default:
                    break;
            }

            return null;
        }


        private int? Compare(object val, string test)
        {
            if (val is IComparable comp)
            {
                if (typeof(decimal).IsAssignableFrom(val.GetType()))
                {
                    return ((decimal)comp).CompareTo(decimal.Parse(test));
                }

                return comp.CompareTo(test);
            }

            return null;
        }


        private void PopulateFields()
        {
            mFieldSelection.Items.Clear();
            mProperty = null;
            Type recordType = null;
            var defaultSelection = "Storage";
            if (mSearchRecordTypeSelection.SelectedItem as string == "Series")
            {
                recordType = typeof(SeriesRecord);
                defaultSelection = "Publisher";
            }
            else if (mSearchRecordTypeSelection.SelectedItem as string == "Issue")
            {
                recordType = typeof(IssueRecord);
            }

            if (recordType is null)
            {
                return;
            }

            foreach (var prop in recordType.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public))
            {
                var propType = prop.GetType();
                if (!propType.IsGenericType && !propType.IsArray)
                {
                    mFieldSelection.Items.Add(prop.Name);
                }    
            }

            mFieldSelection.SelectedItem = defaultSelection;
        }


        private void FireSearchUpdated()
        {
            if (mSearchEnabledCheck.Checked)
            {
                OnSearchUpdated?.Invoke(this);
            }
        }


        private void mCloseButton_Click(object sender, System.EventArgs e)
        {
            OnSearchDeleted?.Invoke(this);
            FireSearchUpdated();
        }


        private void mSearchEnabledCheck_CheckedChanged(object sender, System.EventArgs e)
        {
            OnSearchUpdated?.Invoke(this);
        }


        private void mSearchRecordTypeSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            mProperty = null;
            PopulateFields();
            FireSearchUpdated();
        }

        private void mMatchOpSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            mRegex = null;
            FireSearchUpdated();
        }

        private void mMatchValueBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectNextControl(ActiveControl, true, true, true, true);
                e.Handled = true;
            }
        }

        private void mMatchValueBox_Leave(object sender, EventArgs e)
        {
            mRegex = null;
            FireSearchUpdated();
        }

        private void mFieldSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            mProperty = null;
        }


        private PropertyInfo mProperty = null;
        private Regex mRegex = null;
    }
}
