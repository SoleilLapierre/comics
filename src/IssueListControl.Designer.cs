namespace Comics
{
    partial class IssueListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueListControl));
            this.mIssueListStatusBar = new System.Windows.Forms.StatusStrip();
            this.mStatusBarText = new System.Windows.Forms.ToolStripStatusLabel();
            this.mIssueListGrid = new System.Windows.Forms.DataGridView();
            this.mIssueListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mIssueListToolBad = new System.Windows.Forms.ToolStrip();
            this.mIssueListMenuIssueAddButton = new System.Windows.Forms.ToolStripButton();
            this.mIssueListMenuIssueAddRangeButton = new System.Windows.Forms.ToolStripButton();
            this.mIssueListMenuRangeText = new System.Windows.Forms.ToolStripTextBox();
            this.mIssueListMenuIssueDeleteButton = new System.Windows.Forms.ToolStripButton();
            this.mIssueListStatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mIssueListGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mIssueListBindingSource)).BeginInit();
            this.mIssueListToolBad.SuspendLayout();
            this.SuspendLayout();
            // 
            // mIssueListStatusBar
            // 
            this.mIssueListStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mStatusBarText});
            this.mIssueListStatusBar.Location = new System.Drawing.Point(0, 618);
            this.mIssueListStatusBar.Name = "mIssueListStatusBar";
            this.mIssueListStatusBar.Size = new System.Drawing.Size(494, 22);
            this.mIssueListStatusBar.TabIndex = 0;
            this.mIssueListStatusBar.Text = "statusStrip1";
            // 
            // mStatusBarText
            // 
            this.mStatusBarText.Name = "mStatusBarText";
            this.mStatusBarText.Size = new System.Drawing.Size(0, 17);
            // 
            // mIssueListGrid
            // 
            this.mIssueListGrid.AllowUserToAddRows = false;
            this.mIssueListGrid.AllowUserToDeleteRows = false;
            this.mIssueListGrid.AllowUserToOrderColumns = true;
            this.mIssueListGrid.AllowUserToResizeRows = false;
            this.mIssueListGrid.AutoGenerateColumns = false;
            this.mIssueListGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.mIssueListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mIssueListGrid.DataSource = this.mIssueListBindingSource;
            this.mIssueListGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mIssueListGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.mIssueListGrid.Location = new System.Drawing.Point(0, 25);
            this.mIssueListGrid.Name = "mIssueListGrid";
            this.mIssueListGrid.ReadOnly = true;
            this.mIssueListGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mIssueListGrid.ShowCellErrors = false;
            this.mIssueListGrid.ShowCellToolTips = false;
            this.mIssueListGrid.ShowEditingIcon = false;
            this.mIssueListGrid.ShowRowErrors = false;
            this.mIssueListGrid.Size = new System.Drawing.Size(494, 593);
            this.mIssueListGrid.TabIndex = 1;
            this.mIssueListGrid.SelectionChanged += new System.EventHandler(this.mIssueListGrid_SelectionChanged);
            this.mIssueListGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mIssueListGrid_MouseMove);
            // 
            // mIssueListToolBad
            // 
            this.mIssueListToolBad.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mIssueListMenuIssueAddButton,
            this.mIssueListMenuIssueAddRangeButton,
            this.mIssueListMenuRangeText,
            this.mIssueListMenuIssueDeleteButton});
            this.mIssueListToolBad.Location = new System.Drawing.Point(0, 0);
            this.mIssueListToolBad.Name = "mIssueListToolBad";
            this.mIssueListToolBad.Size = new System.Drawing.Size(494, 25);
            this.mIssueListToolBad.TabIndex = 2;
            // 
            // mIssueListMenuIssueAddButton
            // 
            this.mIssueListMenuIssueAddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mIssueListMenuIssueAddButton.Image = ((System.Drawing.Image)(resources.GetObject("mIssueListMenuIssueAddButton.Image")));
            this.mIssueListMenuIssueAddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mIssueListMenuIssueAddButton.Name = "mIssueListMenuIssueAddButton";
            this.mIssueListMenuIssueAddButton.Size = new System.Drawing.Size(23, 22);
            this.mIssueListMenuIssueAddButton.ToolTipText = "Add new issue";
            this.mIssueListMenuIssueAddButton.Click += new System.EventHandler(this.mIssueListMenuIssueAddButton_Click);
            // 
            // mIssueListMenuIssueAddRangeButton
            // 
            this.mIssueListMenuIssueAddRangeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mIssueListMenuIssueAddRangeButton.Image = ((System.Drawing.Image)(resources.GetObject("mIssueListMenuIssueAddRangeButton.Image")));
            this.mIssueListMenuIssueAddRangeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mIssueListMenuIssueAddRangeButton.Name = "mIssueListMenuIssueAddRangeButton";
            this.mIssueListMenuIssueAddRangeButton.Size = new System.Drawing.Size(23, 22);
            this.mIssueListMenuIssueAddRangeButton.Text = "Add Range";
            this.mIssueListMenuIssueAddRangeButton.ToolTipText = "Add Range";
            this.mIssueListMenuIssueAddRangeButton.Click += new System.EventHandler(this.mIssueListMenuIssueAddRangeButton_Click);
            // 
            // mIssueListMenuRangeText
            // 
            this.mIssueListMenuRangeText.Name = "mIssueListMenuRangeText";
            this.mIssueListMenuRangeText.Size = new System.Drawing.Size(100, 25);
            this.mIssueListMenuRangeText.ToolTipText = "Enter range numbers here";
            // 
            // mIssueListMenuIssueDeleteButton
            // 
            this.mIssueListMenuIssueDeleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mIssueListMenuIssueDeleteButton.Enabled = false;
            this.mIssueListMenuIssueDeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("mIssueListMenuIssueDeleteButton.Image")));
            this.mIssueListMenuIssueDeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mIssueListMenuIssueDeleteButton.Name = "mIssueListMenuIssueDeleteButton";
            this.mIssueListMenuIssueDeleteButton.Size = new System.Drawing.Size(23, 22);
            this.mIssueListMenuIssueDeleteButton.ToolTipText = "Delete selected issue(s)";
            this.mIssueListMenuIssueDeleteButton.Click += new System.EventHandler(this.mIssueListMenuIssueDeleteButton_Click);
            // 
            // IssueListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mIssueListGrid);
            this.Controls.Add(this.mIssueListStatusBar);
            this.Controls.Add(this.mIssueListToolBad);
            this.Name = "IssueListControl";
            this.Size = new System.Drawing.Size(494, 640);
            this.mIssueListStatusBar.ResumeLayout(false);
            this.mIssueListStatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mIssueListGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mIssueListBindingSource)).EndInit();
            this.mIssueListToolBad.ResumeLayout(false);
            this.mIssueListToolBad.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip mIssueListStatusBar;
        private System.Windows.Forms.DataGridView mIssueListGrid;
        private System.Windows.Forms.BindingSource mIssueListBindingSource;
        private System.Windows.Forms.ToolStrip mIssueListToolBad;
        private System.Windows.Forms.ToolStripButton mIssueListMenuIssueAddButton;
        private System.Windows.Forms.ToolStripButton mIssueListMenuIssueDeleteButton;
        private System.Windows.Forms.ToolStripStatusLabel mStatusBarText;
        private System.Windows.Forms.ToolStripButton mIssueListMenuIssueAddRangeButton;
        private System.Windows.Forms.ToolStripTextBox mIssueListMenuRangeText;
    }
}
