using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Comics
{
    public partial class SeriesPropertiesForm : Form
    {
        public SeriesPropertiesForm()
        : this(new SeriesRecord())
        {
        }

        public SeriesPropertiesForm(SeriesRecord s)
        {
            mSeries = s;
            InitializeComponent();
            mTitleText.DataBindings.Add(new Binding("Text", mSeries, "Title"));
            mAltTitleText.DataBindings.Add(new Binding("Text", mSeries, "AltTitle"));
            mPublisherText.DataBindings.Add(new Binding("Text", mSeries, "Publisher"));
            mVolumeText.DataBindings.Add(new Binding("Text", mSeries, "Volume"));
            mFormatText.DataBindings.Add(new Binding("Text", mSeries, "Format"));
            mKeywordsText.DataBindings.Add(new Binding("Text", mSeries, "Keywords"));
            mCommentsText.DataBindings.Add(new Binding("Text", mSeries, "Comments"));
            mTextRefLinks.DataBindings.Add(new Binding("Text", mSeries, "ReferenceURL"));
            mLsLengthBox.DataBindings.Add(new Binding("Value", mSeries, "LimitedSeriesLength"));
            mSortKeyBox.DataBindings.Add(new Binding("Text", mSeries, "SortKey"));
        }

        public SeriesRecord Result => mSeries;

        private void Validate(object sender, CancelEventArgs e)
        {
            mOKButton.Enabled = (mTitleText.Text.Length > 0);
        }

        private void mOKButton_Click(object sender, EventArgs e)
        {
            mOKButton.Focus();
            Validate();
            if (mOKButton.Enabled)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private SeriesRecord mSeries;
    }
}