﻿namespace Comics
{
    public interface IIssueFilter
    {
        bool IncludeIssue(IssueRecord aIssue);
    }
}
