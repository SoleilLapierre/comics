using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Comics
{
    public partial class MainForm : Form
    {
        #region ----- Construction -----

        public MainForm()
        {
            InitializeComponent();
        }

        #endregion
        #region ----- Main Menu handlers -----

        #region ----- File Menu -----

        private void OnClickMenuFileNewMySql(object sender, EventArgs e)
        {
            ConnectionDialog dlg = new ConnectionDialog("Create Database");

            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            try
            {
                MySqlStorage storage = MySqlStorage.Create(dlg.HostName, dlg.DatabaseName, dlg.UserName, dlg.Password, Collection.CurrentSchema);
                Collection c = new Collection(storage);
                c.OnCollectionPropertiesChanged += new Collection.CollectionPropertiesChangedEvent(Collection_OnPropertiesChanged);
                c.OnSeriesModified += new Collection.SeriesModifiedEvent(c_OnSeriesModified);
                c.OnIssueModified += new Collection.IssueModifiedEvent(c_OnIssueModified);
                AddViewer(c);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating database: " + ex.Message);
            }
        }


        private void OnClickMenuFileNewSqlite(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog()
            {
                AddExtension = true,
                CheckFileExists = false,
                DefaultExt = ".sqlite",
                Filter = "sqlite files (*.sqlite)|*.sqlite|All files (*.*)|*.*",
                FilterIndex = 0
            };

            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (string.IsNullOrEmpty(dlg.FileName))
            {
                return;
            }

            if (File.Exists(dlg.FileName))
            {
                if (DialogResult.Yes != MessageBox.Show($"File {dlg.FileName} exists! Overwrite?", "Confirm overwrite", MessageBoxButtons.YesNo))
                {
                    return;
                }
            }

            try
            {
                var dbName = Path.GetFileNameWithoutExtension(dlg.FileName);
                SqliteStorage store = SqliteStorage.Create(dlg.FileName, dbName, Collection.CurrentSchema);
                Collection c = new Collection(store);
                c.OnCollectionPropertiesChanged += new Collection.CollectionPropertiesChangedEvent(Collection_OnPropertiesChanged);
                c.OnSeriesModified += new Collection.SeriesModifiedEvent(c_OnSeriesModified);
                c.OnIssueModified += new Collection.IssueModifiedEvent(c_OnIssueModified);
                AddViewer(c);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating database: " + ex.Message);
            }
        }


        private void OnClickMenuFileOpenMySql(object sender, EventArgs e)
        {
            ConnectionDialog cd = new ConnectionDialog("Open Database");
            DialogResult dr = cd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                try
                {
                    MySqlStorage storage = MySqlStorage.Open(cd.HostName, cd.DatabaseName, cd.UserName, cd.Password, Collection.CurrentSchema);
                    Collection c = new Collection(storage);
                    c.OnCollectionPropertiesChanged += new Collection.CollectionPropertiesChangedEvent(Collection_OnPropertiesChanged);
                    c.OnSeriesModified += new Collection.SeriesModifiedEvent(c_OnSeriesModified);
                    c.OnIssueModified += new Collection.IssueModifiedEvent(c_OnIssueModified);
                    AddViewer(c);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    MessageBox.Show(ex.Message, "ERROR");
                }
            }
        }


        private void OnClickMenuFileOpenSqlite(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog()
            {
                CheckFileExists = true,
                DefaultExt = ".sqlite",
                Filter = "sqlite files (*.sqlite)|*.sqlite|All files (*.*)|*.*",
                FilterIndex = 0
            };

            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (string.IsNullOrEmpty(dlg.FileName) || !File.Exists(dlg.FileName))
            {
                return;
            }

            try
            {
                SqliteStorage store = SqliteStorage.Open(dlg.FileName, Collection.CurrentSchema);
                Collection c = new Collection(store);
                c.OnCollectionPropertiesChanged += new Collection.CollectionPropertiesChangedEvent(Collection_OnPropertiesChanged);
                c.OnSeriesModified += new Collection.SeriesModifiedEvent(c_OnSeriesModified);
                c.OnIssueModified += new Collection.IssueModifiedEvent(c_OnIssueModified);
                AddViewer(c);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error opening database: " + ex.Message);
            }
        }


        private void SaveAllCollections()
        {
            foreach (Collection c in mViewerWindows.Keys)
            {
                if (!c.Saved)
                {
                    c.Save();
                }
            }
        }


        private void OnClickMenuFileSave(object sender, EventArgs e)
        {
            SaveAllCollections();
        }


        private void OnClickMenuFileExit(object sender, EventArgs e)
        {
            bool clean = true;

            foreach (Collection c in mViewerWindows.Keys)
            {
                if (!c.Saved)
                {
                    clean = false;
                    break;
                }
            }

            if (!clean)
            {
                DialogResult dr = MessageBox.Show("There are unsaved sollections open! Save all before quitting?", "Confirm", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    SaveAllCollections();
                }
                else if (dr == DialogResult.Cancel)
                {
                    return;
                }
            }

            Application.Exit();
        }


        private void mExportSqlite_OnClick(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog()
            {
                AddExtension = true,
                CheckFileExists = false,
                DefaultExt = ".sqlite",
                Filter = "sqlite files (*.sqlite)|*.sqlite|All files (*.*)|*.*",
                FilterIndex = 0
            };

            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (string.IsNullOrEmpty(dlg.FileName))
            {
                return;
            }

            if (File.Exists(dlg.FileName))
            {
                if (DialogResult.Yes != MessageBox.Show($"File {dlg.FileName} exists! Overwrite?", "Confirm overwrite", MessageBoxButtons.YesNo))
                {
                    return;
                }
            }

            try
            {
                Collection current = null;
                foreach (var pair in mCollectionListItems)
                {
                    if (pair.Value == mCollectionListView.SelectedItems[0])
                    {
                        current = pair.Key;
                        break;
                    }
                }

                if (current != null)
                {
                    var dbName = Path.GetFileNameWithoutExtension(dlg.FileName);
                    SqliteStorage store = SqliteStorage.Create(dlg.FileName, dbName, Collection.CurrentSchema);
                    Collection c = new Collection(store);

                    c.Import(current);
                    c.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating database: " + ex.Message);
            }
        }

        #endregion
        #endregion
        #region ----- Viewer window management -----

        private void AddViewer(Collection aCollection)
        {
            CollectionViewer cv = new CollectionViewer(aCollection);
            cv.FormClosing += new FormClosingEventHandler(OnViewerClosing);

            if (!mViewerWindows.ContainsKey(aCollection))
            {
                mViewerWindows.Add(aCollection, new List<CollectionViewer>());
            }

            mViewerWindows[aCollection].Add(cv);
            UpdateCollectionInfo(aCollection);
            
            cv.Show();
        }


        void OnViewerClosing(object sender, FormClosingEventArgs e)
        {
            var cv = sender as CollectionViewer;
            mViewerWindows[cv.Collection].Remove(cv);
            UpdateCollectionInfo(cv.Collection);
        }

        #endregion
        #region ----- Collection list management -----

        private void UpdateCollectionInfo(Collection aCollection)
        {
            if (mCollectionListItems.ContainsKey(aCollection))
            {
                mCollectionListView.Items.Remove(mCollectionListItems[aCollection]);
                mCollectionListItems.Remove(aCollection);
            }

            ListViewItem lvi = mCollectionListView.Items.Add(aCollection.InfoRecord.CollectionName);
            mCollectionListItems.Add(aCollection, lvi);

            if (aCollection.Saved)
            {
                lvi.SubItems.Add("Yes", Color.Green, SystemColors.Window, SystemFonts.DefaultFont);
            }
            else
            {
                lvi.SubItems.Add("No", Color.Red, SystemColors.Window, SystemFonts.DefaultFont);
            }

            lvi.SubItems.Add(
                String.Format("{0} series, {1} issues, {2} windows open",
                              aCollection.Series.Count,
                              aCollection.Issues.Count,
                              mViewerWindows[aCollection].Count));

            mCollectionListView.Invalidate();
        }


        private void Collection_OnPropertiesChanged(Collection aCollection)
        {
            UpdateCollectionInfo(aCollection);
        }


        private void c_OnIssueModified(Collection aCollection, int aIssueID, Collection.CollectionItemModificationType aReason)
        {
            UpdateCollectionInfo(aCollection);
        }


        private void c_OnSeriesModified(Collection aCollection, int aSeriesID, Collection.CollectionItemModificationType aReason)
        {
            UpdateCollectionInfo(aCollection);
        }

        private void mCollectionListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            mSqliteExportMenuItem.Enabled = mCollectionListView.SelectedItems.Count == 1;
        }

        #endregion
        #region ----- Data Members -----

        private Dictionary<Collection, List<CollectionViewer>> mViewerWindows = new Dictionary<Collection, List<CollectionViewer>>();
        private Dictionary<Collection, ListViewItem> mCollectionListItems = new Dictionary<Collection, ListViewItem>();

        #endregion
    }
}