namespace Comics
{
    partial class SearchTreeViewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchTreeViewControl));
            this.mLowerToolStrip = new System.Windows.Forms.ToolStrip();
            this.mAddSearchButton = new System.Windows.Forms.ToolStripButton();
            this.mTreeView = new System.Windows.Forms.TreeView();
            this.mSearchList = new System.Windows.Forms.FlowLayoutPanel();
            this.mLowerToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mLowerToolStrip
            // 
            this.mLowerToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.mLowerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mAddSearchButton});
            this.mLowerToolStrip.Location = new System.Drawing.Point(0, 482);
            this.mLowerToolStrip.Name = "mLowerToolStrip";
            this.mLowerToolStrip.Size = new System.Drawing.Size(415, 25);
            this.mLowerToolStrip.TabIndex = 0;
            this.mLowerToolStrip.Text = "toolStrip1";
            // 
            // mAddSearchButton
            // 
            this.mAddSearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mAddSearchButton.Image = ((System.Drawing.Image)(resources.GetObject("mAddSearchButton.Image")));
            this.mAddSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mAddSearchButton.Name = "mAddSearchButton";
            this.mAddSearchButton.Size = new System.Drawing.Size(23, 22);
            this.mAddSearchButton.Text = "Add Search";
            this.mAddSearchButton.Click += new System.EventHandler(this.mAddSearchButton_Click);
            // 
            // mTreeView
            // 
            this.mTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTreeView.Location = new System.Drawing.Point(0, 0);
            this.mTreeView.Name = "mTreeView";
            this.mTreeView.Size = new System.Drawing.Size(415, 482);
            this.mTreeView.TabIndex = 1;
            // 
            // mSearchList
            // 
            this.mSearchList.AutoScroll = true;
            this.mSearchList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSearchList.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mSearchList.Location = new System.Drawing.Point(0, 0);
            this.mSearchList.Name = "mSearchList";
            this.mSearchList.Size = new System.Drawing.Size(415, 482);
            this.mSearchList.TabIndex = 2;
            // 
            // SearchTreeViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mSearchList);
            this.Controls.Add(this.mTreeView);
            this.Controls.Add(this.mLowerToolStrip);
            this.Name = "SearchTreeViewControl";
            this.Size = new System.Drawing.Size(415, 507);
            this.mLowerToolStrip.ResumeLayout(false);
            this.mLowerToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mLowerToolStrip;
        private System.Windows.Forms.ToolStripButton mAddSearchButton;
        private System.Windows.Forms.TreeView mTreeView;
        private System.Windows.Forms.FlowLayoutPanel mSearchList;
    }
}
