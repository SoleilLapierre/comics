using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Comics
{
    public partial class IssueEditControl : UserControl
    {
        #region ----- Public construction and methods -----

        public delegate void IssueEditPerformedEvent(Collection aCollection, List<int> aEditedIssues);
        public event IssueEditPerformedEvent OnIssuesEdited;

        public IssueEditControl()
        {
            InitializeComponent();
            PopulateWidgets();
        }


        public Collection Collection
        {
            set { mCollection = value; }
        }


        public void EditIssues(List<int> aIssueList)
        {
            mIssues = aIssueList;
            mAddedPrereqs = new List<int>();
            PopulateWidgets();
        }

        #endregion
        #region ----- Private functonality -----

        private List<IssueRecord> GetIssueRecords()
        {
            List<IssueRecord> list = new List<IssueRecord>();

            if (mIssues != null)
            {
                foreach (int index in mIssues)
                {
                    list.Add(mCollection.Issues[index]);
                }
            }

            return list;
        }

        private void PopulateWidgets()
        {
            mCostModified = false;
            mNumIssuesModified = false;
            mPopulatingWidgets = true;

            if (mIssues.Count < 1)
            {
                mApplyButton.Enabled = false;
                mResetFormButton.Enabled = false;
                mIssuePropTextTitle.Text = "";
                mIssuePropTextSeries.Text = "<No selection>";
                mIssuePropTextIssueNumber.Text = "";
                mIssuePropTextIssueType.Text = "";
                mIssuePropTextCost.Value = 0;
                mIssuePropTextNumCopies.Value = 1;
                mIssuePropTextCondition.Text = "";
                mIssuePropTextRefLinks.Text = "";
                mIssuePropTextStorage.Text = "";
                mIssuePropTextKeywords.Text = "";
                mIssuePropTextPrereqs.Text = "";
                mIssuePropTextPrevLinks.Text = "";
                mIssuePropTextComments.Text = "";
                mIssuePropTextDate.Checked = false;
                mIssuePropTextDate.Value = DateTime.Now;
                mTableLayoutContainer.Enabled = false;
                mPopulatingWidgets = false;
                return;
            }

            mApplyButton.Enabled = true;
            mResetFormButton.Enabled = true;
            mTableLayoutContainer.Enabled = true;

            List<IssueRecord> issues = GetIssueRecords();

            if (PropertyHasCommonValue(issues, "SeriesID", out var commonValue))
            {
                SeriesRecord series = mCollection.Series[(int)commonValue];
                mIssuePropTextSeries.Text = series.FullTitle;
                // No tag because not editable.
            }
            else
            {
                mIssuePropTextSeries.Text = DIFFERENT;
            }

            PopulateStringWidget(issues, "Title", mIssuePropTextTitle);
            PopulateStringWidget(issues, "Issue", mIssuePropTextIssueNumber);

            if (PropertyHasCommonValue(issues, "Cost", out commonValue))
            {
                var commonCost = (decimal)commonValue;
                mIssuePropTextCost.Value = commonCost;
                mIssuePropTextCost.Tag = commonCost;
                mLabelDifferingCost.Visible = false;
            }
            else
            {
                mIssuePropTextCost.Value = issues[0].Cost;
                mIssuePropTextCost.Tag = null;
                mLabelDifferingCost.Visible = true;
            }

            if (PropertyHasCommonValue(issues, "NumCopies", out commonValue))
            {
                var commonNumCopies = (UInt32)commonValue;
                mIssuePropTextNumCopies.Value = (decimal)commonNumCopies;
                mIssuePropTextNumCopies.Tag = commonNumCopies;
                mLabelDifferingNumCopies.Visible = false;
            }
            else
            {
                mIssuePropTextNumCopies.Value = (decimal)issues[0].NumCopies;
                mIssuePropTextNumCopies.Tag = null;
                mLabelDifferingNumCopies.Visible = true;
            }

            PopulateStringWidget(issues, "IssueType", mIssuePropTextIssueType);
            PopulateStringWidget(issues, "Condition", mIssuePropTextCondition);
            PopulateStringWidget(issues, "Storage", mIssuePropTextStorage);
            PopulateStringWidget(issues, "Keywords", mIssuePropTextKeywords);
            PopulateStringWidget(issues, "ReferenceURL", mIssuePropTextRefLinks);
            PopulateStringWidget(issues, "ReadPrev", mIssuePropTextPrereqs);
            PopulateStringWidget(issues, "Comments", mIssuePropTextComments);

            if (PropertyHasCommonValue(issues, "DateAdded", out commonValue))
            {
                DateTime commonDate = (DateTime)commonValue;
                mIssuePropTextDate.Value = commonDate;
                mIssuePropTextDate.Tag = commonDate;
                mIssuePropTextDate.Checked = true;
                mLabelDifferingDate.Visible = false;
            }
            else
            {
                mIssuePropTextDate.Value = issues[0].DateAdded;
                mIssuePropTextDate.Tag = null;
                mIssuePropTextDate.Checked = false;
                mLabelDifferingDate.Visible = true;
            }

            if (mIssues.Count == 1)
            {
                StringBuilder sb = new StringBuilder();

                IssueRecord theIssue = issues[0];
                if (theIssue.IssueRefsReadPrev != null)
                {
                    List<int> aggregate = new List<int>(theIssue.IssueRefsReadPrev);
                    aggregate.AddRange(mAddedPrereqs);

                    foreach (int i in aggregate)
                    {
                        IssueRecord issue = mCollection.Issues[i];
                        SeriesRecord series = mCollection.Series[issue.SeriesID];
                        sb.Append(series.FullTitle);
                        sb.Append(" #");
                        sb.AppendLine(issue.Issue);
                    }
                }

                mIssuePropTextPrevLinks.Text = sb.ToString();
            }
            else
            {
                mIssuePropTextPrevLinks.Text = "<CANNOT DISPLAY>";
            }

            mPopulatingWidgets = false;
        }


        private static void PopulateStringWidget(System.Collections.IEnumerable aCollection, string aPropertyName, Control aControl)
        {
            if (PropertyHasCommonValue(aCollection, aPropertyName, out var commonValue))
            {
                string commonString = commonValue as string;
                aControl.Text = commonString;
                aControl.Tag = commonString;
            }
            else
            {
                aControl.Text = DIFFERENT;
                aControl.Tag = null;
            }
        }


        private static bool PropertyHasCommonValue(System.Collections.IEnumerable aCollection, string aPropertyName, out object aCommonValue)
        {
            bool result = false;
            object first = null;
            aCommonValue = null;

            foreach (object item in aCollection)
            {
                Type itemType = item.GetType();
                PropertyInfo prop = itemType.GetProperty(aPropertyName);
                object val = prop.GetValue(item, null);
                if (first is null)
                {
                    first = val;
                    result = true;
                }
                else
                {
                    if (val is null)
                    {
                        if (first != null)
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (!val.Equals(first))
                    {
                        result = false;
                        break;
                    }
                }
            }

            if (result)
            {
                aCommonValue = first;
            }

            return result;
        }


        private void ApplyEdits()
        {
            List<IssueRecord> issues = GetIssueRecords();

            if (issues.Count < 1)
            {
                return;
            }

            ApplyEditFromStringWidget(issues, "Title", mIssuePropTextTitle);
            ApplyEditFromStringWidget(issues, "Issue", mIssuePropTextIssueNumber);

            if (mCostModified)
            {
                SetPropertyOnAll(issues, "Cost", mIssuePropTextCost.Value);
                mCostModified = false;
            }

            if (mIssuePropTextNumCopies.Tag != null) // Common value
            {
                if (!mIssuePropTextNumCopies.Tag.Equals((UInt32)mIssuePropTextNumCopies.Value)) // Changed
                {
                    SetPropertyOnAll(issues, "NumCopies", (UInt32)mIssuePropTextNumCopies.Value);
                }
            }
            else // No common values
            {
                if (mNumIssuesModified)
                {
                    SetPropertyOnAll(issues, "NumCopies", (UInt32)mIssuePropTextNumCopies.Value);
                }
            }

            ApplyEditFromStringWidget(issues, "IssueType", mIssuePropTextIssueType);
            ApplyEditFromStringWidget(issues, "Condition", mIssuePropTextCondition);
            ApplyEditFromStringWidget(issues, "Storage", mIssuePropTextStorage);
            ApplyEditFromStringWidget(issues, "Keywords", mIssuePropTextKeywords);
            ApplyEditFromStringWidget(issues, "ReferenceURL", mIssuePropTextRefLinks);
            ApplyEditFromStringWidget(issues, "ReadPrev", mIssuePropTextPrereqs);
            ApplyEditFromStringWidget(issues, "Comments", mIssuePropTextComments);

            if (mIssuePropTextDate.Checked)
            {
                if (mIssuePropTextDate.Tag != null) // Common value
                {
                    if (!mIssuePropTextDate.Tag.Equals(mIssuePropTextDate.Value)) // Changed
                    {
                        SetPropertyOnAll(issues, "DateAdded", mIssuePropTextDate.Value);
                    }
                }
                else // No common values
                {
                    SetPropertyOnAll(issues, "DateAdded", mIssuePropTextDate.Value);
                }
            }

            foreach (IssueRecord issue in issues)
            {
                foreach (int newItem in mAddedPrereqs)
                {
                    if (!issue.IssueRefsReadPrev.Contains(newItem))
                    {
                        issue.IssueRefsReadPrev.Add(newItem);
                        issue.Dirty = true;
                    }
                }
            }

            mAddedPrereqs = new List<int>();

            FireIssuesEditedEvent();
            PopulateWidgets();
            
        }


        private static void ApplyEditFromStringWidget(System.Collections.IEnumerable aCollection, string aPropertyName, Control aWidget)
        {
            if (aWidget.Tag != null) // Common value
            {
                if (!aWidget.Tag.Equals(aWidget.Text)) // Changed
                {
                    SetPropertyOnAll(aCollection, aPropertyName, aWidget.Text);
                }
            }
            else // No common values
            {
                if (!aWidget.Text.Equals(DIFFERENT))
                {
                    SetPropertyOnAll(aCollection, aPropertyName, aWidget.Text);
                }
            }
        }


        private static void SetPropertyOnAll(System.Collections.IEnumerable aCollection, string aPropertyName, object aNewValue)
        {
            foreach (object item in aCollection)
            {
                Type itemType = item.GetType();
                PropertyInfo prop = itemType.GetProperty(aPropertyName);
                prop.SetValue(item, aNewValue, null);
            }
        }

        #endregion
        #region ----- Events -----

        private void FireIssuesEditedEvent()
        {
            if (OnIssuesEdited != null)
            {
                OnIssuesEdited(mCollection, mIssues);
            }
        }

        #region ----- Widget event handlers -----

        private void mApplyButton_Click(object sender, EventArgs e)
        {
            ApplyEdits();
        }

        private void mCancelButton_Click(object sender, EventArgs e)
        {
            mAddedPrereqs = new List<int>();
            PopulateWidgets();
        }

        private void mIssuePropTextCost_ValueChanged(object sender, EventArgs e)
        {
            if (!mPopulatingWidgets)
            {
                mCostModified = true;
            }
        }

        private void mIssuePropTextNumCopies_ValueChanged(object sender, EventArgs e)
        {
            mNumIssuesModified = true;
        }

        private void mIssuePropTextPrevLinks_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        #endregion

        #region ----- Read-previously list drag and drop -----

        private void mIssuePropTextPrevLinks_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;

            if (mIssues.Count > 0)
            {
                if (e.Data.GetDataPresent(typeof(List<int>)))
                {
                    List<int> issues = e.Data.GetData(typeof(List<int>)) as List<int>;
                    if (issues.Count > 0)
                    {
                        e.Effect = DragDropEffects.Link;
                    }
                }
            }

        }


        private void mIssuePropTextPrevLinks_DragDrop(object sender, DragEventArgs e)
        {
            if (mIssues.Count > 0)
            {
                if (e.Data.GetDataPresent(typeof(List<int>)))
                {
                    List<int> issues = e.Data.GetData(typeof(List<int>)) as List<int>;

                    foreach (int newItem in issues)
                    {
                        if (!mAddedPrereqs.Contains(newItem))
                        {
                            mAddedPrereqs.Add(newItem);
                        }
                    }

                    PopulateWidgets();
                }
            }
        }

        #endregion 

        #endregion
        #region ----- Private data -----

        private const string DIFFERENT = "<DIFFERING>";
        private Collection mCollection;
        private List<int> mIssues = new List<int>();
        private List<int> mAddedPrereqs = new List<int>();
        private bool mCostModified;
        private bool mNumIssuesModified;
        private bool mPopulatingWidgets = false;

        #endregion
    }
}
