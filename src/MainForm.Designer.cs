namespace Comics
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileNewMySql = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileNewSqlite = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mFileOpenMySql = new System.Windows.Forms.ToolStripMenuItem();
            this.mFileOpenSqlIte = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSqliteExportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mCollectionListView = new System.Windows.Forms.ListView();
            this.mTitleColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mSavedColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mInfoColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 292);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(373, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuFile});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(373, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mMenuFile
            // 
            this.mMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuFileNew,
            this.mMenuFileOpen,
            this.mMenuFileSave,
            this.exportToolStripMenuItem,
            this.mMenuFileExit});
            this.mMenuFile.Name = "mMenuFile";
            this.mMenuFile.Size = new System.Drawing.Size(37, 20);
            this.mMenuFile.Text = "File";
            // 
            // mMenuFileNew
            // 
            this.mMenuFileNew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuFileNewMySql,
            this.mMenuFileNewSqlite});
            this.mMenuFileNew.Name = "mMenuFileNew";
            this.mMenuFileNew.Size = new System.Drawing.Size(180, 22);
            this.mMenuFileNew.Text = "New Collection";
            // 
            // mMenuFileNewMySql
            // 
            this.mMenuFileNewMySql.Name = "mMenuFileNewMySql";
            this.mMenuFileNewMySql.Size = new System.Drawing.Size(117, 22);
            this.mMenuFileNewMySql.Text = "MySql...";
            this.mMenuFileNewMySql.Click += new System.EventHandler(this.OnClickMenuFileNewMySql);
            // 
            // mMenuFileNewSqlite
            // 
            this.mMenuFileNewSqlite.Name = "mMenuFileNewSqlite";
            this.mMenuFileNewSqlite.Size = new System.Drawing.Size(117, 22);
            this.mMenuFileNewSqlite.Text = "SQLite...";
            this.mMenuFileNewSqlite.Click += new System.EventHandler(this.OnClickMenuFileNewSqlite);
            // 
            // mMenuFileOpen
            // 
            this.mMenuFileOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mFileOpenMySql,
            this.mFileOpenSqlIte});
            this.mMenuFileOpen.Name = "mMenuFileOpen";
            this.mMenuFileOpen.Size = new System.Drawing.Size(180, 22);
            this.mMenuFileOpen.Text = "Open";
            // 
            // mFileOpenMySql
            // 
            this.mFileOpenMySql.Name = "mFileOpenMySql";
            this.mFileOpenMySql.Size = new System.Drawing.Size(117, 22);
            this.mFileOpenMySql.Text = "MySql...";
            this.mFileOpenMySql.Click += new System.EventHandler(this.OnClickMenuFileOpenMySql);
            // 
            // mFileOpenSqlIte
            // 
            this.mFileOpenSqlIte.Name = "mFileOpenSqlIte";
            this.mFileOpenSqlIte.Size = new System.Drawing.Size(117, 22);
            this.mFileOpenSqlIte.Text = "SQLite...";
            this.mFileOpenSqlIte.Click += new System.EventHandler(this.OnClickMenuFileOpenSqlite);
            // 
            // mMenuFileSave
            // 
            this.mMenuFileSave.Name = "mMenuFileSave";
            this.mMenuFileSave.Size = new System.Drawing.Size(180, 22);
            this.mMenuFileSave.Text = "Save All";
            this.mMenuFileSave.Click += new System.EventHandler(this.OnClickMenuFileSave);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mSqliteExportMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // mSqliteExportMenuItem
            // 
            this.mSqliteExportMenuItem.Enabled = false;
            this.mSqliteExportMenuItem.Name = "mSqliteExportMenuItem";
            this.mSqliteExportMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mSqliteExportMenuItem.Text = "SQLite...";
            this.mSqliteExportMenuItem.Click += new System.EventHandler(this.mExportSqlite_OnClick);
            // 
            // mMenuFileExit
            // 
            this.mMenuFileExit.Name = "mMenuFileExit";
            this.mMenuFileExit.Size = new System.Drawing.Size(180, 22);
            this.mMenuFileExit.Text = "Exit";
            this.mMenuFileExit.Click += new System.EventHandler(this.OnClickMenuFileExit);
            // 
            // mCollectionListView
            // 
            this.mCollectionListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.mCollectionListView.AllowColumnReorder = true;
            this.mCollectionListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.mTitleColumn,
            this.mSavedColumn,
            this.mInfoColumn});
            this.mCollectionListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mCollectionListView.FullRowSelect = true;
            this.mCollectionListView.GridLines = true;
            this.mCollectionListView.HideSelection = false;
            this.mCollectionListView.Location = new System.Drawing.Point(0, 24);
            this.mCollectionListView.MultiSelect = false;
            this.mCollectionListView.Name = "mCollectionListView";
            this.mCollectionListView.ShowGroups = false;
            this.mCollectionListView.Size = new System.Drawing.Size(373, 268);
            this.mCollectionListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.mCollectionListView.TabIndex = 2;
            this.mCollectionListView.UseCompatibleStateImageBehavior = false;
            this.mCollectionListView.View = System.Windows.Forms.View.Details;
            this.mCollectionListView.SelectedIndexChanged += new System.EventHandler(this.mCollectionListView_SelectedIndexChanged);
            // 
            // mTitleColumn
            // 
            this.mTitleColumn.Text = "Title";
            this.mTitleColumn.Width = 115;
            // 
            // mSavedColumn
            // 
            this.mSavedColumn.Text = "Saved";
            this.mSavedColumn.Width = 45;
            // 
            // mInfoColumn
            // 
            this.mInfoColumn.Text = "Details";
            this.mInfoColumn.Width = 162;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 314);
            this.Controls.Add(this.mCollectionListView);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Comics";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mMenuFile;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileNew;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileNewMySql;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileSave;
        private System.Windows.Forms.ListView mCollectionListView;
        private System.Windows.Forms.ColumnHeader mTitleColumn;
        private System.Windows.Forms.ColumnHeader mSavedColumn;
        private System.Windows.Forms.ColumnHeader mInfoColumn;
        private System.Windows.Forms.ToolStripMenuItem mMenuFileNewSqlite;
        private System.Windows.Forms.ToolStripMenuItem mFileOpenMySql;
        private System.Windows.Forms.ToolStripMenuItem mFileOpenSqlIte;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mSqliteExportMenuItem;
    }
}

