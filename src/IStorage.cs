using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Comics
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class StorageNameAttribute : XmlElementAttribute
    {
        public StorageNameAttribute(string aFieldName)
        : base(aFieldName)
        {
        }
    };

    [AttributeUsage(AttributeTargets.Property)]
    public class StorageRequiredFieldAttribute : Attribute
    {
        public bool Required = false;

        public StorageRequiredFieldAttribute()
        {
            Required = true;
        }

        public StorageRequiredFieldAttribute(bool aRequired)
        {
            Required = aRequired;
        }
    };


    [AttributeUsage(AttributeTargets.Property)]
    public class StorageKeyAttribute : Attribute
    {
    };


    [AttributeUsage(AttributeTargets.Property)]
    public class StorageTimestampAttribute : Attribute
    {
    };


    public class Record
    {
        private int mID;
        private DateTime mTimestamp;
        private bool mDirty;

        public delegate void RecordDirtiedEvent(Record aRecord);
        public event RecordDirtiedEvent OnRecordDirtied;

        [StorageKey]
        [StorageRequiredField]
        public int ID 
        {
            get { return mID; }
            set 
            { 
                mID = value;
                Dirty = true;
            }
        }

        [StorageRequiredField]
        [StorageTimestamp]
        public DateTime Timestamp
        {
            get { return mTimestamp; }
            set { mTimestamp = value; }
        }

        [XmlIgnore]
        public bool Dirty
        {
            get { return mDirty; }
            set 
            {
                bool prevValue = mDirty;
                mDirty = value;

                if (!prevValue && mDirty && OnRecordDirtied != null)
                {
                    OnRecordDirtied(this);
                }
            }
        }
    };


    public interface IStorage
    {
        Dictionary<int, T> GetRecords<T>() where T : Record;

        void BeginTransaction();
        bool CommitTransaction();
        void RollbackTransaction();

        bool Update<T>(T aRecord) where T : Record;
        bool AddRecord<T>(T aRecord) where T : Record;
        bool DeleteRecord<T>(T aRecord) where T : Record;

        string GetMessages();
    }
}
