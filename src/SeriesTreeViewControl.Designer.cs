namespace Comics
{
    partial class SeriesTreeViewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeriesTreeViewControl));
            this.mUpperToolStrip = new System.Windows.Forms.ToolStrip();
            this.mAddSeriesButton = new System.Windows.Forms.ToolStripButton();
            this.mDeleteSeriesButton = new System.Windows.Forms.ToolStripButton();
            this.mTreeView = new System.Windows.Forms.TreeView();
            this.mSeriesTreeNodeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newSeriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSeriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateReadingOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUpperToolStrip.SuspendLayout();
            this.mSeriesTreeNodeContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mUpperToolStrip
            // 
            this.mUpperToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mAddSeriesButton,
            this.mDeleteSeriesButton});
            this.mUpperToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mUpperToolStrip.Name = "mUpperToolStrip";
            this.mUpperToolStrip.Size = new System.Drawing.Size(406, 25);
            this.mUpperToolStrip.TabIndex = 0;
            this.mUpperToolStrip.Text = "toolStrip1";
            // 
            // mAddSeriesButton
            // 
            this.mAddSeriesButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mAddSeriesButton.Image = ((System.Drawing.Image)(resources.GetObject("mAddSeriesButton.Image")));
            this.mAddSeriesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mAddSeriesButton.Name = "mAddSeriesButton";
            this.mAddSeriesButton.Size = new System.Drawing.Size(23, 22);
            this.mAddSeriesButton.ToolTipText = "Add New Series";
            this.mAddSeriesButton.Click += new System.EventHandler(this.mNewSeriesButton_Click);
            // 
            // mDeleteSeriesButton
            // 
            this.mDeleteSeriesButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mDeleteSeriesButton.Enabled = false;
            this.mDeleteSeriesButton.Image = ((System.Drawing.Image)(resources.GetObject("mDeleteSeriesButton.Image")));
            this.mDeleteSeriesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mDeleteSeriesButton.Name = "mDeleteSeriesButton";
            this.mDeleteSeriesButton.Size = new System.Drawing.Size(23, 22);
            this.mDeleteSeriesButton.ToolTipText = "Delete Series";
            this.mDeleteSeriesButton.Click += new System.EventHandler(this.mDeleteSeriesButton_Click);
            // 
            // mTreeView
            // 
            this.mTreeView.AllowDrop = true;
            this.mTreeView.ContextMenuStrip = this.mSeriesTreeNodeContextMenu;
            this.mTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mTreeView.HideSelection = false;
            this.mTreeView.Location = new System.Drawing.Point(0, 25);
            this.mTreeView.Name = "mTreeView";
            this.mTreeView.Size = new System.Drawing.Size(406, 490);
            this.mTreeView.TabIndex = 1;
            this.mTreeView.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.mTreeView_AfterCollapse);
            this.mTreeView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mTreeView_MouseUp);
            this.mTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.mTreeView_DragDrop);
            this.mTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.mTreeView_AfterSelect);
            this.mTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.mTreeView_DragEnter);
            this.mTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.mTreeView_AfterExpand);
            this.mTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.mTreeView_ItemDrag);
            this.mTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this.mTreeView_DragEnter);
            // 
            // mSeriesTreeNodeContextMenu
            // 
            this.mSeriesTreeNodeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSeriesToolStripMenuItem,
            this.deleteSeriesToolStripMenuItem,
            this.toolStripSeparator1,
            this.propertiesToolStripMenuItem,
            this.generateReadingOrderToolStripMenuItem});
            this.mSeriesTreeNodeContextMenu.Name = "mSeriesTreeNodeContextMenu";
            this.mSeriesTreeNodeContextMenu.Size = new System.Drawing.Size(204, 120);
            // 
            // newSeriesToolStripMenuItem
            // 
            this.newSeriesToolStripMenuItem.Name = "newSeriesToolStripMenuItem";
            this.newSeriesToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.newSeriesToolStripMenuItem.Text = "New Series...";
            this.newSeriesToolStripMenuItem.Click += new System.EventHandler(this.mNewSeriesButton_Click);
            // 
            // deleteSeriesToolStripMenuItem
            // 
            this.deleteSeriesToolStripMenuItem.Name = "deleteSeriesToolStripMenuItem";
            this.deleteSeriesToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.deleteSeriesToolStripMenuItem.Text = "Delete Series...";
            this.deleteSeriesToolStripMenuItem.Click += new System.EventHandler(this.mDeleteSeriesButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(200, 6);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.propertiesToolStripMenuItem.Text = "Properties...";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // generateReadingOrderToolStripMenuItem
            // 
            this.generateReadingOrderToolStripMenuItem.Name = "generateReadingOrderToolStripMenuItem";
            this.generateReadingOrderToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.generateReadingOrderToolStripMenuItem.Text = "Generate Reading Order";
            this.generateReadingOrderToolStripMenuItem.Click += new System.EventHandler(this.generateReadingOrderToolStripMenuItem_Click);
            // 
            // SeriesTreeViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mTreeView);
            this.Controls.Add(this.mUpperToolStrip);
            this.Name = "SeriesTreeViewControl";
            this.Size = new System.Drawing.Size(406, 515);
            this.mUpperToolStrip.ResumeLayout(false);
            this.mUpperToolStrip.PerformLayout();
            this.mSeriesTreeNodeContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mUpperToolStrip;
        private System.Windows.Forms.ToolStripButton mAddSeriesButton;
        private System.Windows.Forms.ToolStripButton mDeleteSeriesButton;
        private System.Windows.Forms.TreeView mTreeView;
        private System.Windows.Forms.ContextMenuStrip mSeriesTreeNodeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem newSeriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSeriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateReadingOrderToolStripMenuItem;
    }
}
